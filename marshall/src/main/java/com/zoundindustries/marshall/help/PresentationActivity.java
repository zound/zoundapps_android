package com.zoundindustries.marshall.help;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.apps_lib.multiroom.UEActivityBase;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivityPresentationBinding;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by nbalazs on 01/11/2016.
 */

public class PresentationActivity extends UEActivityBase{

    private ActivityPresentationBinding mBinding;
    private String mLang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = LayoutInflater.from(this).inflate(R.layout.activity_presentation, null);
        mBinding = DataBindingUtil.bind(view);
        setContentView(view);

        mLang = Locale.getDefault().getLanguage();

        if (!checkIfLanguageIsSupported()) {
            mLang = "en";
        }

        setupAppBar();
        enableUpNavigation();
        setTitle("");

        setupControls();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if (mBinding.webView.canGoBack()) {
                    mBinding.webView.goBack();
                } else {
                    super.onBackPressed();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint({"AddJavascriptInterface", "SetJavaScriptEnabled"})
    private void setupControls() {
        String guideURL = "";
        Resources res = getResources();
        String options[] = new String[]{res.getString(R.string.help_speaker_knobs), res.getString(R.string.help_presets), res.getString(R.string.help_single_multi)};
        Bundle bundle = getIntent().getExtras();
        String pageName = bundle.getString("PAGE_TO_LOAD", null);

        if (pageName != null) {
            if (pageName.compareTo(options[0]) == 0) {
                guideURL = "file:///android_asset/quickguide/" + mLang + "/speaker_knobs.html";
            } else if (pageName.compareTo(options[1]) == 0) {
                guideURL = "file:///android_asset/quickguide/" + mLang + "/presets.html";
            } else if (pageName.compareTo(options[2]) == 0) {
                guideURL = "file:///android_asset/quickguide/" + mLang + "/solo_multi.html";
            }
        }

        mBinding.webView.addJavascriptInterface(this, "WebApiInterface");
        WebSettings webSettings = mBinding.webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mBinding.webView.setBackgroundColor(Color.TRANSPARENT);
        mBinding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                setTitle(mBinding.webView.getTitle());
            }
        });

        swichWebViewContent(guideURL);
    }


    private void swichWebViewContent(final String url) {
        mBinding.webView.post(new Runnable() {
            @Override
            public void run() {
                mBinding.webView.loadUrl(url);
            }
        });
    }

    @JavascriptInterface
    @SuppressWarnings("unused")
    public void openPageNamed(String page) {
        String guideURL = "";
        String currentLanguage = Locale.getDefault().getLanguage();

        switch (page) {
            case "add_preset_speaker":
                guideURL = "file:///android_asset/quickguide/" + mLang + "/add_preset_speaker.html";
                break;
            case "add_preset_app":
                guideURL = "file:///android_asset/quickguide/" + mLang + "/add_preset_app.html";
                break;
            default:
                break;
        }

        swichWebViewContent(guideURL);
    }

    boolean checkIfLanguageIsSupported() {
        boolean isSupported = false;
        try {
            List<String> list = Arrays.asList(getResources().getAssets().list("quickguide"));
            isSupported = list.contains(mLang);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isSupported;
    }
}

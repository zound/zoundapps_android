package com.zoundindustries.marshall.help;

import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivityQuickGuideBinding;

/**
 * Created by nbalazs on 01/11/2016.
 */

public class QuickGuideActivity extends UEActivityBase{

    private ActivityQuickGuideBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = LayoutInflater.from(this).inflate(R.layout.activity_quick_guide, null);
        mBinding = DataBindingUtil.bind(view);
        setContentView(view);

        setupAppBar();
        enableUpNavigation();
        setTitle(getResources().getString(R.string.help_quick_guide));

        setupControls();
    }

    private void setupControls() {
        Resources res = getResources();
        final String array[] = new String[]{res.getString(R.string.help_speaker_knobs), res.getString(R.string.help_presets), res.getString(R.string.help_single_multi)};
        ArrayAdapter<String> listAdapter = new HelpScreenListAdapter(this, R.layout.list_item_help_screen, array);
        addHeaderViewAndFooterView();
        mBinding.helpListView.setAdapter(listAdapter);
        mBinding.helpListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString("PAGE_TO_LOAD", array[position-1]);
                NavigationHelper.goToActivity(QuickGuideActivity.this, PresentationActivity.class, NavigationHelper.AnimationType.SlideToLeft, false, bundle);
            }
        });
    }

    public void addHeaderViewAndFooterView() {
        View listHeaderView = LayoutInflater.from(this).inflate(R.layout.list_item_header, null);
        View listFooterView = LayoutInflater.from(this).inflate(R.layout.list_item_footer, null);
        mBinding.helpListView.addHeaderView(listHeaderView, null, false);
        mBinding.helpListView.addFooterView(listFooterView, null, false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

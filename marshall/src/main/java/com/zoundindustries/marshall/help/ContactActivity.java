package com.zoundindustries.marshall.help;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.UEActivityBase;
import com.frontier_silicon.loggerlib.FsLogger;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivityContactBinding;

/**
 * Created by nbalazs on 10/02/2017.
 */

public class ContactActivity extends UEActivityBase {

    private ActivityContactBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = LayoutInflater.from(this).inflate(R.layout.activity_contact, null);
        mBinding = DataBindingUtil.bind(rootView);
        setContentView(rootView);

        setupAppBar();
        enableUpNavigation();
        setTitle(getResources().getString(R.string.contact_caps));

        init();
    }

    private void init() {
        mBinding.buttonSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getResources()
                            .getString(R.string.link_marshall_contact_support))));
                } catch (ActivityNotFoundException e) {
                    // TODO we should handle this somehow - open WebView / show toast / open market
                    FsLogger.log("ActivityNotFoundException - No browser found.");
                }
            }
        });

        mBinding.buttonGoToWebSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getResources()
                            .getString(R.string.link_marshall_shop))));
                } catch (ActivityNotFoundException e) {
                    // TODO we should handle this somehow - open WebView / show toast / open market
                    FsLogger.log("ActivityNotFoundException - No browser found.");
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
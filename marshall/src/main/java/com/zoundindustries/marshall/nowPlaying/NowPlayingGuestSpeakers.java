package com.zoundindustries.marshall.nowPlaying;

import android.os.Bundle;

import com.apps_lib.multiroom.UEActivityBase;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrPosition;
import com.zoundindustries.marshall.R;

/**
 * Created by nbalazs on 18/09/2017.
 */

public class NowPlayingGuestSpeakers extends UEActivityBase {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_now_playing_guest_speakers);

        NowPlayingFragment nowPlayingFragment = new NowPlayingFragment();
        nowPlayingFragment.hideButtonsLayout();
        getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, nowPlayingFragment).commit();

        SlidrConfig slidrConfig = new SlidrConfig.Builder()
                .position(SlidrPosition.TOP).build();
        Slidr.attach(this, slidrConfig);

        attachActivityToVolumePopup();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.slide_out_down);
    }
}

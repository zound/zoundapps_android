package com.zoundindustries.marshall.nowPlaying;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps_lib.multiroom.NavigationHelper;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.FragmentNowPlayingFooterBinding;
import com.zoundindustries.marshall.source.SourcesActivity;

/**
 * Created by lsuhov on 24/05/16.
 */
public class NowPlayingFooterFragment extends Fragment {

    private FragmentNowPlayingFooterBinding mBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_now_playing_footer, container, false);

        NowPlayingFooterViewModel viewModel = new NowPlayingFooterViewModel();
        mBinding.setViewModel(viewModel);

        Radio radio = NetRemoteManager.getInstance().getCurrentRadio();

        mBinding.getViewModel().init(radio, getActivity());

        setupControls();

        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        if (mBinding != null) {
            mBinding.getViewModel().dispose();
            mBinding.setViewModel(null);
            mBinding = null;
        }

        super.onDestroyView();
    }

    private void setupControls() {
        mBinding.nowPlayingFooterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SourcesActivity)getActivity()).onFooterClicked();
            }
        });

        mBinding.nowPlayingContent.setSelected(true);
    }
}

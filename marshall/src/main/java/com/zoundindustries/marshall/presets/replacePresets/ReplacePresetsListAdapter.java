package com.zoundindustries.marshall.presets.replacePresets;

import android.content.Context;
import android.databinding.Observable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps_lib.multiroom.nowPlaying.NowPlayingManager;
import com.apps_lib.multiroom.presets.PresetItemModel;
import com.apps_lib.multiroom.presets.PresetSubType;
import com.apps_lib.multiroom.presets.spotify.SpotifyManager;
import com.apps_lib.multiroom.util.ZoundUtil;
import com.bumptech.glide.Glide;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.zoundindustries.marshall.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lsuhov on 24/04/16.
 */
class ReplacePresetsListAdapter extends ArrayAdapter<PresetItemModel> {

    private int mLayoutId;
    private int positionOfReplacedPreset = -1;
    private Map<PresetItemModel, Observable.OnPropertyChangedCallback> mObservableCallbackMap = new HashMap<>();

    ReplacePresetsListAdapter(Context context, int resource, List<PresetItemModel> list) {
        super(context, resource, list);

        mLayoutId = resource;
    }

    void presetWasReplacedAt(int position) {
        positionOfReplacedPreset = position;
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(mLayoutId, null);
        }

        PresetItemModel presetItemModel = getItem(position);

        if (presetItemModel != null) {
            boolean isEmpty = TextUtils.isEmpty(presetItemModel.presetName.get()) &&
                    TextUtils.isEmpty(presetItemModel.presetType.get());

            final ImageView presetImageView = (ImageView) v.findViewById(R.id.presetImageView);

            if (isEmpty || TextUtils.isEmpty(presetItemModel.artworkUrl.get())) {
                presetImageView.setImageResource(R.drawable.artwork_placeholder_black_empty);
            } else {
                trySettingArtwork(presetItemModel.artworkUrl.get(), presetImageView, true, position);
            }

            addOnPropertyChangeOnCallback(presetItemModel, presetImageView);

            TextView presetIndexTextView = (TextView) v.findViewById(R.id.indexOfPresetTextView);
            presetIndexTextView.setText(String.valueOf(presetItemModel.getFriendlyPresetIndex()));
            presetIndexTextView.setAlpha(positionOfReplacedPreset == presetItemModel.presetIndex ? 1.0f : 0.2f);

            TextView presetNameTextView = (TextView) v.findViewById(R.id.presetNameTextView);
            presetNameTextView.setText(isEmpty ? getContext().getString(R.string.empty_preset) : presetItemModel.presetName.get());

            TextView presetTypeTextView = (TextView) v.findViewById(R.id.presetTypeTextView);
            presetTypeTextView.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
            if (!isEmpty) {
                presetTypeTextView.setText(PresetSubType.getFriendlyNameFromSubType(
                        presetItemModel.presetSubType.get(), getContext()));
            }
        }

        return v;
    }

    private void trySettingArtwork(String artworkUrl, final ImageView presetImageView, boolean firstLoad, int presetIndex) {
        boolean canLoadArtwork = false;
            PresetItemModel presetItemModel = getItem(presetIndex);
        if (firstLoad) {
            if (presetItemModel != null) {
                canLoadArtwork = presetItemModel.presetType.get().equals("IR") || (presetItemModel.presetType.get().equals("Spotify") && SpotifyManager.getInstance().getSpotifyAuthenticator().isSpotifyLinked());
            }
        } else {
            NodeSysCapsValidModes.Mode currentMode = NowPlayingManager.getInstance().getNowPlayingDataModel().currentMode.get();
            canLoadArtwork = (currentMode == NodeSysCapsValidModes.Mode.IR) || (currentMode == NodeSysCapsValidModes.Mode.Spotify && SpotifyManager.getInstance().getSpotifyAuthenticator().isSpotifyLinked());
        }
        if (canLoadArtwork) {
            if (URLUtil.isValidUrl(artworkUrl) && !ZoundUtil.isDestroyed(presetImageView.getContext())) {
                try {
                    Glide.with(presetImageView.getContext())
                            .load(artworkUrl)
                            .into(presetImageView);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        } else {
            presetImageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.artwork_placeholder_black_empty));
            if (presetItemModel.presetType.get().equals("Spotify") && !SpotifyManager.getInstance().getSpotifyAuthenticator().isSpotifyLinked()) {
                presetItemModel.artworkUrl.set("");
            }
        }
    }

    private void addOnPropertyChangeOnCallback(final PresetItemModel presetItemModel, final ImageView presetImageView) {
        Observable.OnPropertyChangedCallback callback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        trySettingArtwork(presetItemModel.artworkUrl.get(), presetImageView, false, presetItemModel.presetIndex);
                        notifyDataSetChanged();
                    }
                }, true);
            }
        };

        Observable.OnPropertyChangedCallback oldCallback = mObservableCallbackMap.get(presetItemModel);
        if (oldCallback != null) {
            mObservableCallbackMap.remove(presetItemModel);
            presetItemModel.artworkUrl.removeOnPropertyChangedCallback(oldCallback);
        }

        presetItemModel.artworkUrl.addOnPropertyChangedCallback(callback);

        mObservableCallbackMap.put(presetItemModel, callback);
    }

    public void onDestroy() {
        List<PresetItemModel> keySetList = new ArrayList<>(mObservableCallbackMap.keySet());
        for (PresetItemModel presetItemModel : keySetList) {
            presetItemModel.artworkUrl.removeOnPropertyChangedCallback(mObservableCallbackMap.get(presetItemModel));
            mObservableCallbackMap.remove(presetItemModel);
        }

        mObservableCallbackMap.clear();
    }
}

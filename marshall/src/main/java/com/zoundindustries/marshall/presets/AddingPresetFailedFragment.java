package com.zoundindustries.marshall.presets;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.apps_lib.multiroom.AnimatedDialogFragmentBase;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.FailedPresetDialogBinding;


/**
 * Created by nbalazs on 12/10/2016.
 */
public class AddingPresetFailedFragment extends AnimatedDialogFragmentBase {

    public static DialogFragment newInstance() {
        return new AddingPresetFailedFragment();
    }

    private FailedPresetDialogBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.failed_preset_dialog, container, false);
        mBinding = DataBindingUtil.bind(rootView);
        FailedToSavePresetViewModel viewModel = new FailedToSavePresetViewModel(this, getActivity());
        mBinding.setViewModel(viewModel);
        init();
        Window window = getDialog().getWindow();
        if (window != null) {
            window.requestFeature(Window.FEATURE_NO_TITLE);
        }
        return rootView;
    }

    private void init() {

        int sizeInPixel = (int) convertDpToPixel(15, getContext());

        Spannable content2 = new SpannableString(getResources().getString(R.string.preset_failed_content_how_to_add_a_preset_2));
        Spannable content3 = new SpannableString(getResources().getString(R.string.preset_failed_content_how_to_add_a_preset_3));
        Spannable content4 = new SpannableString(getResources().getString(R.string.preset_failed_content_how_to_add_a_preset_4));
        Drawable cloudIcon = ContextCompat.getDrawable(getContext(), R.drawable.ic_cloud_white);
        cloudIcon.setBounds(0, 0, sizeInPixel, sizeInPixel);
        ImageSpan cloudImageSpan = new ImageSpan(cloudIcon, ImageSpan.ALIGN_BASELINE) {
            public void draw(Canvas canvas, CharSequence text, int start,
                             int end, float x, int top, int y, int bottom,
                             Paint paint) {
                Drawable b = getDrawable();
                canvas.save();

                int transY = bottom - b.getBounds().bottom;
                // this is the key
                transY -= paint.getFontMetricsInt().descent / 2;

                canvas.translate(x, transY);
                b.draw(canvas);
                canvas.restore();
            }
        };
        Spannable imagePlaceholder = new SpannableString("             ");
        imagePlaceholder.setSpan(cloudImageSpan, 2, 12, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        Spannable content5 = new SpannableString(getResources().getString(R.string.preset_failed_content_how_to_add_a_preset_5));

        Spanned part1 = (Spanned) TextUtils.concat(content2, content3, content4, imagePlaceholder, content5);

        Spannable content6 = new SpannableString(getResources().getString(R.string.preset_failed_content_how_to_add_a_preset_6));
        Drawable presetAddIcon = ContextCompat.getDrawable(getContext(), R.drawable.add_preset_icon);
        presetAddIcon.setBounds(0, 0, sizeInPixel, sizeInPixel);
        ImageSpan presetAddIconSpan = new ImageSpan(presetAddIcon, ImageSpan.ALIGN_BOTTOM) {
            public void draw(Canvas canvas, CharSequence text, int start,
                             int end, float x, int top, int y, int bottom,
                             Paint paint) {
                Drawable b = getDrawable();
                canvas.save();

                int transY = bottom - b.getBounds().bottom;
                // this is the key
                transY -= paint.getFontMetricsInt().descent / 2;

                canvas.translate(x, transY);
                b.draw(canvas);
                canvas.restore();
            }
        };
        Spannable imagePlaceholder2 = new SpannableString(" (   ) ");
        imagePlaceholder2.setSpan(presetAddIconSpan, 3, 4, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        Spannable content7 = new SpannableString(getResources().getString(R.string.preset_failed_content_how_to_add_a_preset_7));

        Spanned part2 = (Spanned) TextUtils.concat(content6, imagePlaceholder2, content7);

        mBinding.contentTextMsgP1.setText(getResources().getString(R.string.preset_failed_content_how_to_add_a_preset_1));
        mBinding.contentTextMsgP2.setText(part1);
        mBinding.contentTextMsgP3.setText(part2);
    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }
}

package com.zoundindustries.marshall.presets;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.help.PresentationActivity;

/**
 * Created by nbalazs on 12/10/2016.
 */

public class FailedToSavePresetViewModel {

    private AddingPresetFailedFragment mDialog;
    private Activity mActivity;

    FailedToSavePresetViewModel(AddingPresetFailedFragment dialog, Activity activity) {
        mDialog = dialog;
        mActivity = activity;
    }

    @SuppressWarnings("unused")
    public void onBackClicked(View view) {
        mDialog.dismiss();
    }

    @SuppressWarnings("unused")
    public void onReadMoreClicked(View view) {
        Bundle bundle = new Bundle();
        bundle.putString("PAGE_TO_LOAD", mActivity.getResources().getString(R.string.help_presets));
        NavigationHelper.goToActivity(mActivity, PresentationActivity.class, NavigationHelper.AnimationType.SlideToLeft, false, bundle);
    }
}

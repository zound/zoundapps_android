package com.zoundindustries.marshall.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;

import com.apps_lib.multiroom.widgets.AnimationFreeSwitchBase;
import com.zoundindustries.marshall.R;

/**
 * Created by nbalazs on 26/09/2016.
 */
public class AnimationFreeSwitch extends AnimationFreeSwitchBase {

    public AnimationFreeSwitch(Context context) {
        super(context);
        mLeftString = context.getResources().getString(R.string.single).toUpperCase();
        mRightString = context.getResources().getString(R.string.multi).toUpperCase();
    }

    public AnimationFreeSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        mLeftString = context.getResources().getString(R.string.single).toUpperCase();
        mRightString = context.getResources().getString(R.string.multi).toUpperCase();
    }

    public AnimationFreeSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mLeftString = context.getResources().getString(R.string.single).toUpperCase();
        mRightString = context.getResources().getString(R.string.multi).toUpperCase();
    }

    @Override
    protected Paint createSelectedTextPaint() {
        Paint textPaint = new Paint();
        //noinspection deprecation
        textPaint.setColor(getContext().getResources().getColor(R.color.m_home_switch_color));
        textPaint.setTextSize(getFixedSize());
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        // Set textSize, typeface, etc, as you wish
        return textPaint;
    }

    @Override
    protected Paint createNotSelectedTextPaint() {
        Paint textPaint = new Paint();
        //noinspection deprecation
        textPaint.setColor(getContext().getResources().getColor(R.color.colorAccent));
        textPaint.setTextSize(getFixedSize());
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        // Set textSize, typeface, etc, as you wish
        return textPaint;
    }

    @Override
    protected float getFixedSize() {
        float textSizeInDP = 12.0f;
        float density = getContext().getResources().getDisplayMetrics().density;

        if ((Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) && (density < 1.75f)) {
            textSizeInDP = 9.0f;
        }
        return textSizeInDP * density + 0.5f;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        final Rect textBounds = new Rect();
        mNotSelectedTextPaint.getTextBounds(mRightString, 0, mRightString.length(), textBounds);

        // The baseline for the text: centered, including the height of the text itself
        final int heightBaseline = canvas.getClipBounds().height() / 2 + textBounds.height() / 2;

        // This is one quarter of the full width, to measure the centers of the texts
        final int widthQuarter = canvas.getClipBounds().width() / 4;
        canvas.drawText(mLeftString, 0, mLeftString.length(),
                widthQuarter + 4, heightBaseline,
                !isChecked() ? mNotSelectedTextPaint : mSelectedTextPaint);
        canvas.drawText(mRightString, 0, mRightString.length(),
                widthQuarter * 3 - 4, heightBaseline,
                isChecked() ? mNotSelectedTextPaint : mSelectedTextPaint);
    }
}

package com.zoundindustries.marshall.setup.normalSetup;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apps_lib.multiroom.myHome.speakers.SpeakerNameManager;
import com.apps_lib.multiroom.setup.normalSetup.AccessPointModel;
import com.apps_lib.multiroom.setup.normalSetup.SetupManager;
import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.ESpeakerModelType;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.myHome.HomeSpeakersMediator;
import com.zoundindustries.marshall.speakerImages.MarshallSpeakerTypeDecoder;

import java.util.ArrayList;
import java.util.List;

public class AccessPointsArrayAdapter extends ArrayAdapter<AccessPointModel> {

    private Context mContext = null;
    private int mLayoutId;
    private List<Radio> mRadioList = new ArrayList<>();

    public AccessPointsArrayAdapter(Context context, int resource, List<AccessPointModel> objects) {
        super(context, resource, objects);

        mContext = context;
        mLayoutId = resource;

        mRadioList.addAll(HomeSpeakersMediator.mNotConfiguredRadios);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null || view.getTag() == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(mLayoutId, null);
        }

        if (position < super.getCount()) {
            view = handleAccessPointModel(position, view);
        } else if (mRadioList.size() > 0) {
            view = handleRadio(position - super.getCount(), view);
        }

        return view;
    }

    private View handleRadio(int position, View view) {
        Radio radio = mRadioList.get(position);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageSpeaker);
        TextView tv1 = (TextView) view.findViewById(R.id.text1);
        TextView tv3 = (TextView) view.findViewById(R.id.text3);

        imageView.setImageResource(SpeakerManager.getInstance().getImageIdForSSID(radio.getModelName(), ESpeakerImageSizeType.Home));

        SpeakerNameManager speakerNameManager = SpeakerNameManager.getInstance();

        ESpeakerModelType speakerModelType;
        if (SpeakerManager.getInstance().getSpeakerModelFromSSID(radio.getModelName()) != ESpeakerModelType.Unknown) {
            speakerModelType = SpeakerManager.getInstance().getSpeakerModelFromSSID(radio.getModelName());
        } else {
            speakerModelType = SpeakerManager.getInstance().getSpeakerModelFromSSID(radio.getModelName());
        }
        tv1.setText(speakerNameManager.generateNameForDevice(speakerModelType, radio.getUDN(), radio.getFriendlyName() != null ? radio.getFriendlyName() : getRadioNameAndMac(radio.getSessionId())));


        tv3.setText(mContext.getResources().getString(R.string.setup_connected));

        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarConnecting);
        progressBar.setVisibility(View.GONE);

        ImageView checkMarkImageView = (ImageView) view.findViewById(R.id.radioSelectedImageView);
        checkMarkImageView.setVisibility(View.GONE);

        view.setTag(radio);

        return view;
    }

    private View handleAccessPointModel(int position, View view) {
        AccessPointModel device = getItem(position);
        ScanResult scanResult = device.getScanResult();

        ImageView imageView = (ImageView) view.findViewById(R.id.imageSpeaker);
        TextView tv1 = (TextView) view.findViewById(R.id.text1);
        TextView tv3 = (TextView) view.findViewById(R.id.text3);

        imageView.setImageResource(SpeakerManager.getInstance().getImageIdForSSID(scanResult.SSID, ESpeakerImageSizeType.Home));

        SpeakerNameManager speakerNameManager = SpeakerNameManager.getInstance();
        if (SpeakerManager.getInstance().getSpeakerModelFromSSID(scanResult.SSID) != ESpeakerModelType.Unknown) {
            ESpeakerModelType speakerModelType = SpeakerManager.getInstance().getSpeakerModelFromSSID(scanResult.SSID);
            tv1.setText(speakerNameManager.generateNameForDevice(speakerModelType, scanResult.SSID, device.getSpeakerName() != null ? device.getSpeakerName() : getRadioNameAndMac(scanResult.SSID)));
        } else {
            ESpeakerModelType speakerModelType = SpeakerManager.getInstance().getSpeakerModelFromSSID(scanResult.SSID);
            tv1.setText(speakerNameManager.generateNameForDevice(speakerModelType, scanResult.SSID, device.getSpeakerName() != null ? device.getSpeakerName() : scanResult.SSID));
        }

        tv3.setText(mContext.getResources().getString(R.string.setup_connect_to_wi_fi));

        boolean isConnectingToRadio = SetupManager.getInstance().isConnecting(scanResult);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarConnecting);
        progressBar.setVisibility(isConnectingToRadio ? View.VISIBLE : View.GONE);

        boolean isConnectedToRadio = SetupManager.getInstance().isConnectedToRadio() && SetupManager.getInstance().isConnectedToGivenAP(scanResult);

        ImageView checkMarkImageView = (ImageView) view.findViewById(R.id.radioSelectedImageView);
        checkMarkImageView.setVisibility(isConnectedToRadio ? View.VISIBLE : View.GONE);

        view.setTag(device);

        return view;
    }

    @Override
    public int getCount() {
        return super.getCount() + mRadioList.size();
    }

    private String getRadioNameAndMac(String radioFullName) {
        String[] radioName = radioFullName.split("_");
        int length = radioName.length;
        return radioName[0] + " " + radioName[length - 1];
    }

    public final int getAccessPointCount() {
        return super.getCount();
    }

    public final int getRadioCount() {
        return mRadioList.size();
    }

    public final Radio getRadioForIndex(int index) {
        return mRadioList.get(index - super.getCount());
    }
}

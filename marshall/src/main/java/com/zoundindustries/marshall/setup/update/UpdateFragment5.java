package com.zoundindustries.marshall.setup.update;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.apps_lib.multiroom.NavigationHelper;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.setup.presets.AllFinishedActivity;
import com.zoundindustries.marshall.setup.presets.ChoosePresetsTypeActivityMarshall;

/**
 * Created by nbalazs on 31/05/2017.
 */

public class UpdateFragment5 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.update_screen_content_5, container, false);;

        Button addPresetBtn = (Button)view.findViewById(R.id.buttonAddPresets);
        addPresetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToActivity(getActivity(), ChoosePresetsTypeActivityMarshall.class, NavigationHelper.AnimationType.SlideToLeft, true);
            }
        });

        Button skipBtn = (Button)view.findViewById(R.id.buttonSkip);
        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToActivity(getActivity(), AllFinishedActivity.class, NavigationHelper.AnimationType.SlideToLeft, true);
            }
        });

        return view;
    }

}
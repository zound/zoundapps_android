package com.zoundindustries.marshall.setup.normalSetup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.FragmentNoNewSpeakerFound1Binding;


/**
 * Created by viftime on 28/03/16.
 */
public class NoNewSpeakerFound1Fragment extends NoNewSpeakerFoundFragmentBase {

    public static final String FRAGMENT_TAG = "TAG_NO_NEW_SPEAKER_FOUND_1_FRAGMENT";

    private FragmentNoNewSpeakerFound1Binding mBinding;

    public NoNewSpeakerFound1Fragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_no_new_speaker_found_1, container, false);

        return mBinding.getRoot();
    }

    @Override
    protected void setupControls() {
        mBinding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNextPage();
            }
        });

        mBinding.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelSetup();
            }
        });
    }

}

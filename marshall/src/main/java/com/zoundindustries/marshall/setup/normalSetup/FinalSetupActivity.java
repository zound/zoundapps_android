package com.zoundindustries.marshall.setup.normalSetup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.setup.normalSetup.SetupManager;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeMiscNvsData;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivitySetupFinalBinding;

/**
 * Created by lsuhov on 04/06/16.
 */
public class FinalSetupActivity extends UEActivityBase {

    private ActivitySetupFinalBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setup_final);

        setupAppBar();
        setTitle(R.string.all_done);

        setupControls();

        markOnBoardingFinished();

    }

    private void setupControls() {
        mBinding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToHomeAndClearActivityStack(FinalSetupActivity.this);
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    public void markOnBoardingFinished() {
        Radio radio = SetupManager.getInstance().getRadioForPresets();
        if (radio != null) {
            NodeInfo nodeInfo = new NodeMiscNvsData("{\"s\":1}");
            radio.setNode(nodeInfo, false);
        }
    }
}

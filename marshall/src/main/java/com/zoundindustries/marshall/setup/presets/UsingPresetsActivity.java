package com.zoundindustries.marshall.setup.presets;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.github.sahasbhop.apngview.ApngImageLoader;
import com.zoundindustries.marshall.GuiUtilsMarshall;
import com.zoundindustries.marshall.R;

/**
 * Created by nbalazs on 28/06/2017.
 */

public class UsingPresetsActivity extends UEActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_using_presets);

        setupAppBar();
        setTitle(R.string.using_presets_title);

        setupControls();

        startAnimation();
    }

    private void startAnimation() {
        ImageView presetImage = (ImageView) findViewById(R.id.blinkingLedsImageAnimated);
        ImageView knobLabels = (ImageView) findViewById(R.id.knobLabels);
        GuiUtilsMarshall.rescaleImagesBasedOnScreenDensity(this, knobLabels, presetImage);

        String uri = "assets://apng/presets_tutorial.png";
        ApngImageLoader.getInstance().init(this);
        ApngImageLoader.getInstance().displayApng(uri, presetImage, new ApngImageLoader.ApngConfig(0, true));
        knobLabels.setVisibility(View.VISIBLE);
    }

    private void setupControls() {
        findViewById(R.id.buttonDone) .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToActivity(UsingPresetsActivity.this, AllFinishedActivity.class, NavigationHelper.AnimationType.SlideToLeft);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        ApngImageLoader.getInstance().stop();
    }
}
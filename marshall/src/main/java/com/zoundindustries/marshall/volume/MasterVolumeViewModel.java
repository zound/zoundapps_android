package com.zoundindustries.marshall.volume;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableFloat;
import android.databinding.ObservableInt;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.SeekBar;

import com.apps_lib.multiroom.myHome.speakers.SpeakerModel;
import com.apps_lib.multiroom.volume.VolumeModel;
import com.apps_lib.multiroom.volume.VolumesManager;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;
import com.zoundindustries.marshall.databinding.ListItemVolumeScreenMasterBinding;

/**
 * Created by lsuhov on 22/06/16.
 */
public class MasterVolumeViewModel implements SeekBar.OnSeekBarChangeListener {
    private SpeakerModel mSpeakerModel;
    private VolumeModel mMasterVolumeModel;
    private Observable.OnPropertyChangedCallback mPropertyChangedCallback;

    private static int MAX_VIRTUAL_STEP_VALUE = 100;
    private static int CHECK_TIME_LIMIT = 1000;

    private int maxValue = 0;
    public ObservableInt maxLayoutValue = new ObservableInt(MAX_VIRTUAL_STEP_VALUE);
    public ObservableInt seekLayoutValue = new ObservableInt(0);
    public ObservableFloat seekbarOpacity = new ObservableFloat(VolumesManager.UNMUTED_OPACITY);
    public ObservableBoolean isVisible = new ObservableBoolean(false);
    public ObservableBoolean mMasterIsPresent = new ObservableBoolean(true);

    private boolean isSeeking = false;
    private ListItemVolumeScreenMasterBinding mBinding;

    private Activity mActivity;

    private long mLastVolumeChangeMoment;

    MasterVolumeViewModel(SpeakerModel speakerModel, boolean masterIsPresent, ListItemVolumeScreenMasterBinding binding, Activity activity) {
        mSpeakerModel = speakerModel;
        mMasterIsPresent.set(masterIsPresent);
        mBinding = binding;
        mActivity = activity;
        init();
    }

    private void updateVolumeValue() {
        if (isSeeking || !shouldUpdateVolumeLayout()) {
            return;
        }

        final int virtualMasterVolumeValue = realMasterStepToVirtual(mMasterVolumeModel.value.get());
        if (!isTheSameVirtualMasterValue(seekLayoutValue.get(), realMasterStepToVirtual(virtualMasterVolumeValue))) {
            final ObjectAnimator animation = ObjectAnimator.ofInt(mBinding.masterVolumeSeekBar, "progress", virtualMasterVolumeValue);
            animation.setDuration(250);
            animation.setInterpolator(new AccelerateDecelerateInterpolator());
            animation.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    seekLayoutValue.set(virtualMasterVolumeValue);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            if (mActivity != null) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        animation.start();
                    }
                });
            }
        }
    }


    private void init() {
        if (mSpeakerModel.deviceModel == null || mSpeakerModel.deviceModel.mRadio == null) {
            FsLogger.log("Device model or Radio from MasterVolumeViewModel is null", LogLevel.Error);
            return;
        }

        mMasterVolumeModel = VolumesManager.getInstance().masterVolumeModel;

        VolumesManager.getInstance().masterUdn = mSpeakerModel.deviceModel.mRadio.getUDN();

        addListeners();

        updateAll();

        mBinding.masterVolumeSeekBar.setOnSeekBarChangeListener(this);
    }

    private void updateAll() {
        requestVolumeStepsIfNeeded();
        updateVolumeSteps();
        updateVolumeValue();
        updateMuted();
    }

    private void addListeners() {
        mPropertyChangedCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (observable == mMasterVolumeModel.steps) {
                    updateVolumeSteps();
                } else if (observable == mMasterVolumeModel.value) {
                    updateVolumeValue();
                } else if (observable == mMasterVolumeModel.muted) {
                    updateMuted();
                }
            }
        };

        mMasterVolumeModel.steps.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mMasterVolumeModel.value.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mMasterVolumeModel.muted.addOnPropertyChangedCallback(mPropertyChangedCallback);
    }

    private void updateVolumeSteps() {
        maxValue = mMasterVolumeModel.steps.get() - 1;
        maxLayoutValue.set(MAX_VIRTUAL_STEP_VALUE - MAX_VIRTUAL_STEP_VALUE % maxValue);
        isVisible.set(maxValue != 0);
    }

    private void updateMuted() {
        seekbarOpacity.set(mMasterVolumeModel.muted.get() ? VolumesManager.MUTED_OPACITY : VolumesManager.UNMUTED_OPACITY);
    }

    private void requestVolumeStepsIfNeeded() {
        if (!mMasterVolumeModel.stepsRetrieved) {
            VolumesManager.getInstance().retrieveVolumeSteps(mSpeakerModel.deviceModel, mMasterVolumeModel);
        }
    }

    public void dispose() {
        if (mMasterVolumeModel != null) {
            mMasterVolumeModel.steps.removeOnPropertyChangedCallback(mPropertyChangedCallback);
            mMasterVolumeModel.value.removeOnPropertyChangedCallback(mPropertyChangedCallback);
            mMasterVolumeModel.muted.removeOnPropertyChangedCallback(mPropertyChangedCallback);
            mMasterVolumeModel = null;
        }

        mSpeakerModel = null;
        mPropertyChangedCallback = null;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
        if (!isSeeking) {
            return;
        }

        if (mMasterVolumeModel != null && !isTheSameVirtualMasterValue(seekLayoutValue.get(), progress)) {
            int realMasterValue = virtualMasterStepToReal(progress);
            VolumesManager.getInstance().setMasterVolumeValueToRadio(mSpeakerModel.deviceModel.mRadio, realMasterValue);
            setMomentOfVolumeSet();
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isSeeking = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isSeeking = false;
    }

    private boolean isTheSameVirtualMasterValue(int actualValue, int newValue) {
        return maxValue != 0 && virtualMasterStepToReal(actualValue) == virtualMasterStepToReal(newValue);

    }

    private int realMasterStepToVirtual(int realEqStep) {
        return maxValue != 0 ? (MAX_VIRTUAL_STEP_VALUE / maxValue) * realEqStep : 0;
    }

    private int virtualMasterStepToReal(int virtualEqStep) {
        if (maxValue == 0) {
            return 0;
        }

        int virtualStepPerValue = MAX_VIRTUAL_STEP_VALUE / maxValue;

        return virtualEqStep / virtualStepPerValue;
    }

    private void setMomentOfVolumeSet() {
        mLastVolumeChangeMoment = System.currentTimeMillis();
    }

    private boolean shouldUpdateVolumeLayout() {
        return (System.currentTimeMillis() - mLastVolumeChangeMoment) > CHECK_TIME_LIMIT;
    }
}

package com.zoundindustries.marshall.volume;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.apps_lib.multiroom.myHome.speakers.SpeakerModel;
import com.apps_lib.multiroom.myHome.speakers.SpeakersBuilder;
import com.apps_lib.multiroom.volume.VolumeSpeakersBuilder;
import com.frontier_silicon.components.multiroom.IMultiroomGroupingListener;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.frontier_silicon.components.multiroom.MultiroomItemModel;
import com.frontier_silicon.loggerlib.FsLogger;

import java.util.List;

/**
 * Created by nbalazs on 24/05/2017.
 */

public class VolumeSpeakersMediator implements IMultiroomGroupingListener {

    protected Activity mActivity;
    protected RecyclerView mRecyclerView;
    protected VolumeScreenAdapter mAdapter;
    protected SpeakersBuilder mSpeakersBuilder;

    public void init(Activity activity, RecyclerView recyclerView) {
        mActivity = activity;
        mRecyclerView = recyclerView;
        mAdapter = new VolumeScreenAdapter(mActivity);
        mSpeakersBuilder = new VolumeSpeakersBuilder();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity.getApplicationContext()));
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    public void attachToDiscovery() {
        FsLogger.log("Speakers Mediator: attach to discovery");
        detachFromDiscovery();

        MultiroomGroupManager.getInstance().addListener(this);

        updateListOfHomeSpeakers();
    }

    public void detachFromDiscovery() {
        MultiroomGroupManager.getInstance().removeListener(this);
    }

    @Override
    public void onGroupingUpdate() {
        FsLogger.log("Speakers Mediator grouping update");
        if (mActivity != null && !mActivity.isFinishing()) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateListOfHomeSpeakers();
                }
            });
        }
    }

    private void updateListOfHomeSpeakers() {
        if (mActivity == null) {
            return;
        }

        List<MultiroomItemModel> multiroomItemModels = MultiroomGroupManager.getInstance().getFlatGroupDeviceList();

        List<SpeakerModel> homeSpeakers = mSpeakersBuilder.buildHomeSpeakersFromMultiroomModels(multiroomItemModels, mActivity.getApplicationContext());

        mAdapter.swapItems(homeSpeakers);
    }

    public void dispose() {
        mActivity = null;
        if (mRecyclerView != null) {
            mRecyclerView.setAdapter(null);
            mRecyclerView.clearOnChildAttachStateChangeListeners();
            mRecyclerView.clearOnScrollListeners();
            mRecyclerView.getRecycledViewPool().clear();
            mRecyclerView.removeAllViewsInLayout();
            mRecyclerView = null;
        }
        if (mAdapter != null) {
            mAdapter.dispose();
            mAdapter = null;
        }
    }
}
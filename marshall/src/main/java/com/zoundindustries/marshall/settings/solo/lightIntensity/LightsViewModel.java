package com.zoundindustries.marshall.settings.solo.lightIntensity;

import android.app.Activity;
import android.databinding.BaseObservable;
import android.databinding.Observable;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.SeekBar;

import com.apps_lib.multiroom.widgets.NoJumpSeekBar;
import com.frontier_silicon.NetRemoteLib.NetRemote;

/**
 * Created by cvladu on 07/06/2017.
 */

public class LightsViewModel extends BaseObservable implements SeekBar.OnSeekBarChangeListener, NoJumpSeekBar.IMotionEvent {
    private int virtualMaxSteps = 1300;
    private boolean isDragging = false;
    public ObservableInt seekValue = new ObservableInt(0);
    public ObservableInt maxSteps = new ObservableInt(0);
    public ObservableField<Drawable> ledIntensityImage = new ObservableField<>();
    public Observable.OnPropertyChangedCallback mPropertyChangedCallback;

    private Activity mActivity;
    private boolean isSeeking = false;
    private LightIntensityModel lightIntensityModel = LightIntensityManager.getInstance().getLightIntensityModel();
    private LightIntensityManager lightIntensityManager;


    public LightsViewModel() {

    }

    public void init(Activity activity) {
        mActivity = activity;
        lightIntensityManager = LightIntensityManager.getInstance();

        updateLightIntensitySteps();
        addListeners();
        updateSeekbarAndledIntensity();
    }

    private void addListeners() {
        mPropertyChangedCallback = new OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (observable == lightIntensityModel.lightIntensity && !isSeeking) {
                    updateSeekbarAndledIntensity();
                }
            }
        };

        lightIntensityModel.lightIntensity.addOnPropertyChangedCallback(mPropertyChangedCallback);

    }

    private void updateLightIntensitySteps() {
        maxSteps.set(lightIntensityManager.getStepsNumber());
    }

    private void updateSeekbarAndledIntensity() {
        decideLedAdjusterImage(lightIntensityManager.transformValueToSteps(lightIntensityModel.lightIntensity.get()));
        seekValue.set(lightIntensityManager.transformValueToSteps(lightIntensityModel.lightIntensity.get()));
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (!isSeeking) {
            return;
        }

        lightIntensityManager.transformStepsToValueAndSetLedIntensity(isDragging ? progress/100 : progress);
        setLedAdjusterValue(isDragging ? progress/100 : progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isSeeking = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isSeeking = false;
    }

    private void setLedAdjusterValue(int progress) {
        if (seekValue.get() == progress) {
            return;
        }

        decideLedAdjusterImage(progress);
    }

    private void decideLedAdjusterImage(int stepNumber) {
        int resource;

        if (mActivity == null) {
            return;
        }

        if (stepNumber <= 9) {
            resource = mActivity.getResources().getIdentifier("led_intensity_0" + stepNumber,
                    "drawable", mActivity.getPackageName());
            setImage(resource);
        } else if (stepNumber > 9 && stepNumber <= lightIntensityManager.getStepsNumber()) {
            resource = mActivity.getResources().getIdentifier("led_intensity_" + stepNumber,
                    "drawable", mActivity.getPackageName());
            setImage(resource);
        }

    }

    private void setImage(final int imgResource) {
        NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
            @Override
            public void run() {
                if(mActivity != null) {
                    Bitmap speakerImageBitmap = BitmapFactory.decodeResource(mActivity.getResources(), imgResource);
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(mActivity.getResources(), speakerImageBitmap);
                    ledIntensityImage.set(bitmapDrawable);
                }
            }
        });
    }

    private void removeListeners() {
        lightIntensityModel.lightIntensity.removeOnPropertyChangedCallback(mPropertyChangedCallback);
    }

    public void dispose() {
        removeListeners();

        lightIntensityManager = null;
        mActivity = null;
    }

    @Override
    public void onMoving(SeekBar seekBar) {
        isDragging = true;
        isSeeking = false;
        seekBar.setMax(virtualMaxSteps);
        isSeeking = true;

    }

    @Override
    public void onFinishMoving(SeekBar seekBar) {
        isDragging = false;
        isSeeking = false;
        seekBar.setMax(lightIntensityManager.getStepsNumber());
        isSeeking = true;
    }
}

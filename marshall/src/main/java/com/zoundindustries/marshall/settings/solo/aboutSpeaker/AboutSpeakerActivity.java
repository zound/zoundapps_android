package com.zoundindustries.marshall.settings.solo.aboutSpeaker;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.settings.RadioFromBundleExtractor;
import com.apps_lib.multiroom.setup.update.CheckForUpdateRunnable;
import com.apps_lib.multiroom.setup.update.ICheckForUpdateListener;
import com.zoundindustries.marshall.settings.solo.SettingsViewModel;
import com.apps_lib.multiroom.util.DisposableTimerTask;
import com.frontier_silicon.NetRemoteLib.Node.NodeCastVersion;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysInfoBuildVersion;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanConnectedSSID;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanMacAddress;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanRssi;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivitySettingsAboutSpeakerBinding;
import com.zoundindustries.marshall.settings.solo.speaker.SpeakerUpdateNoteFragment;

import java.util.Map;
import java.util.Timer;

/**
 * Created by lsuhov on 08/06/16.
 */
public class AboutSpeakerActivity extends UEActivityBase {

    public static final int EXTRA_SHIFT_FOR_WIFI_TEXT = 4;

    private Radio mRadio;
    private ActivitySettingsAboutSpeakerBinding mBinding;
    private String mWifiNetworkName;
    private Timer mTimer;
    private DisposableTimerTask mTimerTask;
    private SettingsViewModel mSettingsViewModel;
    private boolean mActivityWasCreated;
    private CheckForUpdateRunnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings_about_speaker);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.about_this_speaker_caps);

        mRadio = RadioFromBundleExtractor.extractRadio(getIntent().getExtras(), AboutSpeakerActivity.class);

        mSettingsViewModel = new SettingsViewModel(this, mRadio);

        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        setupControls();
        mActivityWasCreated = true;

        checkForUpdate();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSettingsViewModel.startListeningToDiscovery();

        if (mActivityWasCreated) {
            //not
            mActivityWasCreated = false;
            return;
        }
        startStrengthTimer();
    }

    @Override
    protected void onPause() {
        mSettingsViewModel.stopListening();
        stopStrengthTimer();

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (runnable != null) {
            runnable.resetListener();
        }

        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupControls() {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }
        setSpeakerName();

        RadioNodeUtil.getNodesFromRadioAsync(mRadio, new Class[]{ NodeCastVersion.class,
                        NodeSysNetWlanConnectedSSID.class, NodeSysNetWlanRssi.class, NodeSysNetWlanMacAddress.class, NodeSysInfoBuildVersion.class},
                false, new RadioNodeUtil.INodesResultListener() {
                    @Override
                    public void onNodesResult(Map<Class, NodeInfo> nodes) {
                        NodeSysNetWlanConnectedSSID ssidNode = (NodeSysNetWlanConnectedSSID) nodes.get(NodeSysNetWlanConnectedSSID.class);
                        if (ssidNode != null) {
                            mWifiNetworkName = RadioNodeUtil.getHumanReadableSSID(ssidNode.getValue());
                        }
                        NodeSysNetWlanRssi signalStrengthNode = (NodeSysNetWlanRssi) nodes.get(NodeSysNetWlanRssi.class);
                        if (signalStrengthNode != null) {
                            showWifiWithStrength(signalStrengthNode.getValue());
                        }

                        startStrengthTimer();

                        setIPAddress();
                        setMACAddress((NodeSysNetWlanMacAddress)nodes.get(NodeSysNetWlanMacAddress.class));
                        setModelName();
                        setFirmwareVersion((NodeSysInfoBuildVersion)nodes.get(NodeSysInfoBuildVersion.class));
                        setGoogleCastVersion((NodeCastVersion)nodes.get(NodeCastVersion.class));
                    }
                });

        setUpdateButton();
    }

    void setSpeakerName() {
        mBinding.speakerNameTextView.setText(
                getSpan(getResources().getString(R.string.speaker_name_with_value,
                        mRadio.getFriendlyName()),
                        getShiftForSpans(mRadio.getFriendlyName())));
    }

    private void setUpdateButton() {
        mBinding.buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpeakerUpdateNoteFragment speakerUpdateNoteFragment = SpeakerUpdateNoteFragment.newInstance(mSettingsViewModel);
                FragmentManager fragmentManager = getSupportFragmentManager();
                speakerUpdateNoteFragment.show(fragmentManager, "OldUpdateWarningDialogFragment");
            }
        });
    }

    private void setGoogleCastVersion(NodeCastVersion nodeCastVersion) {
        if (nodeCastVersion != null) {

            mBinding.googleCastVersionTextView.setText(
                    getSpan(getString(R.string.chromecast_built_in_version_with_value,
                            nodeCastVersion.getValue()),
                            getShiftForSpans(nodeCastVersion.getValue())));

            mBinding.googleCastVersionTextView.setVisibility(View.VISIBLE);
        } else {
            mBinding.googleCastVersionTextView.setVisibility(View.GONE);
        }
    }

    private void setModelName() {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        mBinding.speakerModelTextView.setText(
                getSpan(getString(R.string.model_name_with_value,
                        getModelNameWithoutUnderscore(mRadio.getModelName())),
                        getModelNameWithoutUnderscore(mRadio.getModelName()).length()));
    }

    private String getModelNameWithoutUnderscore(String modelName) {
        StringBuilder builder = new StringBuilder();
        builder.append("Marshall ");

        if(modelName.contains("_")) {
            String[] array = modelName.split("_");
            int length = array.length;
            for(int i=0;i<length;i++) {
                if (i==0) {
                    builder.append( array[i] + " Multi-Room ");
                } else {
                    builder.append( array[i]);
                }
            }
            
        } else {
            builder.delete(0, builder.length());
            builder.append(modelName);
        }

        return  builder.toString();
    }

    private void setMACAddress(NodeSysNetWlanMacAddress nodeSysNetWlanMacAddress) {
        if (nodeSysNetWlanMacAddress == null) {
            return;
        }

        mBinding.speakerMACTextView.setText(
                getSpan(getString(R.string.mac_address_with_value,
                        nodeSysNetWlanMacAddress.getValue()),
                        getShiftForSpans(nodeSysNetWlanMacAddress.getValue())));
    }

    private void setIPAddress() {
        mBinding.speakerIpTextView.setText(
                getSpan(getString(R.string.ip_address_with_value,
                        mRadio.getIpAddress()),
                        getShiftForSpans(mRadio.getIpAddress())));
    }

    private void setFirmwareVersion(NodeSysInfoBuildVersion nodeSysInfoBuildVersion) {
        if (nodeSysInfoBuildVersion == null) {
            return;
        }

        mBinding.firmwareVersionTextView.setText(
                getSpan(getString(R.string.system_firmware_version_with_value,
                        nodeSysInfoBuildVersion.getValue()),
                        getShiftForSpans(nodeSysInfoBuildVersion.getValue())));
    }

    private void startStrengthTimer() {
        stopStrengthTimer();

        mTimerTask = new WiFiStrengthTimerTask();
        mTimer = new Timer();
        mTimer.schedule(mTimerTask, 3000, 3000);
    }

    private void stopStrengthTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask.dispose();
            mTimerTask = null;
        }
    }

    private class WiFiStrengthTimerTask extends DisposableTimerTask {

        @Override
        public void run() {
            if (mIsDisposed) {
                cancel();
                return;
            }

            if (!AboutSpeakerActivity.this.isFinishing()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getWifiStrengthAndShowIt();
                    }
                });
            }
        }
    }

    private void getWifiStrengthAndShowIt() {
        if (!isFinishing()) {
            if (mSettingsViewModel.startHomeActivityIfNeeded()) {
                return;
            }
            RadioNodeUtil.getNodeFromRadioAsync(mRadio, NodeSysNetWlanRssi.class, new RadioNodeUtil.INodeResultListener() {
                @Override
                public void onNodeResult(NodeInfo node) {
                    NodeSysNetWlanRssi signalStrengthNode = (NodeSysNetWlanRssi)node;
                    if (signalStrengthNode != null) {
                        showWifiWithStrength(signalStrengthNode.getValue());
                    }
                }
            });
        }
    }

    private void showWifiWithStrength(long rawWiFiStrength) {
        mBinding.wifiNetworkNameTextView.setText(
                getSpan(getString(R.string.wifi_network_name_with_value,
                        mWifiNetworkName,
                        rawWiFiStrength),
                        getSpanShiftForWifiStrength(rawWiFiStrength)));
    }

    private int getSpanShiftForWifiStrength(long rawWiFiStrength) {
        return getShiftForSpans(mWifiNetworkName) +
                Long.toString(rawWiFiStrength).length() + EXTRA_SHIFT_FOR_WIFI_TEXT;
    }

    private int getShiftForSpans(String resourceArgument) {
        return resourceArgument == null ? 0 : resourceArgument.length();
    }

    private Spannable getSpan(String textWithValue, int shiftValue) {
        Spannable spannable = new SpannableString(textWithValue);

        spannable.setSpan(
                new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)),
                spannable.length() - shiftValue,
                spannable.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannable;
    }

    private void checkForUpdate() {
        runnable = new CheckForUpdateRunnable(mRadio, new ICheckForUpdateListener() {
            @Override
            public void onAvailableUpdate() {
                runOnUiThread(() -> {
                    if (mBinding != null) {
                        mBinding.buttonUpdate.setVisibility(View.VISIBLE);
                    }
                });
            }

            @Override
            public void onNoAvailableUpdate() {
            }

            @Override
            public void onCheckFailed() {
            }
        });
        new Thread(runnable).start();
    }
}

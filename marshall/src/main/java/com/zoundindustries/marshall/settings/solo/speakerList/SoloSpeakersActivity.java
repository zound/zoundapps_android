package com.zoundindustries.marshall.settings.solo.speakerList;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.util.SortedList;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.myHome.SpeakersDiscoveryManager;
import com.apps_lib.multiroom.settings.solo.speakerList.RadioSelectedEvent;
import com.apps_lib.multiroom.speakerImages.ESpeakerModelType;
import com.apps_lib.multiroom.speakerImages.SpeakerTypeDecoder;
import com.frontier_silicon.NetRemoteLib.Discovery.IRadioDiscoveryListener;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivitySettingsSpeakerListBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nbalazs on 09/06/2017.
 */

public class SoloSpeakersActivity extends UEActivityBase implements IRadioDiscoveryListener {

    private ActivitySettingsSpeakerListBinding mBinding;
    private SoloSpeakersAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings_speaker_list);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.solo_speakers_activity_title);

        setupControls();
    }

    private void setupControls() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        mAdapter = new SoloSpeakersAdapter(this);

        mBinding.listOfSpeakers.setHasFixedSize(true);
        mBinding.listOfSpeakers.setLayoutManager(layoutManager);
        mBinding.listOfSpeakers.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {

        NetRemoteManager.getInstance().addRadioDiscoveryListener(this);
        SpeakersDiscoveryManager.getInstance().tryStartDiscovery();
        updateAdapter();

        EventBus.getDefault().register(this);

        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        NetRemoteManager.getInstance().removeRadioDiscoveryListener(this);
        EventBus.getDefault().unregister(this);
    }

    private void updateAdapter() {
        List<Radio> radios = NetRemoteManager.getInstance().getRadios();
        List<Radio> theFinalList = new ArrayList<>();

        for (Radio radio : radios) {
            if (SpeakerTypeDecoder.getMarshallSpeakerModel(radio.getModelName()) != ESpeakerModelType.Unknown) {
                theFinalList.add(radio);
            }
        }

        mAdapter.mSortedRadios.beginBatchedUpdates();
        mAdapter.mSortedRadios.clear();
        mAdapter.mSortedRadios.addAll(theFinalList);
        mAdapter.mSortedRadios.endBatchedUpdates();
    }

    @Override
    public void onRadioFound(final Radio radio) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SpeakerTypeDecoder.getMarshallSpeakerModel(radio.getModelName()) != ESpeakerModelType.Unknown) {
                        mAdapter.mSortedRadios.add(radio);
                    }
                }
            });
        }
    }


    @Override
    public void onRadioLost(final Radio radio) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAdapter.mSortedRadios.remove(radio);
                }
            });
        }
    }

    @Override
    public void onRadioUpdated(final Radio radio) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final int position = mAdapter.mSortedRadios.indexOf(radio);
                    if (position == SortedList.INVALID_POSITION) {
                        return;
                    }
                    mAdapter.mSortedRadios.updateItemAt(position, radio);
                }
            });
        }
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void onSpeakerClicked(RadioSelectedEvent event) {
        if (event.radio != null) {
            Bundle bundle = new Bundle();
            bundle.putString(NavigationHelper.RADIO_UDN_ARG, event.radio.getUDN());

            NavigationHelper.goToActivity(this, ActivityFactory.getActivityFactory().getSettingsActivity(), NavigationHelper.AnimationType.SlideToLeft,
                    false, bundle);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
package com.zoundindustries.marshall.settings.solo.speaker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apps_lib.multiroom.settings.solo.speaker.SpeakerSettingModel;
import com.zoundindustries.marshall.R;

import java.util.List;

/**
 * Created by lsuhov on 07/06/16.
 */
public class SettingsSpeakerAdapter extends ArrayAdapter<SpeakerSettingModel> {

    private int mLayoutId;

    public SettingsSpeakerAdapter(Context context, int resource, List<SpeakerSettingModel> objects) {
        super(context, resource, objects);

        mLayoutId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        Context context = getContext();

        SpeakerSettingModel settingModel = getItem(position);

        if (v == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(mLayoutId, null);
        }

        TextView textView = (TextView) v.findViewById(R.id.settingTextView);
        textView.setText(settingModel.name);

        return v;
    }
}

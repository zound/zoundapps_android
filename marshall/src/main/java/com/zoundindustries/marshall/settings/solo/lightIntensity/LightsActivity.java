package com.zoundindustries.marshall.settings.solo.lightIntensity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;

import com.apps_lib.multiroom.UEActivityBase;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivityLedAdjusterBinding;

/**
 * Created by cvladu on 07/06/2017.
 */

public class LightsActivity extends UEActivityBase {
    ActivityLedAdjusterBinding mBinding;
    LightsViewModel viewModel = new LightsViewModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_led_adjuster);
        setupAppBar();
        enableUpNavigation();
        setTitle(getString(R.string.settings_lights));

        mBinding.setViewModel(viewModel);
        mBinding.ledAdjusterSeekBar.setOnSeekBarChangeListener(viewModel);
        mBinding.ledAdjusterSeekBar.setMotionListener(viewModel);
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.init(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        viewModel.dispose();
    }
}

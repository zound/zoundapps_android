package com.zoundindustries.marshall.settings.multi.streamingQuality;

import android.app.Activity;
import android.databinding.ObservableBoolean;
import android.view.View;
import android.widget.Toast;

import com.apps_lib.multiroom.multiroom.MultiroomRebuildGroups;
import com.apps_lib.multiroom.settings.multi.streamingQuality.StreamingQualityMgr;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivityStreamingQualityBinding;

import java.util.List;

/**
 * Created by nbalazs on 06/12/2016.
 */

public class StreamingQualityViewModel implements StreamingQualityMgr.IStreamingQualityChangeListener{
    private Activity mActivity;
    private ActivityStreamingQualityBinding mBinding;
    public ObservableBoolean isHighStreamingQualityChecked = new ObservableBoolean(false);

    StreamingQualityViewModel(Activity activity, ActivityStreamingQualityBinding binding) {
        mActivity = activity;
        mBinding = binding;

        initLayout();
    }

    public void onHighStreamingQualityButtonClicked(View view) {
        if (view.getId() == mBinding.textViewHighRadioBtn.getId() || view.getId() == mBinding.radioButtonHigh.getId()) {
            isHighStreamingQualityChecked.set(true);
        }
        writeNodeMultiroomDeviceTransportOptimisation(false);   // set optimization disabled
    }

    public void onLowStreamingQualityButtonClicked(View view) {
        if (view.getId() == mBinding.textViewLowRadioBtn.getId() || view.getId() == mBinding.radioButtonLow.getId()) {
          isHighStreamingQualityChecked.set(false);
        }
        writeNodeMultiroomDeviceTransportOptimisation(true);    // set optimization enabled
    }

    private void initLayout() {
        List<Radio> radios = NetRemoteManager.getInstance().getRadios();
        boolean checkHigh = !StreamingQualityMgr.getInstance().getGeneralOptimizationStatus(radios);
       isHighStreamingQualityChecked.set(checkHigh);
    }

    private void writeNodeMultiroomDeviceTransportOptimisation(boolean optimizationEnabled) {
        List<Radio> radios = NetRemoteManager.getInstance().getRadios();
        StreamingQualityMgr.getInstance().changeStreamingQuality(radios, this, optimizationEnabled);
    }

    @Override
    public void onStreamingQualityChanged(boolean success) {
        if (success) {
            MultiroomRebuildGroups.getInstance().rebuildMultiroomGroupsAndDevices();
        } else {
            Toast.makeText(mActivity, mActivity.getString(R.string.optimisation_error_msg), Toast.LENGTH_SHORT).show();
        }
    }
}

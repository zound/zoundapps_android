package com.zoundindustries.marshall.source;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.FragmentSourceAuxinBinding;

/**
 * Created by lsuhov on 11/05/16.
 */
public class AuxinSourceFragment extends SimpleSourceFragment {

    private FragmentSourceAuxinBinding mBinding;
    private AuxinSourceViewModel mViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_source_auxin, container, false);

        mViewModel = new AuxinSourceViewModel(this);
        mViewModel.init();
        mBinding.setViewModel(mViewModel);

        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        if (mBinding != null) {
            if (mBinding.getViewModel() != null) {
                mBinding.getViewModel().dispose();
            }
            mBinding = null;
        }

        super.onDestroyView();
    }

    @Override
    protected void setViewClickable(boolean clickable) {
        mViewModel.setViewClickable(clickable);
    }
}

package com.zoundindustries.marshall.source;

import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;

import com.apps_lib.multiroom.presets.PresetsManager;
import com.apps_lib.multiroom.source.SourceFragment;

/**
 * Created by nbalazs on 21/01/2017.
 */
class SourcesInteractionHandler implements ViewPager.OnPageChangeListener, ViewPager.OnTouchListener {

    private ViewPager mViewPager;
    private SourcesAdapter mAdapter;
    private boolean mIsFirstRun;
    private boolean mLeftToRight;
    private boolean mRightToLeft;
    private float mStartX;


    SourcesInteractionHandler(ViewPager viewPager, SourcesAdapter sourcesAdapter) {
        mViewPager = viewPager;
        mAdapter = sourcesAdapter;
        mIsFirstRun = true;
        mLeftToRight = true;
        mRightToLeft = true;
        mStartX = 0.0f;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mIsFirstRun) {
            mIsFirstRun = false;
            int selectedPosition = mViewPager.getCurrentItem();

            SourceFragment previousFragment = mAdapter.getResgisteredFragment(selectedPosition - 1);
            SourceFragment currentFragment = mAdapter.getResgisteredFragment(selectedPosition);
            SourceFragment nextFragment = mAdapter.getResgisteredFragment(selectedPosition + 1);

            currentFragment.setFragmentAlphaForPosition(SourceFragment.EFragmentPosition.PRIMARY);

            if (currentFragment.getFragmentType() == SourceFragment.ESourceFragmentType.PRESET_SOURCE_FRAGMENT) {
                if (previousFragment.getFragmentType() == SourceFragment.ESourceFragmentType.PRESET_SOURCE_FRAGMENT) {
                    previousFragment.setFadeOutAlpha(SourceFragment.LAYOUT_ALPHA_25, true);
                } else {
                    previousFragment.setFadeOutAlpha(SourceFragment.LAYOUT_ALPHA_0, true);
                }

                if (nextFragment.getFragmentType() == SourceFragment.ESourceFragmentType.PRESET_SOURCE_FRAGMENT) {
                    nextFragment.setFadeOutAlpha(SourceFragment.LAYOUT_ALPHA_25, true);
                } else {
                    nextFragment.setFadeOutAlpha(SourceFragment.LAYOUT_ALPHA_0, true);
                }
            } else {
                previousFragment.setFadeOutAlpha(SourceFragment.LAYOUT_ALPHA_0, true);
                nextFragment.setFadeOutAlpha(SourceFragment.LAYOUT_ALPHA_0, true);
            }

            previousFragment.setScaleToOffset(0f, SourceFragment.EFragmentPosition.SECONDARY);
            currentFragment.setScaleToOffset(0f, SourceFragment.EFragmentPosition.PRIMARY);
            nextFragment.setScaleToOffset(0f, SourceFragment.EFragmentPosition.SECONDARY);
        } else {
            SourceFragment currentFragment = mAdapter.getResgisteredFragment(position);
            currentFragment.setFragmentAlphaForOffset(positionOffset, SourceFragment.EFragmentPosition.PRIMARY);
            currentFragment.setScaleToOffset(positionOffset, SourceFragment.EFragmentPosition.PRIMARY);

            SourceFragment previousFragment = mAdapter.getResgisteredFragment(position + 1);
            previousFragment.setFragmentAlphaForOffset(positionOffset, SourceFragment.EFragmentPosition.SECONDARY);
            previousFragment.setScaleToOffset(positionOffset, SourceFragment.EFragmentPosition.SECONDARY);

            int maxOffsetPixels = (int) ((float) positionOffsetPixels / positionOffset);
            currentFragment.onOffsetChanged(positionOffset, positionOffsetPixels, mLeftToRight, mRightToLeft);
            previousFragment.onOffsetChanged(positionOffset, -(maxOffsetPixels - positionOffsetPixels), mLeftToRight, mRightToLeft);
        }
    }

    @Override
    public void onPageSelected(int position) {
        setAlpha(position);
        PresetsManager.getInstance().setCurrentTabIndex(position % 10);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == ViewPager.SCROLL_STATE_IDLE) {
            int position = mViewPager.getCurrentItem();

            SourceFragment previoseFragment = mAdapter.getResgisteredFragment(position - 1);
            previoseFragment.setFragmentAlphaForPosition(SourceFragment.EFragmentPosition.SECONDARY);

            SourceFragment currentFragment = mAdapter.getResgisteredFragment(position);
            currentFragment.setFragmentAlphaForPosition(SourceFragment.EFragmentPosition.PRIMARY);

            SourceFragment nextFragment = mAdapter.getResgisteredFragment(position + 1);
            nextFragment.setFragmentAlphaForPosition(SourceFragment.EFragmentPosition.SECONDARY);

            setAlpha(position);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mStartX = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                float endX = event.getX();
                mLeftToRight = endX - mStartX > 0;
                mRightToLeft = endX - mStartX < 0;
                break;
        }
        return false;
    }

    private void setAlpha(int position) {
        SourceFragment previousFragment = null;
        SourceFragment currentFragment = null;
        SourceFragment nextFragmentInThatDirection = null;

        if (mLeftToRight) {
            previousFragment = mAdapter.getResgisteredFragment(position + 1);
            currentFragment = mAdapter.getResgisteredFragment(position);
            nextFragmentInThatDirection = mAdapter.getResgisteredFragment(position - 1);
        } else if (mRightToLeft) {
            previousFragment = mAdapter.getResgisteredFragment(position - 1);
            currentFragment = mAdapter.getResgisteredFragment(position);
            nextFragmentInThatDirection = mAdapter.getResgisteredFragment(position + 1);
        }

        if (previousFragment == null || currentFragment == null || nextFragmentInThatDirection == null) {
            return;
        }

        switch (currentFragment.getFragmentType()) {
            case PRESET_SOURCE_FRAGMENT:
                previousFragment.setFadeOutAlpha(SourceFragment.LAYOUT_ALPHA_25, true);
                nextFragmentInThatDirection.setFadeOutAlpha(SourceFragment.LAYOUT_ALPHA_25, true);
                break;
            case SIMPLE_SOURCE_FRAGMENT:
                previousFragment.setFadeOutAlpha(SourceFragment.LAYOUT_ALPHA_0, true);
                nextFragmentInThatDirection.setFadeOutAlpha(SourceFragment.LAYOUT_ALPHA_0, true);
                break;
            default:
                break;
        }
    }
}

package com.zoundindustries.marshall.source;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.apps_lib.multiroom.source.SourceFragment;
import com.apps_lib.multiroom.source.SourceTab;
import com.apps_lib.multiroom.source.SourcesRegistry;

public class SourcesAdapter extends FragmentPagerAdapter {

    private SparseArray<Fragment> mRegisteredFragments = new SparseArray<>();

    SourcesAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        int virtualPosition = SourcesRegistry.getInstance().getVirtualPosition(position);

        Fragment newFragment = null;
        SourceTab sourceTab = SourcesRegistry.getInstance().getSourceTabBasedOnVirtualPosition(virtualPosition);

        switch (sourceTab) {
            case PresetTab:
                newFragment = new PresetSourceFragment();

                Bundle bundle = new Bundle();
                bundle.putInt(PresetSourceFragment.PRESET_INDEX_KEY, virtualPosition);

                newFragment.setArguments(bundle);
                break;
            case AuxinTab:
                newFragment = new AuxinSourceFragment();
                break;
            case RCATab:
                newFragment = new RCASourceFragment();
                break;
            case BluetoothTab:
                newFragment = new BluetoothSourceFragment();
                break;
            case CloudTab:
                newFragment = new CloudSourceFragment();
                break;
        }

        return newFragment;
    }

    @Override
    public int getCount() {
        return SourcesRegistry.getInstance().getTotalPageNumbers();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        mRegisteredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        mRegisteredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    SourceFragment getResgisteredFragment(int position) {
        return (SourceFragment) mRegisteredFragments.get(position);
    }
}

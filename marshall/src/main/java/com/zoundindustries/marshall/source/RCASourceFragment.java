package com.zoundindustries.marshall.source;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.FragmentSourceRcaBinding;

/**
 * Created by nbalazs on 16/05/2017.
 */

public class RCASourceFragment extends SimpleSourceFragment {

    private FragmentSourceRcaBinding mBinding;
    private RCASourceViewModel mViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_source_rca, container, false);

        mViewModel = new RCASourceViewModel(this);
        mViewModel.init();
        mBinding.setViewModel(mViewModel);

        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        if (mBinding != null) {
            if (mBinding.getViewModel() != null) {
                mBinding.getViewModel().dispose();
            }
            mBinding = null;
        }

        super.onDestroyView();
    }

    @Override
    protected void setViewClickable(boolean clickable) {
        mViewModel.setViewClickable(clickable);
    }
}
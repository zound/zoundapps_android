package com.zoundindustries.marshall.castTos;

import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.frontier_silicon.components.common.CommonPreferences;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivityIntroductionBinding;

/**
 * Created by lsuhov on 30/05/16.
 */
public class CastTosIntroductionActivity extends UEActivityBase {

    private ActivityIntroductionBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_introduction);

        setupControls();

        startAnimation();
    }

    private void startAnimation() {
        View titleView = findViewById(R.id.introductionTitle);
        View buttonView = findViewById(R.id.buttonAccept);
        View tosIntroductionView = findViewById(R.id.textTosIntroduction);

        Animation animTo100 = AnimationUtils.loadAnimation(this, R.anim.tos_anim_fade_in_100);
        Animation animTo90 = AnimationUtils.loadAnimation(this, R.anim.tos_anim_fade_in_90);

        titleView.startAnimation(animTo100);
        buttonView.startAnimation(animTo90);
        tosIntroductionView.startAnimation(animTo100);
    }


    private void setupControls() {
        setupAcceptButton();
    }

    private void setupAcceptButton() {
        mBinding.buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonPreferences.getInstance().setCastTosAcceptance(true);
                NavigationHelper.goToActivity(CastTosIntroductionActivity.this, ActivityFactory.getActivityFactory().getHomeActivity(), NavigationHelper.AnimationType.SlideToLeft);
                finish();
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        } else {
            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);
            System.exit(0);
        }
    }
}

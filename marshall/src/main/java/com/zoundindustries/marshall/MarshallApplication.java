package com.zoundindustries.marshall;

import com.apps_lib.multiroom.MainApplication;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.factory.FragmentFactory;
import com.apps_lib.multiroom.persistence.PersistenceMgr;
import com.apps_lib.multiroom.presets.spotify.SpotifyManager;
import com.apps_lib.multiroom.setup.presets.DefaultIRPresetListCreatorTask;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.zoundindustries.marshall.factory.MarshallActivityCreator;
import com.zoundindustries.marshall.factory.MarshallFragmentCreator;
import com.zoundindustries.marshall.speakerImages.MarshallSpeakerImageRetriever;
import com.zoundindustries.marshall.speakerImages.MarshallSpeakerTypeDecoder;

/**
 * Created by nbalazs on 03/05/2017.
 */

public class MarshallApplication extends MainApplication{

    private final String PRESET_SERVER_URL = "https://bitbucket.org/zoundindustries/radiopresets/src/master/marshall_radio_presets.json";

    private final String REDIRECT_URL = "marshall://spotify-callback";
    private final String CLIENT_ID = "a1eab887f6f440fba0d6c53a2b24c024";
    private final String SERVER_ENDPOINT = "https://marshallapp-dev-spotify.herokuapp.com";

    @Override
    public final ECurrentApplication getCurrentApplication() {
        return ECurrentApplication.MARSHALL_APP;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        PersistenceMgr.getInstance().init(this);
        SpeakerManager.init(new MarshallSpeakerTypeDecoder(), new MarshallSpeakerImageRetriever());
        ActivityFactory.init(new MarshallActivityCreator());
        FragmentFactory.init(new MarshallFragmentCreator());
        VibratorService.init(this);
        DefaultIRPresetListCreatorTask.init(PRESET_SERVER_URL, R.raw.radio_presets);
        SpotifyManager.getInstance().init(getApplicationContext(), REDIRECT_URL, CLIENT_ID, SERVER_ENDPOINT);
    }
}

<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <variable
            name="viewModel"
            type="com.zoundindustries.marshall.volume.VolumeSpeakerViewModel" />
    </data>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:gravity="center"
            android:orientation="horizontal">

            <ImageView
                android:id="@+id/speakerIcon"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_weight="20"
                android:scaleType="fitCenter"
                android:src="@{viewModel.speakerImage}"
                tools:ignore="ContentDescription"
                tools:src="@drawable/ic_marshall_placeholder_volume" />

            <LinearLayout
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_weight="50"
                android:orientation="vertical">

                <TextView
                    android:id="@+id/speakerName"
                    style="@style/MarshallVolumeScreenSpeakerName"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:ellipsize="end"
                    android:maxLines="1"
                    android:text="@{viewModel.radioName}"
                    android:textAllCaps="true"
                    tools:text="LIVINGROOM" />

                <TextView
                    android:id="@+id/soloMulti"
                    style="@style/MarshallVolumeScreenSoloMultiLabel"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:ellipsize="end"
                    android:maxLines="1"
                    android:text="@{viewModel.soloMulti}"
                    tools:text="Multi" />

            </LinearLayout>

        </LinearLayout>

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:alpha="@{viewModel.seekbarsOpacity}"
            android:orientation="vertical">

            <LinearLayout
                android:id="@+id/volumeSeekBarContainerLayout"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:alpha="@{viewModel.volumeSeekbarOpacity}"
                android:orientation="vertical">

                <TextView
                    style="@style/MarshallVolumeScreenSeekBarLabel"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginEnd="@dimen/default_margin"
                    android:layout_marginLeft="@dimen/default_margin"
                    android:layout_marginRight="@dimen/default_margin"
                    android:layout_marginStart="@dimen/default_margin"
                    android:gravity="end"
                    android:text="@string/volume_volume"
                    tools:text="Volume" />

                <com.apps_lib.multiroom.widgets.NoJumpSeekBar
                    android:id="@+id/volumeSeekBar"
                    style="@style/MarshallVolumeSeekBar"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:max="@{viewModel.volumeMaxLayoutValue}"
                    android:paddingBottom="@dimen/default_margin_very_small"
                    android:paddingTop="@dimen/default_margin_very_small"
                    android:progress="@{viewModel.volumeSeekLayoutValue}"
                    app:extra_space_on_thumb="true" />

            </LinearLayout>

            <LinearLayout
                android:id="@+id/animatedLayout"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical"
                android:visibility="gone">

                <LinearLayout
                    android:id="@+id/bassSeekBarContainerLayout"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:gravity="center"
                    android:orientation="vertical">

                    <TextView
                        style="@style/MarshallVolumeScreenSeekBarLabel"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginEnd="@dimen/default_margin"
                        android:layout_marginLeft="@dimen/default_margin"
                        android:layout_marginRight="@dimen/default_margin"
                        android:layout_marginStart="@dimen/default_margin"
                        android:gravity="end"
                        android:text="@string/volume_bass"
                        tools:text="Bass" />

                    <com.apps_lib.multiroom.widgets.NoJumpSeekBar
                        android:id="@+id/bassSeekBar"
                        style="@style/MarshallVolumeSeekBar"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:max="@{viewModel.eqMaxLayoutStep}"
                        android:paddingBottom="@dimen/default_margin_small"
                        android:paddingTop="@dimen/default_margin_small"
                        android:progress="@{viewModel.eqBassLayoutProgress}"
                        app:extra_space_on_thumb="true" />

                </LinearLayout>

                <LinearLayout
                    android:id="@+id/trebleSeekBarContainerLayout"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="vertical">

                    <TextView
                        style="@style/MarshallVolumeScreenSeekBarLabel"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginEnd="@dimen/default_margin"
                        android:layout_marginLeft="@dimen/default_margin"
                        android:layout_marginRight="@dimen/default_margin"
                        android:layout_marginStart="@dimen/default_margin"
                        android:gravity="end"
                        android:text="@string/volume_treble"
                        tools:text="Treble" />

                    <com.apps_lib.multiroom.widgets.NoJumpSeekBar
                        android:id="@+id/trebleSeekBar"
                        style="@style/MarshallVolumeSeekBar"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:max="@{viewModel.eqMaxLayoutStep}"
                        android:paddingBottom="@dimen/default_margin_small"
                        android:paddingTop="@dimen/default_margin_small"
                        android:progress="@{viewModel.eqTrebleLayoutProgress}"
                        app:extra_space_on_thumb="true" />

                </LinearLayout>

            </LinearLayout>

        </LinearLayout>

        <Button
            android:id="@+id/buttonExpandEqualiser"
            style="@style/DefaultButtonSmallFilled"
            android:layout_width="wrap_content"
            android:layout_marginBottom="12dp"
            android:layout_marginLeft="@dimen/activity_horizontal_margin"
            android:layout_marginStart="@dimen/activity_horizontal_margin"
            android:onClick="@{viewModel::onEqButtonClicked}"
            android:paddingTop="7dp"
            android:text="@{viewModel.isEqualiserShowing ? @string/volume_collapse_eq : @string/volume_expand_eq}"
            tools:text="@string/volume_expand_eq" />

    </LinearLayout>

</layout>
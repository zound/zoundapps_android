package com.apps_lib.multiroom.myHome.speakers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.apps_lib.multiroom.persistence.PersistenceMgr;
import com.apps_lib.multiroom.setup.update.CheckForUpdateRunnable;
import com.apps_lib.multiroom.setup.update.ICheckForUpdateListener;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.components.multiroom.IMultiroomGroupingListener;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.frontier_silicon.components.multiroom.MultiroomItemModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by lsuhov on 22/06/16.
 */
public class SpeakersMediator implements IMultiroomGroupingListener {

    protected Activity mActivity;
    protected RecyclerView mRecyclerView;
    protected SpeakersAdapter mAdapter;
    protected SpeakersBuilder mSpeakersBuilder;

    private CheckTheListOfSpeakersForUpdateTask mCheckRunnable = null;

    public void init(Activity activity, RecyclerView recyclerView) {
        mActivity = activity;
        mRecyclerView = recyclerView;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity.getApplicationContext());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    public void attachToDiscovery() {
        FsLogger.log("Speakers Mediator: attach to discovery");
        detachFromDiscovery();

        MultiroomGroupManager.getInstance().addListener(this);

        updateListOfHomeSpeakers();
    }

    public void detachFromDiscovery() {
        MultiroomGroupManager.getInstance().removeListener(this);
    }

    @Override
    public void onGroupingUpdate() {
        FsLogger.log("Speakers Mediator grouping update");
        if (mActivity != null && !mActivity.isFinishing()) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateListOfHomeSpeakers();
                }
            });
        }
    }

    protected void updateListOfHomeSpeakers() {
        if (mActivity == null) {
            return;
        }

        List<MultiroomItemModel> multiroomItemModels = MultiroomGroupManager.getInstance().getFlatGroupDeviceList();

        List<SpeakerModel> homeSpeakers = mSpeakersBuilder.buildHomeSpeakersFromMultiroomModels(multiroomItemModels, mActivity.getApplicationContext());

        mAdapter.swapItems(homeSpeakers);

        checkForUpdatesIfNeeded(homeSpeakers);
    }

    public void dispose() {
        mActivity = null;
        if (mRecyclerView != null) {
            mRecyclerView.setAdapter(null);
            mRecyclerView.clearOnChildAttachStateChangeListeners();
            mRecyclerView.clearOnScrollListeners();
            mRecyclerView.getRecycledViewPool().clear();
            mRecyclerView.removeAllViewsInLayout();
            mRecyclerView = null;
        }
        if (mAdapter != null) {
            mAdapter.dispose();
            mAdapter = null;
        }
    }

    private void checkForUpdatesIfNeeded(List<SpeakerModel> homeList) {
        if (homeList == null) {
            return;
        }

        List<SpeakerModel> listOfSpeakersToBeCheckedForUpdate = new ArrayList<>();
        for (SpeakerModel speakerModel : homeList) {
            if (speakerModel != null && speakerModel.deviceModel != null) {
                Radio radio = speakerModel.deviceModel.mRadio;
                if (radio != null && (getDifferenceInDaysFromNowToMomentOfLastUpdateCheck(radio) >= 7 || isSpeakerUpdateCheckedForTheFirstTime(radio))) {
                    listOfSpeakersToBeCheckedForUpdate.add(speakerModel);
                }
            }
        }
        checkTheListOfSpeakersForSWUpdate(listOfSpeakersToBeCheckedForUpdate);
    }

    private void checkTheListOfSpeakersForSWUpdate(List<SpeakerModel> speakerModels) {
        if (speakerModels != null && speakerModels.size() == 0) {
            return;
        }

        if (mCheckRunnable != null) {
            return;
        }

        mCheckRunnable = new CheckTheListOfSpeakersForUpdateTask(speakerModels, new IUpdateCheckingFinishedListener() {
            @Override
            public void onCheckFinished(List<SpeakerModel> speakerModels) {
                List<String> serialNumbers = new ArrayList<>();
                for (SpeakerModel speakerModel : speakerModels) {
                    serialNumbers.add(speakerModel.deviceModel.mRadio.getSN());
                }
                PersistenceMgr.getInstance().saveListOfUpdatebleSpeaker(serialNumbers);
                mCheckRunnable = null;
            }
        });
        mCheckRunnable.execute();
    }

    private long getDifferenceInDaysFromNowToMomentOfLastUpdateCheck(Radio radio) {
        long daysDifference = 0;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = null;
        Date dateFromPreferences = null;

        String lastUpdateCheck = PersistenceMgr.getInstance().getMomentOfTheLastSpeakerUpdateCheck(radio.getSN());

        try {
            dateFromPreferences = simpleDateFormat.parse(lastUpdateCheck);
            currentDate = simpleDateFormat.parse(simpleDateFormat.format(Calendar.getInstance().getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (currentDate != null && dateFromPreferences != null) {
            long difference = currentDate.getTime() - dateFromPreferences.getTime();
            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            daysDifference = difference / daysInMilli;

        }

        return Math.abs(daysDifference);
    }

    private boolean isSpeakerUpdateCheckedForTheFirstTime(Radio radio) {
        String lastUpdatedDate = PersistenceMgr.getInstance().getMomentOfTheLastSpeakerUpdateCheck(radio.getSN());

        if (TextUtils.isEmpty(lastUpdatedDate)) {
            return true;
        } else {
            return false;
        }
    }

    private interface IUpdateCheckingFinishedListener {
        void onCheckFinished(List<SpeakerModel> speakerModels);
    }

    @SuppressLint("StaticFieldLeak")
    private class CheckTheListOfSpeakersForUpdateTask extends AsyncTask<Void, Void, Void> {
        private List<SpeakerModel> mSpeakerModels;
        private List<SpeakerModel> mUpdateAvailableSpeakerModels;
        private IUpdateCheckingFinishedListener mListener;

        private CheckTheListOfSpeakersForUpdateTask(List<SpeakerModel> speakerModels, IUpdateCheckingFinishedListener listener) {
            mSpeakerModels = speakerModels;
            mUpdateAvailableSpeakerModels = new ArrayList<>();
            mListener = listener;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            final CountDownLatch countDownLatch = new CountDownLatch(mSpeakerModels.size());
            for (final SpeakerModel speakerModel : mSpeakerModels) {
                if (mSpeakerModels != null && speakerModel.deviceModel != null && speakerModel.deviceModel.mRadio != null) {
                    CheckForUpdateRunnable runnable = new CheckForUpdateRunnable(speakerModel.deviceModel.mRadio, new ICheckForUpdateListener() {
                        @Override
                        public void onAvailableUpdate() {
                            mUpdateAvailableSpeakerModels.add(speakerModel);
                            setDateOfCheck(speakerModel);
                            countDownLatch.countDown();
                        }

                        @Override
                        public void onNoAvailableUpdate() {
                            setDateOfCheck(speakerModel);
                            countDownLatch.countDown();
                        }

                        @Override
                        public void onCheckFailed() {
                            countDownLatch.countDown();
                        }
                    });
                    Thread thread = new Thread(runnable);
                    thread.start();
                }
            }
            try {
                countDownLatch.await();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mListener != null) {
                mListener.onCheckFinished(mUpdateAvailableSpeakerModels);
            }
        }
    }

    private void setDateOfCheck(SpeakerModel speakerModel) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = Calendar.getInstance().getTime();
        PersistenceMgr.getInstance().setMomentOfTheLastSpeakerUpdateCheck(speakerModel.deviceModel.mRadio.getSN(), simpleDateFormat.format(currentDate));
    }
}

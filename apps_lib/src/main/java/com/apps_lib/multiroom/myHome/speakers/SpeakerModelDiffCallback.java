package com.apps_lib.multiroom.myHome.speakers;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;

import java.util.List;

/**
 * Created by lsuhov on 19/09/16.
 */

public class SpeakerModelDiffCallback extends DiffUtil.Callback {

    private List<SpeakerModel> mOldList;
    private List<SpeakerModel> mNewList;

    public SpeakerModelDiffCallback(List<SpeakerModel> oldList, List<SpeakerModel> newList) {
        mOldList = oldList;
        mNewList = newList;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    /**
     * Called by the DiffUtil to decide whether two object represent the same Item.
     * <p>
     * For example, if your items have unique ids, this method should check their id equality.
     *
     * @param oldItemPosition The position of the item in the old list
     * @param newItemPosition The position of the item in the new list
     * @return True if the two items represent the same object or false if they are different.
     */
    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        SpeakerModel oldSpeakerModel = mOldList.get(oldItemPosition);
        SpeakerModel newSpeakerModel = mNewList.get(newItemPosition);

        if (!isSpeakerModelTypeTheSame(oldSpeakerModel, newSpeakerModel)) {
            return false;
        }

        MultiroomDeviceModel oldDeviceModel = oldSpeakerModel.deviceModel;
        MultiroomDeviceModel newDeviceModel = newSpeakerModel.deviceModel;

        if (oldDeviceModel != null) {
            if (newDeviceModel != null) {
                return oldDeviceModel.equals(newDeviceModel);
            } else {
                return false;
            }
        } else {
            if (newDeviceModel != null) {
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * Called by the DiffUtil when it wants to check whether two items have the same data.
     * DiffUtil uses this information to detect if the contents of an item has changed.
     * <p>
     * DiffUtil uses this method to check equality instead of {@link Object#equals(Object)}
     * so that you can change its behavior depending on your UI.
     * For example, if you are using DiffUtil with a
     * {@link RecyclerView.Adapter RecyclerView.Adapter}, you should
     * return whether the items' visual representations are the same.
     * <p>
     * This method is called only if {@link #areItemsTheSame(int, int)} returns
     * {@code true} for these items.
     *
     * @param oldItemPosition The position of the item in the old list
     * @param newItemPosition The position of the item in the new list which replaces the
     *                        oldItem
     * @return True if the contents of the items are the same or false if they are different.
     */
    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        SpeakerModel oldSpeakerModel = mOldList.get(oldItemPosition);
        SpeakerModel newSpeakerModel = mNewList.get(newItemPosition);

        if (!isSpeakerModelTypeTheSame(oldSpeakerModel, newSpeakerModel)) {
            return false;
        }

        if (oldSpeakerModel.isInMultiMode != newSpeakerModel.isInMultiMode) {
            return false;
        }

        MultiroomDeviceModel oldDeviceModel = oldSpeakerModel.deviceModel;
        MultiroomDeviceModel newDeviceModel = newSpeakerModel.deviceModel;

        if ((oldDeviceModel != null && newDeviceModel == null) ||
                (oldDeviceModel == null && newDeviceModel != null)) {
            return false;
        } else if (oldDeviceModel == null && newDeviceModel == null) {
            return true;
        }

        Radio oldRadio = null;
        Radio newRadio = null;

        if (oldDeviceModel != null) {
            oldRadio = oldDeviceModel.mRadio;
        }
        if (newDeviceModel != null) {
            newRadio = newDeviceModel.mRadio;
        }
        if (oldRadio == null) {
            if (newRadio != null) {
                return false;
            }
        } else {
            if (newRadio == null) {
                return false;
            }

            return oldRadio.areContentsTheSame(newRadio);
        }

        return true;
    }

    private boolean isSpeakerModelTypeTheSame(SpeakerModel oldSpeakerModel, SpeakerModel newSpeakerModel) {
        if (oldSpeakerModel.type != newSpeakerModel.type
                || oldSpeakerModel.name != newSpeakerModel.name) {
            return false;
        }
        return true;
    }
}

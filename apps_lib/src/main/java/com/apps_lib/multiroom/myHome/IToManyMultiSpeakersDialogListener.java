package com.apps_lib.multiroom.myHome;

/**
 * Created by nbalazs on 27/03/2017.
 */
public interface IToManyMultiSpeakersDialogListener {
    void onDialogFinished();
}

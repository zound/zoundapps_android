package com.apps_lib.multiroom.myHome.speakers;

import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.apps_lib.multiroom.speakerImages.ESpeakerModelType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nbalazs on 25/10/2016.
 */
public class SpeakerNameManager {

    private static SpeakerNameManager INSTANCE;

    private Map<ESpeakerModelType, Value> mSpeakerNameInventory;

    public static SpeakerNameManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SpeakerNameManager();
        }
        return INSTANCE;
    }

    private SpeakerNameManager() {
        mSpeakerNameInventory = new HashMap<>();
    }

    public void generateNameForModelList(List<SpeakerModel> speakerModels) {
        Collections.sort(speakerModels);
        for (SpeakerModel speakerModel : speakerModels) {
            ESpeakerModelType speakerModelType = SpeakerManager.getInstance().getSpeakerModelFromModelName(speakerModel.deviceModel.mRadio.getModelName());
            register(speakerModelType, speakerModel.deviceModel.mRadio.getUDN(), speakerModel.deviceModel.mRadio.getFriendlyName());
        }
    }

    public String generateNameForDevice(ESpeakerModelType speakerModelType, String key, String friendlyName) {
        register(speakerModelType, key, friendlyName);
        return mSpeakerNameInventory.get(speakerModelType).mMapOfInternalFriendlyNames.get(key);
    }

    public String getNameForRadio(Radio radio) {
        ESpeakerModelType speakerModelType = SpeakerManager.getInstance().getSpeakerModelFromModelName(radio.getModelName());
        Value registry = mSpeakerNameInventory.get(speakerModelType);
        return registry != null ? registry.mMapOfInternalFriendlyNames.get(radio.getUDN()) : null;
    }

    public void updateNameForRadio(Radio radio) {
        ESpeakerModelType speakerModelType = SpeakerManager.getInstance().getSpeakerModelFromModelName(radio.getModelName());

        register(speakerModelType, radio.getUDN(), radio.getFriendlyName());
    }

    private void register(ESpeakerModelType speakerModelType, String key, String friendlyName) {
        Value inventory = mSpeakerNameInventory.get(speakerModelType);
        if (inventory == null) {
            inventory = new Value();
        }
        inventory.generateEntry(key, friendlyName);

        mSpeakerNameInventory.put(speakerModelType, inventory);
    }

    private class Value {
        private Map<String, String> mMapOfInternalFriendlyNames;            // [UNIQUE KEY   --> InternalFriendlyName]

        private Map<String, Integer> mMapOfFriendlyNameCounters;            // [FriendlyName --> FriendlyNameCounter]

        private List<String> mlistOfExistingNames;                          // [List of all the available speaker names]

        private Value() {
            mMapOfInternalFriendlyNames = new HashMap<>();
            mMapOfFriendlyNameCounters = new HashMap<>();
            mlistOfExistingNames = new ArrayList<>();
        }

        private void generateEntry(String key, String radioName) {
            // There is already at least one object with this name but this radio object is not registered yet, do the registration
            if ((mMapOfFriendlyNameCounters.containsKey(radioName)) && (!mMapOfInternalFriendlyNames.containsKey(key))) {
                Integer counterObject = mMapOfFriendlyNameCounters.get(radioName);
                String internalName = generateName(radioName, counterObject++);
                mMapOfInternalFriendlyNames.put(key, internalName);
                mMapOfFriendlyNameCounters.put(radioName, counterObject);
                mlistOfExistingNames.add(internalName);
            } else if (!mMapOfInternalFriendlyNames.containsKey(key)) { // The speaker maybe exists, but it has a new name
                Integer counterObject = 2;
                mMapOfInternalFriendlyNames.put(key, radioName);
                mMapOfFriendlyNameCounters.put(radioName, counterObject);
                mlistOfExistingNames.add(radioName);
            } else if (mMapOfInternalFriendlyNames.containsKey(key)){
                mlistOfExistingNames.remove(mMapOfInternalFriendlyNames.get(key));
                Integer counterObject = mMapOfFriendlyNameCounters.get(radioName);
                if (counterObject == null) {
                    counterObject = 2;
                }
                if (!mlistOfExistingNames.contains(radioName)) {
                    mMapOfInternalFriendlyNames.put(key, radioName);
                    mlistOfExistingNames.add(radioName);
                } else {
                    int i = 2;
                    while (i <= counterObject) {
                        String possibleInternalFriendlyName = generateName(radioName, i++);
                        if (!mlistOfExistingNames.contains(possibleInternalFriendlyName)) {
                            mlistOfExistingNames.add(possibleInternalFriendlyName);
                            mMapOfInternalFriendlyNames.put(key, possibleInternalFriendlyName);
                            break;
                        }
                    }
                    if (i == counterObject) {
                        counterObject ++ ;
                        mMapOfFriendlyNameCounters.remove(radioName);
                        mMapOfFriendlyNameCounters.put(radioName, counterObject);
                    }
                }
            }
        }

        private String generateName(String radioName, Integer counterObject) {
            return radioName + " (" + counterObject + ")";
        }
    }
}

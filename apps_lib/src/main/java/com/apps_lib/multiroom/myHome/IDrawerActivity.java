package com.apps_lib.multiroom.myHome;

/**
 * Created by nbalazs on 12/05/2017.
 */

public interface IDrawerActivity {
    void refreshNavigationDrawer(boolean displaySpeakerSettings);
}

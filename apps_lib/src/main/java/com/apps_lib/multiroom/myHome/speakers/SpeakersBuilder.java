package com.apps_lib.multiroom.myHome.speakers;

import android.content.Context;
import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.frontier_silicon.components.multiroom.MultiroomItemModel;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by lsuhov on 22/06/16.
 */
public abstract class SpeakersBuilder {

    protected List<MultiroomItemModel> mMultiroomItemModels;
    protected Context mContext;
    protected List<SpeakerModel> mMultiHomeSpeakers = new ArrayList<>();
    protected List<SpeakerModel> mSoloHomeSpeakers = new ArrayList<>();
    protected List<SpeakerModel> mFinalListOfHomeSpeakerModels = new ArrayList<>();
    private boolean mDividerEnabled = false;

    public SpeakersBuilder(boolean dividerEnabled) {
        mDividerEnabled = dividerEnabled;
    }


    public List<SpeakerModel> buildHomeSpeakersFromMultiroomModels(List<MultiroomItemModel> multiroomItemModels, Context context) {

        mMultiroomItemModels = multiroomItemModels;
        mContext = context;

        putSpeakersInSoloAndMultiLists();

        generateFinalListOfHomeSpeakerModels();

        generateNameForSpeakers();

        return mFinalListOfHomeSpeakerModels;
    }

    private void putSpeakersInSoloAndMultiLists() {
        mFinalListOfHomeSpeakerModels.clear();
        mMultiHomeSpeakers.clear();
        mSoloHomeSpeakers.clear();

        for (MultiroomItemModel multiroomItemModel : mMultiroomItemModels) {
            if (multiroomItemModel.getIsGroupHeader()) {

                List<MultiroomDeviceModel> multiroomDeviceModels = MultiroomGroupManager.getInstance().
                        getAllDevicesForGroupId(multiroomItemModel.getGroupId());

                for (MultiroomDeviceModel multiroomDeviceModel : multiroomDeviceModels) {
                    Radio radio = multiroomDeviceModel.mRadio;
                    if (radio != null && radio.isMissing()) {
                        continue;
                    }

                    SpeakerModel homeSpeakerModel = new SpeakerModel();
                    homeSpeakerModel.deviceModel = multiroomDeviceModel;
                    homeSpeakerModel.isInMultiMode = true;
                    homeSpeakerModel.type = SpeakerType.Speaker;
                    homeSpeakerModel.name = radio != null ? radio.getFriendlyName() : "";

                    mMultiHomeSpeakers.add(homeSpeakerModel);
                }
            } else {

                String udn = null;
                Radio radio = multiroomItemModel.getRadio();
                if (radio != null && !radio.isMissing()) {
                    udn = radio.getUDN();
                }
                if (!TextUtils.isEmpty(udn)) {
                    MultiroomDeviceModel multiroomDeviceModel = MultiroomGroupManager.getInstance().getDeviceByUdn(udn);
                    if (multiroomDeviceModel == null) {
                        FsLogger.log("SpeakersBuilder: MultiroomDeviceModel was not found", LogLevel.Warning);
                        continue;
                    }

                    SpeakerModel homeSpeakerModel = new SpeakerModel();
                    homeSpeakerModel.deviceModel = multiroomDeviceModel;
                    homeSpeakerModel.isInMultiMode = false;
                    homeSpeakerModel.type = SpeakerType.Speaker;

                    mSoloHomeSpeakers.add(homeSpeakerModel);
                }
            }
        }

        sortList(mMultiHomeSpeakers);
        sortList(mSoloHomeSpeakers);
    }

    private void generateFinalListOfHomeSpeakerModels() {
        if (mMultiHomeSpeakers.size() > 0) {
            addOtherMultiModelsAfterHeader();
            mFinalListOfHomeSpeakerModels.addAll(mMultiHomeSpeakers);
            if (mDividerEnabled && mSoloHomeSpeakers.size() > 0) {
                addDividerModel();
            }
        }

        if (mSoloHomeSpeakers.size() > 0) {
            mFinalListOfHomeSpeakerModels.addAll(mSoloHomeSpeakers);
        }
    }

    public abstract void addOtherMultiModelsAfterHeader();

    private void addDividerModel() {
        SpeakerModel dividerModel = new SpeakerModel();
        dividerModel.type = SpeakerType.Divider;

        mFinalListOfHomeSpeakerModels.add(dividerModel);
    }

    private void sortList(List<SpeakerModel> list) {
        Collections.sort(list);
    }

    public List<SpeakerModel> switchSpeakerBetweenSoloMultiAndReturnFakeList(SpeakerModel speakerModel, boolean isChecked) {
        if (mFinalListOfHomeSpeakerModels.size() != 0) {
            mFinalListOfHomeSpeakerModels.clear();
        }
        SpeakerModel speakerModel1 = null;
        if (isChecked) {
            for (SpeakerModel s : mSoloHomeSpeakers) {
                if (s.deviceModel.equals(speakerModel.deviceModel)) {
                    speakerModel1 = s;
                }
            }
            mSoloHomeSpeakers.remove(speakerModel1);

            if (!mMultiHomeSpeakers.contains(speakerModel)) {
                mMultiHomeSpeakers.add(speakerModel);
            }
        } else {
            for (SpeakerModel s : mMultiHomeSpeakers) {
                if (s.deviceModel.equals(speakerModel.deviceModel)) {
                    speakerModel1 = s;
                }
            }
            mMultiHomeSpeakers.remove(speakerModel1);

            if (!mSoloHomeSpeakers.contains(speakerModel)) {
                mSoloHomeSpeakers.add(speakerModel);
            }
        }
        sortList(mSoloHomeSpeakers);
        sortList(mMultiHomeSpeakers);

        generateFinalListOfHomeSpeakerModels();

        return mFinalListOfHomeSpeakerModels;
    }

    private void generateNameForSpeakers() {
        List<SpeakerModel> allTheSpeakers = new ArrayList<>();
        allTheSpeakers.addAll(mMultiHomeSpeakers);
        allTheSpeakers.addAll(mSoloHomeSpeakers);
        SpeakerNameManager speakerNameManager = SpeakerNameManager.getInstance();
        speakerNameManager.generateNameForModelList(allTheSpeakers);
    }
}

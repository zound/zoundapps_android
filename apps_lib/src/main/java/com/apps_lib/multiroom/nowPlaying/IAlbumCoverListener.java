package com.apps_lib.multiroom.nowPlaying;

import android.graphics.Bitmap;

public interface IAlbumCoverListener {
	void onAlbumCoverUpdate(Bitmap albumCover);
}

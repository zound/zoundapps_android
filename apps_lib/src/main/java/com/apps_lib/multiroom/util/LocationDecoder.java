package com.apps_lib.multiroom.util;

import android.os.AsyncTask;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.loggerlib.LogLevel;

import org.json.JSONObject;

import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by nbalazs on 22/03/2017.
 */
public final class LocationDecoder {

    private static final String IPSTACK_API_KEY = "a40080b5a90fa8518a8faa4400f2fa8e";
    private static final String GEOIP_API_SERVER_URL = "http://api.ipstack.com/check?access_key="
            + IPSTACK_API_KEY;

    private static final String COUNTRY_CODE = "country_code";

    private static String theCountryCode = null;

    interface LocationListener {
        void onLocationReceived(String countryCode);
    }

    public static void getCountryCodeAsync(final LocationListener locationListener) {
        if (locationListener == null) {
            return;
        }

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                return theCountryCode == null ? getCountryCodeSync() : theCountryCode;
            }

            @Override
            protected void onPostExecute(String countryCode) {
                if (theCountryCode == null) {
                    theCountryCode = countryCode;
                }
                locationListener.onLocationReceived(theCountryCode);
            }
        }.execute();
    }

    public static String getCountryCodeSync(){
        if (theCountryCode == null) {
            JSONObject jsonObject = readURL(GEOIP_API_SERVER_URL);
            theCountryCode = "";
            if (jsonObject != null) {
                try {
                    theCountryCode = jsonObject.getString(COUNTRY_CODE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return theCountryCode;
    }

    private static JSONObject readURL(String httpLink) {
        JSONObject jsonObject = null;

        try {
            Request request = new Request.Builder().url(httpLink).build();
            Response response = NetRemote.getOkHttpClient().newCall(request).execute();
            if (response.isSuccessful()) {
                NetRemote.log(LogLevel.Info, "Response from " + httpLink);
                String body = new String(response.body().bytes());
                response.close();

                jsonObject = new JSONObject(body);
            } else {
                NetRemote.log(LogLevel.Error, "Request error at: " + httpLink + ", code :" + response.code());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
}

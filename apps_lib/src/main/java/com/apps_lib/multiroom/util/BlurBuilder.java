package com.apps_lib.multiroom.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;


/**
 * Created by lsuhov on 16/07/16.
 */
public class BlurBuilder {
    private static BlurBuilder ourInstance = new BlurBuilder();

    public static BlurBuilder getInstance() {
        return ourInstance;
    }

    private BlurBuilder() {
    }

    public Bitmap blurImage(float scale, Bitmap input, int radius)
    {
        int width = Math.round(input.getWidth() * scale);
        int height = Math.round(input.getHeight() * scale);

        if (radius < 1 || width < 1 || height < 1) {
            return (null);
        }

        input = Bitmap.createScaledBitmap(input, width, height, false);

        Bitmap bitmap = input.copy(input.getConfig(), true);


        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }

    private Bitmap getScreenshot(View view) {

        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = view.getDrawingCache();

        if (bitmap == null) {
            view.buildDrawingCache();
            bitmap = view.getDrawingCache();
        }

        return bitmap;
    }

    private Bitmap getScreenshotFromActivity(Activity activity) {
        View view = activity.getWindow().getDecorView();

        Bitmap screenshotBitmap = getScreenshot(view);

        if (screenshotBitmap == null) {
            return null;
        }

        int heightOfStatusBar = getHeightOfStatusBar(activity);
        int heightOfNavBar = getHeightOfNavBar(activity);

        Bitmap screenshotWithoutNavBar = Bitmap.createBitmap(screenshotBitmap, 0, heightOfStatusBar,
                screenshotBitmap.getWidth(), screenshotBitmap.getHeight() - heightOfNavBar - heightOfStatusBar);

        view.destroyDrawingCache();
        view.setDrawingCacheEnabled(false);

        return screenshotWithoutNavBar;
    }

    private int getHeightOfNavBar(Context context) {
        Resources resources = context.getResources();
        int result = 0;

        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private int getHeightOfStatusBar(Context context) {
        Resources resources = context.getResources();
        int result = 0;
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public BitmapDrawable getBlurredAndZoomedScreenshotOfActivity(Activity activity) {
        Bitmap screenshotBitmap = getScreenshotFromActivity(activity);

        if (screenshotBitmap == null) {
            return null;
        }

        //the dialog is fullscreen, but it doesn't cover the whole width of the screen
        int empiricalWidthToCrop = screenshotBitmap.getWidth() / 14;
        int empiricalHeightToCrop = screenshotBitmap.getHeight() / 14;

        Bitmap adjustedScreenshot = Bitmap.createBitmap(screenshotBitmap, empiricalWidthToCrop, empiricalHeightToCrop,
                12 * empiricalWidthToCrop, 11 * empiricalHeightToCrop);

        Bitmap blurredBitmap = BlurBuilder.getInstance().blurImage(0.2f, adjustedScreenshot, 25);

        return new BitmapDrawable(activity.getResources(), blurredBitmap);

    }

    public BitmapDrawable getBlurredScreenshotOfActivity(Activity activity, float blurFactor) {
        Bitmap screenshotBitmap = getScreenshotFromActivity(activity);

        if (screenshotBitmap == null) {
            return null;
        }

        Bitmap blurredBitmap = BlurBuilder.getInstance().blurImage(blurFactor, screenshotBitmap, 25);

        return new BitmapDrawable(activity.getResources(), blurredBitmap);
    }
}

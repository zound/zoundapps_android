package com.apps_lib.multiroom.util.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;

/*
 * Class used to simplify AnimationSet creation
 */
public class AnimationHelper {
    @NonNull
    public static AnimationSet getAnimationSet(Context context, int... resources) {
        AnimationSet animationSet = new AnimationSet(false);

        for(int res : resources) {
            animationSet.addAnimation(AnimationUtils.loadAnimation(context, res));
        }
        return animationSet;
    }

    @NonNull
    public static AnimationSet getAnimationSet(Context context,
                                               AnimationEndListener finishedListener,
                                               int... resources) {
        AnimationSet animationSet = new AnimationSet(false);

        for(int res : resources) {
            animationSet.addAnimation(AnimationUtils.loadAnimation(context, res));
        }
        animationSet.setAnimationListener(finishedListener);
        return animationSet;
    }

}

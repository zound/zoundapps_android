package com.apps_lib.multiroom.util.ui;

import com.apps_lib.multiroom.source.SourceFragment;

public class ButtonParamsHelper {

    public static float generateAlphaValue(float currentOffset,
                                           SourceFragment.EFragmentPosition
                                                             fragmentPosition) {
        float alpha = 0.0f;

        if (fragmentPosition == SourceFragment.EFragmentPosition.SECONDARY
                && currentOffset > 0.9f) {
            alpha = (10.0f * currentOffset) - 9.0f;
        } else if (fragmentPosition == SourceFragment.EFragmentPosition.PRIMARY
                && currentOffset < 0.1f) {
            alpha = (-10.0f * currentOffset) + 1.0f;
        }
        return alpha;
    }
}

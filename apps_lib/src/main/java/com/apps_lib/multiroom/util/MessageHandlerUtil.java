package com.apps_lib.multiroom.util;

import android.app.Activity;
import android.widget.EditText;

import com.frontier_silicon.components.common.ErrorSanitizerMessageType;
import com.frontier_silicon.components.common.FsComponentsConfig;
import com.apps_lib.multiroom.R;


/**
 * Created by cvladu on 07/02/17.
 */

public class MessageHandlerUtil {

    public static void handleMessageErrorType(Activity activity, ErrorSanitizerMessageType messageType, EditText editText) {
        switch (messageType) {
            case EmptyName:
                editText.setError(activity.getString(R.string.empty_friendly_name_not_allowed));
                break;
            case BiggerThanSupportedLimit:
                editText.setError(activity.getString(R.string.length_limit_of_friendly_name_was_exceeded, FsComponentsConfig.MAX_LENGTH_OF_FRIENDLY_NAME_WHEN_GOOGLE_CAST_IS_PRESENT));
                break;
            case CharacterNotAllowed:
                editText.setError(activity.getString(R.string.invalid_chars_were_entered_message));
                break;
            case WhiteSpaceAtBeginning:
                editText.setError(activity.getString(R.string.whitespace_not_allowed));
                break;
            case MultipleWhiteSpaces:
                editText.setError(activity.getString(R.string.multiple_whitespaces_not_allowed));
                break;
        }
    }
}

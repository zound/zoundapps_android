package com.apps_lib.multiroom.util.ui;

import android.view.animation.Animation;
/*
 * This interface is created to simplify calls to animationListener which only have onAnimationEnd
 * implemented and onAnimationStart and onAnimationRepeat with empty body.
 */
public interface AnimationEndListener extends Animation.AnimationListener {
    @Override
    default void onAnimationStart(Animation animation) {
        // empty body left on purpose
    }

    @Override
    default void onAnimationRepeat(Animation animation) {
        // empty body left on purpose
    }
}

package com.apps_lib.multiroom.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.apps_lib.multiroom.R;

public class PermissionsUtil {

    public static final int PERMISSIONS_REQUEST_COARSE_LOCATION = 1;
    private static final String PREF_PERMISSION_RATIONALE_SHOWN = "PREF_PERMISSION_RATIONALE_SHOWN";

    static public boolean requestLocationPermission(final Activity activity, boolean askUserForPermission) {
        boolean isPermissionGranted = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (askUserForPermission) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        setPermissionRationaleShown(activity, true);
                        showPermissionRationaleDialog(activity);
                    } else {
                        if (wasPermissionRationaleShown(activity)) {
                            showResetPermissionDialog(activity);
                        }

                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_COARSE_LOCATION);
                    }
                }
            } else {
                isPermissionGranted = true;
            }
        } else {
            isPermissionGranted = true;
        }

        return isPermissionGranted;
    }

    private static void showPermissionRationaleDialog(final Activity activity) {
        AlertDialog.Builder permissionRationaleDlgBuilder = new AlertDialog.Builder(activity);
        permissionRationaleDlgBuilder.setMessage(R.string.location_permission_message);
        permissionRationaleDlgBuilder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_COARSE_LOCATION);
            }
        });

        permissionRationaleDlgBuilder.create().show();
    }

    private static void showResetPermissionDialog(Activity activity) {
        AlertDialog.Builder resetPermissionDlgBuilder = new AlertDialog.Builder(activity);
        resetPermissionDlgBuilder.setMessage(R.string.location_permission_denied_message);
        resetPermissionDlgBuilder.setPositiveButton(android.R.string.ok, null);
        resetPermissionDlgBuilder.create().show();
    }

    private static void setPermissionRationaleShown(Context context, boolean shown) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putBoolean(PREF_PERMISSION_RATIONALE_SHOWN, shown);
        prefEditor.commit();
    }
    
    private static boolean wasPermissionRationaleShown(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(PREF_PERMISSION_RATIONALE_SHOWN, false);
    }

}

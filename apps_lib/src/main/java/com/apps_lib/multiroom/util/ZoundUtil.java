package com.apps_lib.multiroom.util;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Created by lsuhov on 07/02/2017.
 */

public class ZoundUtil {

    public static boolean isDestroyed(Context context) {

        if (context instanceof FragmentActivity || context instanceof Activity) {
            Activity activity = (Activity) context;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && activity.isDestroyed()) {
                return true;
            }
        }
        return false;
    }
}

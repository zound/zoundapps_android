package com.apps_lib.multiroom.settings.solo.equalizer;

import android.app.Activity;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysAudioEqCustomParam0;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysAudioEqCustomParam1;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsEqBands;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.TaskHelper;
import com.apps_lib.multiroom.settings.solo.speaker.IRetrieveTaskListener;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by cvladu on 24/11/16.
 */

public class EqualizerManager {
    private static EqualizerManager mInstance;
    private Radio mRadio;
    private Activity mActivity;

    public long min, max, middle;
    public int minStep, bassValueForSeekbar, trebleValueForSeekbar;

    private Timer mTimer;

    private EqualizerManager() {

    }

    public static EqualizerManager getInstance() {
        if (mInstance == null) {
            mInstance = new EqualizerManager();
        }
        return mInstance;
    }

    public interface IEqualiserListener {
        void onBassUpdated(long bass);
        void onTrebleUpdated(long treble);
    }

    public void init(Radio radio, Activity activity) {
        mRadio = radio;
        mActivity = activity;
    }

    public void startEqualizerTask(final IRetrieveTaskListener listener) {
        EqualizerNodesRetrievalTask task = new EqualizerNodesRetrievalTask(mRadio, mActivity, new IEqualizerNodesListener() {
            @Override
            public void onEqualizerNodesRetrieved(Map<Class, NodeInfo> nodesMap, List<NodeSysCapsEqBands.ListItem> eqBands) {
                if (listener != null) {
                    updateSteps(eqBands);
                    updateProgress(nodesMap);
                    listener.onFinish();
                }
            }
        });

        TaskHelper.execute(task);
    }

    public void startEqValueChecking(final IEqualiserListener listener){
        mTimer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                EqualizerNodesRetrievalTask task = new EqualizerNodesRetrievalTask(mRadio, mActivity, new IEqualizerNodesListener() {
                    @Override
                    public void onEqualizerNodesRetrieved(Map<Class, NodeInfo> nodesMap, List<NodeSysCapsEqBands.ListItem> eqBands) {
                        if (listener != null) {
                            updateSteps(eqBands);
                            updateProgress(nodesMap);
                            listener.onBassUpdated(bassValueForSeekbar);
                            listener.onTrebleUpdated(trebleValueForSeekbar);
                        }
                    }
                });
                TaskHelper.execute(task);
            }
        };

        mTimer.schedule(timerTask, 0, 600);
    }

    public void stopEqValueChecking() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    private void updateSteps(List<NodeSysCapsEqBands.ListItem> eqBands) {
        if (eqBands == null || eqBands.size() == 0) {
            return;
        }

        NodeSysCapsEqBands.ListItem listItem = eqBands.get(0);

        min = listItem.getMin();
        max = listItem.getMax();
        middle = Math.round((min + max) / 2);

        minStep = (int) min;
    }

    private void updateProgress(Map<Class, NodeInfo> nodesMap) {
        NodeSysAudioEqCustomParam0 bassNode = (NodeSysAudioEqCustomParam0) nodesMap.get(NodeSysAudioEqCustomParam0.class);
        NodeSysAudioEqCustomParam1 trebleNode = (NodeSysAudioEqCustomParam1) nodesMap.get(NodeSysAudioEqCustomParam1.class);

        if (bassNode != null) {
            bassValueForSeekbar = transformNodeValueToSeekBarValue(bassNode.getValue());
        }

        if (trebleNode != null) {
            trebleValueForSeekbar = transformNodeValueToSeekBarValue(trebleNode.getValue());
        }
    }

    private int transformNodeValueToSeekBarValue(Long value) {
        return value.intValue() - minStep;
    }

}

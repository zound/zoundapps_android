package com.apps_lib.multiroom.settings.solo.speakerList;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;

/**
 * Created by lsuhov on 07/06/16.
 */
public class RadioSelectedEvent {
    public Radio radio;

    public RadioSelectedEvent(Radio radio) {
        this.radio = radio;
    }
}

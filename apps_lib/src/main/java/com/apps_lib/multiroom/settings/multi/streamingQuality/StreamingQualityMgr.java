package com.apps_lib.multiroom.settings.multi.streamingQuality;

import android.os.AsyncTask;
import android.util.Log;

import com.apps_lib.multiroom.util.ZoundUtil;
import com.frontier_silicon.NetRemoteLib.Discovery.DeviceRecord;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomDeviceTransportOptimisation;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.common.TaskHelper;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.frontier_silicon.components.multiroom.MultiroomItemModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by nbalazs on 07/12/2016.
 */
public class StreamingQualityMgr {
    private static StreamingQualityMgr INSTANCE = null;

    public interface IStreamingQualityChangeListener {
        void onStreamingQualityChanged(boolean success);
    }

    private StreamingQualityMgr() {
    }

    public static StreamingQualityMgr getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new StreamingQualityMgr();
        }
        return INSTANCE;
    }

    public boolean getGeneralOptimizationStatus(List<Radio> radios) {
        int optimisationOn = 0;
        int optimizationOff = 0;
        for (Radio radio : radios) {
            if (isTransportOptimizationEnabled(radio)) {
                optimisationOn++;
            } else {
                optimizationOff++;
            }
        }
        return optimizationOff <= optimisationOn;
    }

    public void changeStreamingQuality(Radio radio, IStreamingQualityChangeListener listener, boolean optimizationEnabled) {
        if (radio != null && listener != null) {
            List<Radio> radios = new ArrayList<>();
            radios.add(radio);
            TaskHelper.execute(new TransportOptimisationSetterTask(listener, radios, optimizationEnabled));
        }
    }

    public void changeStreamingQuality(List<Radio> radios, IStreamingQualityChangeListener listener, boolean optimizationEnabled) {
        if (radios != null && listener != null) {
            TaskHelper.execute(new TransportOptimisationSetterTask(listener, radios, optimizationEnabled));
        }
    }

    public boolean isTransportOptimizationEnabled(Radio radio) {
        DeviceRecord deviceRecord = radio.getDeviceRecord();
        return deviceRecord != null && deviceRecord.transportOptimized != null && deviceRecord.transportOptimized;
    }

    private class TransportOptimisationSetterTask extends AsyncTask<Void, Void, Void> {
        private IStreamingQualityChangeListener mStreamingQualityChangeListener;
        private List<Radio> mRadios;
        private CountDownLatch countDownLatch;
        private volatile boolean mSuccess = true;
        private boolean mOptimizationEnabled;

        TransportOptimisationSetterTask(IStreamingQualityChangeListener streamingQualityChangeListener, List<Radio> radios, boolean optimizationEnabled) {
            mStreamingQualityChangeListener = streamingQualityChangeListener;
            mRadios = radios;
            mOptimizationEnabled = optimizationEnabled;
            countDownLatch = new CountDownLatch(radios.size());
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (final Radio radio : mRadios) {
                if (radio != null && radio.getDeviceRecord() != null && radio.getDeviceRecord().transportOptimized != null
                        && radio.getDeviceRecord().transportOptimized != mOptimizationEnabled) {

                    NodeMultiroomDeviceTransportOptimisation.Ord value = mOptimizationEnabled ? NodeMultiroomDeviceTransportOptimisation.Ord.ENABLED :
                            NodeMultiroomDeviceTransportOptimisation.Ord.DISABLED;

                    RadioNodeUtil.setNodeToRadioAsync(radio, new NodeMultiroomDeviceTransportOptimisation(value), new RadioNodeUtil.INodeSetResultListener() {

                        @Override
                        public void onNodeSetResult(boolean success) {
                            if (success) {
                                radio.getDeviceRecord().transportOptimized = mOptimizationEnabled;
                            }
                            mSuccess &= success;
                            countDownLatch.countDown();
                        }
                    });
                } else {
                    countDownLatch.countDown();
                }
            }

            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mStreamingQualityChangeListener != null) {
                mStreamingQualityChangeListener.onStreamingQualityChanged(mSuccess);
            }
        }
    }
}

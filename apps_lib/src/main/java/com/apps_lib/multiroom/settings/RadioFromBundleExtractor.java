package com.apps_lib.multiroom.settings;

import android.os.Bundle;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.loggerlib.LogLevel;
import com.apps_lib.multiroom.NavigationHelper;

/**
 * Created by lsuhov on 08/06/16.
 */
public class RadioFromBundleExtractor {

    public static Radio extractRadio(Bundle bundle, Class classType) {
        if (bundle == null) {
            FsLogger.log("bundle from " + classType.toString() + "should not be null", LogLevel.Error);
            return null;
        }

        String radioUDN = bundle.getString(NavigationHelper.RADIO_UDN_ARG);
        Radio radio = NetRemoteManager.getInstance().getRadio(radioUDN);

        if (radio == null) {
            FsLogger.log("radio was not found for udn= " + radioUDN + " in " + classType.toString(),
                    LogLevel.Error);
        }

        return radio;
    }
}

package com.apps_lib.multiroom.settings.solo.speaker;

/**
 * Created by cvladu on 25/11/16.
 */

public interface IRetrieveTaskListener {
    void onFinish();
}

package com.apps_lib.multiroom.widgets.other;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

import java.util.Random;

/**
 * Created by nbalazs on 11/10/2017.
 */
public final class BarAnimator {
    private View mView;
    private int mDuration;
    private Random mRandom;
    private boolean mEnableRandom;

    public BarAnimator(View view, int duration, boolean enableRandom) {
        mView = view;
        mDuration = duration;
        mRandom = new Random(999);
        mEnableRandom = enableRandom;
    }

    public void animate() {
        mView.startAnimation(getAnimation());
    }

    private Animation getAnimation() {
        ScaleAnimation animation = new ScaleAnimation(1.0f, 1.0f, mView.getScaleY(), 0.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 1.0f);
        animation.setInterpolator(new CustomBounceInterpolator());
        animation.setDuration(mDuration + (mEnableRandom ? mRandom.nextInt(50) : 0));
        animation.setFillAfter(true);
        animation.setRepeatMode(Animation.REVERSE);
        animation.setRepeatCount(Animation.INFINITE);

        return animation;
    }
}

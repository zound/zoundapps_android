package com.apps_lib.multiroom.widgets.other;

import android.view.animation.Interpolator;

/**
 * Created by nbalazs on 11/10/2017.
 *
 * Linear functions between the next values:
 * f(0.00)     =   0.00        f(X) = 3X                        X   <   0.25f
 * f(0.25)     =   0.75        f(X) = 1 - X        0.25    <=   X   <   0.5
 * f(0.50)     =   0.50        f(X) = 2X - 0.5     0.5     <=   X   <   0.75
 * f(0.75)     =   1.00        f(X) = 3.25 - 3X    0.75    <=   X   <   1
 * f(1.00)     =   0.25        F(X) = 0.25               X == 1
 */

public class CustomBounceInterpolator implements Interpolator {

    @Override
    public float getInterpolation(float input) {
        float retValue;

        if (input < 0.25f) {
            retValue = 3f * input;
        } else if (input < 0.5f) {
            retValue = 1 - input;
        } else if (input < 0.75f) {
            retValue = (2f * input) - 0.5f;
        } else if (input < 1){
            retValue = 3.25f - (3f * input);
        } else {
            retValue = 0.25f;
        }

        return retValue;
    }
}

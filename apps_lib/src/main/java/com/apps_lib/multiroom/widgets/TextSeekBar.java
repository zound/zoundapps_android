package com.apps_lib.multiroom.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;

import com.apps_lib.multiroom.R;


/**
 * Created by nbalazs on 03/04/2017.
 */

public class TextSeekBar extends NoJumpSeekBar{

    private final Paint mTextPaint;
    private String mText = null;

    public TextSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mTextPaint = createTextPaint();
        init(context, attrs);
    }

    public TextSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        mTextPaint = createTextPaint();
        init(context, attrs);
    }

    public TextSeekBar(Context context) {
        super(context);
        mTextPaint = createTextPaint();
    }

    private void init(Context context, AttributeSet attributeSet) {
        TypedArray array = context.obtainStyledAttributes(attributeSet, R.styleable.TextSeekBar, 0, 0);
        mText = array.getString(R.styleable.TextSeekBar_textContent);
        getDeltaProgressOnTap(context, attributeSet);

        array.recycle();
    }

    private void getDeltaProgressOnTap(Context context, AttributeSet attributeSet) {
        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.CircleViewPagerIndicator);
        DELTA_PROGRESS_ON_TAP = typedArray.getInt(R.styleable.CircleViewPagerIndicator_delta_progress_on_tap, 4);
    }

    private Paint createTextPaint() {
        Paint textPaint = new Paint();
        //noinspection deprecation
        textPaint.setColor(getContext().getResources().getColor(R.color.switch_text_color));
        textPaint.setTextSize(getFixedSize());
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextAlign(Paint.Align.CENTER);
        // Set textSize, typeface, etc, as you wish
        return textPaint;
    }

    private float getFixedSize() {
        float textSizeInDP = 12.0f;
        float density = getContext().getResources().getDisplayMetrics().density;

        if ((Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) && (density < 1.75f)) {
            textSizeInDP = 9.0f;
        }
        return textSizeInDP * density + 0.5f;
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mText != null) {

            final Rect textBounds = new Rect();
            mTextPaint.getTextBounds(mText, 0, mText.length(), textBounds);

            // The baseline for the text: centered, including the height of the text itself
            final int heightBaseline = canvas.getClipBounds().height() / 2 + textBounds.height() / 2;

            // Center the text in the SeekBar thumb horizontally
            final Rect rect = getSeekbarThumb().getBounds();
            final int widthBaseline = (rect.left + rect.right) / 2 - textBounds.width() / 7;

            canvas.drawText(mText, 0, mText.length(), widthBaseline, heightBaseline, mTextPaint);
        }
    }
}

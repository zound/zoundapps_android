package com.apps_lib.multiroom.browse_ir.model;

/**
 * Created by lsuhov on 02/07/16.
 */

public enum EIRNavigationResult {
    Success,
    NavigationFailed,
    TotalItemsNotRetrieved
}

package com.apps_lib.multiroom.browse_ir.model;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavList;
import com.frontier_silicon.components.common.FsComponentsConfig;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by lsuhov on 30/06/16.
 */

public class NavigationPagesStore {

    public static final int DEFAULT_START_KEY_ID = -1;

    private Map<Integer, NavigationPageModel> mPagesMap = new HashMap<>();

    private final int corePoolSize = 0;
    private final int maxPoolSize = 1;
    // Sets the amount of time an idle thread waits before terminating
    private final int KEEP_ALIVE_TIME = 1;
    // Sets the Time Unit to seconds
    private final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
    private BlockingQueue<Runnable> mNavigationRunnablesQueue = new LinkedBlockingQueue<>(1000);
    private ExecutorService mThreadPoolExecutor;
    private Boolean mSearchItemWasFound = null;

    NavigationPagesStore() {
        mThreadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maxPoolSize, KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT, mNavigationRunnablesQueue, new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
                FsLogger.log("NavigationPagesStore: thread execution was rejected", LogLevel.Error);
            }
        });
    }

    public void getBrowseItemModelAsync(int browsePosition, IBrowseItemModelRetrieveListener listener) {

        int positionOfNavigationPage = getPositionOfNavigationPage(browsePosition);
        int positionOfBrowseItemInNavigationPage = getPositionOfBrowseItemInNavigationPage(browsePosition);

        NavigationPageModel pageModel;

        synchronized (this) {
            if (!mPagesMap.containsKey(positionOfNavigationPage)) {
                pageModel = new NavigationPageModel(browsePosition - 1, mThreadPoolExecutor);
                mPagesMap.put(positionOfNavigationPage, pageModel);
            }

            pageModel = mPagesMap.get(positionOfNavigationPage);
        }

        pageModel.getBrowseItemModelAsync(positionOfBrowseItemInNavigationPage, listener);
    }

    public void addNavListToStore(int pagePosition, List<NodeNavList.ListItem> listItems) {
        NavigationPageModel pageModel = new NavigationPageModel(pagePosition - 1, listItems, mThreadPoolExecutor);

        synchronized (this) {
            mPagesMap.put(pagePosition, pageModel);
        }
    }

    public boolean searchItemWasFound() {
        if (mSearchItemWasFound != null) {
            return mSearchItemWasFound;
        }

        NavigationPageModel navigationPageModel;
        synchronized (this) {
            navigationPageModel = mPagesMap.get(0);
        }

        if (navigationPageModel != null) {
            mSearchItemWasFound = navigationPageModel.searchItemWasFound;
        }

        return mSearchItemWasFound != null ? mSearchItemWasFound : false;
    }

    public synchronized void clear() {
        Set<Integer> keys = mPagesMap.keySet();
        for (Integer key : keys) {
            NavigationPageModel navigationPageModel = mPagesMap.get(key);
            navigationPageModel.dispose();
        }

        mPagesMap.clear();
        mNavigationRunnablesQueue.clear();
        mSearchItemWasFound = null;
    }

    private int getPositionOfNavigationPage(int browseItemPosition) {
        int adjustedBrowseItemPosition = getBrowseItemPositionIncludingSearchPresence(browseItemPosition);

        int pagePosition = (int) Math.floor(adjustedBrowseItemPosition / FsComponentsConfig.MAX_NAV_LIST_ITEMS);

        FsLogger.log("Page position " + pagePosition + " for browse item " + browseItemPosition, LogLevel.Info);
        return pagePosition;
    }

    private int getPositionOfBrowseItemInNavigationPage(int browseItemPosition) {
        int adjustedBrowseItemPosition = getBrowseItemPositionIncludingSearchPresence(browseItemPosition);

        int positionOfBrowseItemInNavigationPage = adjustedBrowseItemPosition % FsComponentsConfig.MAX_NAV_LIST_ITEMS;

        FsLogger.log("Adjusted item position " + positionOfBrowseItemInNavigationPage + " for browse item " +
                browseItemPosition, LogLevel.Info);
        return positionOfBrowseItemInNavigationPage;
    }

    private int getBrowseItemPositionIncludingSearchPresence(int browseItemPosition) {
        if (searchItemWasFound() && browseItemPosition > (FsComponentsConfig.MAX_NAV_LIST_ITEMS - 2)) {
            return browseItemPosition + 1;
        } else {
            return browseItemPosition;
        }
    }

    public synchronized boolean isEmpty() {
        return mPagesMap.isEmpty();
    }
}

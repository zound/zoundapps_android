package com.apps_lib.multiroom.browse_ir.model;

/**
 * Created by lsuhov on 30/06/16.
 */

public interface IBrowseItemModelRetrieveListener {
    void onBrowseItemModelRetrieved(BrowseIRItemModel itemModel);
}

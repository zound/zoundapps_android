package com.apps_lib.multiroom.browse_ir.model;

import com.frontier_silicon.NetRemoteLib.Node.BaseNavStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavStatus;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNavigationUtil;

import java.util.List;

/**
 * Created by lsuhov on 30/06/16.
 */

class ResetNavigationToCurrentListThread extends Thread {

    private BreadcrumbsKeeper mBreadcrumbsKeeper;
    private Radio mRadio;
    private IIRNavigationListener mListener;
    private boolean mShouldDispose = false;

    ResetNavigationToCurrentListThread(BreadcrumbsKeeper breadcrumbsKeeper, Radio radio, IIRNavigationListener listener) {
        mBreadcrumbsKeeper = breadcrumbsKeeper;
        mRadio = radio;
        mListener = listener;
    }

    @Override
    public void run() {

        if (mShouldDispose) {
            dispose();
            return;
        }

        NodeNavStatus statusNode = RadioNavigationUtil.resetNavigationToRoot(mRadio);

        if (mShouldDispose) {
            dispose();
            return;
        }

        NodeNavStatus.Ord status = statusNode != null ? statusNode.getValueEnum() : null;

        if (mShouldDispose) {
            dispose();
            return;
        }

        if (statusNode == null || status == BaseNavStatus.Ord.FAIL || status == BaseNavStatus.Ord.FATAL_ERR ||
                status == BaseNavStatus.Ord.WAITING) {
            notifyError();
            dispose();
            return;
        }

        if (mShouldDispose) {
            dispose();
            return;
        }

        if (mBreadcrumbsKeeper.hasSearch()) {
            navigateAndSearch();
        } else {
            if (!navigateWithBreadCrumbs()) {
                dispose();
                return;
            }
        }

        if (mShouldDispose) {
            dispose();
            return;
        }

        BrowseIRManager.getInstance().retrieveFirstPageSync(mRadio);

        if (mShouldDispose) {
            dispose();
            return;
        }

        boolean result = BrowseIRManager.getInstance().retrieveTotalNumberOfItemsFromCurrentListSync(mRadio);

        if (mShouldDispose) {
            dispose();
            return;
        }

        if (mListener != null) {
            mListener.onNavigationCompleted(result ? EIRNavigationResult.Success : EIRNavigationResult.TotalItemsNotRetrieved);
        }

        dispose();
    }

    private boolean navigateWithBreadCrumbs() {
        List<Long> breadCrumbs = mBreadcrumbsKeeper.getBreadCrumbs();
        for (Long index : breadCrumbs) {
            boolean success = RadioNavigationUtil.getInstance().navigateNavItem(mRadio, index);

            if (!success) {
                notifyError();
                return false;
            }
        }

        return true;
    }

    private void navigateAndSearch() {
        Long[] searchPath = mBreadcrumbsKeeper.getSearchNavigationPath();

        for (int i = 0; i < searchPath.length - 1; i++) {

            if (mShouldDispose) {
                dispose();
                return;
            }

            boolean success = RadioNavigationUtil.getInstance().navigateNavItem(mRadio, searchPath[i]);

            if (!success) {
                notifyError();
                return;
            }
        }

        if (mShouldDispose) {
            dispose();
            return;
        }

        RadioNavigationUtil.searchNavItem(mRadio, mBreadcrumbsKeeper.getSearchText(), searchPath[searchPath.length - 1]);
    }

    private void notifyError() {
        if (mListener != null) {
            mListener.onNavigationCompleted(EIRNavigationResult.NavigationFailed);
        }
    }

    private void dispose() {
        mBreadcrumbsKeeper = null;
        mListener = null;
        mRadio = null;
    }

    public void cancel() {
        mShouldDispose = true;
    }
}

package com.apps_lib.multiroom.browse_ir.model;

import android.text.TextUtils;

import com.apps_lib.multiroom.util.LocationDecoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by lsuhov on 30/06/16.
 */

class BreadcrumbsKeeper {
    private Stack<Long> mBreadCrumbs = new Stack<>();
    private Long[] mSearchNavigationPath = new Long[] {2L, 2L};
    private String mSearchText = null;

    BreadcrumbsKeeper() {}

    synchronized List<Long> getBreadCrumbs() {
        return new ArrayList<>(mBreadCrumbs);
    }

    synchronized void addToBreadcrumbs(long key) {
        mBreadCrumbs.push(key);
    }

    synchronized void pop() {
        if (mBreadCrumbs.size() > 0) {
            mBreadCrumbs.pop();
        }
    }

    synchronized int size() {
        if (hasSearch()) {
            return 1;
        }
        return mBreadCrumbs.size();
    }

    public synchronized void clear() {
        mBreadCrumbs.clear();
        mSearchText = null;
    }

    Long[] getSearchNavigationPath() {
        return mSearchNavigationPath;
    }

    void setSearchText(String searchText) {
        mSearchText = searchText;
    }

    String getSearchText() {
        return mSearchText;
    }

    void clearSearchText() {
        mSearchText = null;
    }

    boolean hasSearch() {
        return !TextUtils.isEmpty(mSearchText);
    }
}

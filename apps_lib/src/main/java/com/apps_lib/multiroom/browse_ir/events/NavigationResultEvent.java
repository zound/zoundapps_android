package com.apps_lib.multiroom.browse_ir.events;


import com.apps_lib.multiroom.browse_ir.model.EIRNavigationResult;

/**
 * Created by lsuhov on 02/07/16.
 */

public class NavigationResultEvent {
    public EIRNavigationResult result;

    public NavigationResultEvent(EIRNavigationResult result) {
        this.result = result;
    }
}

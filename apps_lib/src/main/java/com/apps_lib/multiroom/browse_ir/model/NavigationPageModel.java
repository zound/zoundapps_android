package com.apps_lib.multiroom.browse_ir.model;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.NodeListItem;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavList;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;

/**
 * Created by lsuhov on 30/06/16.
 */

public class NavigationPageModel {

    public int startIndex;
    public List<BrowseIRItemModel> browseIRItemModels;
    public boolean searchItemWasFound = false;

    private ExecutorService mExecutorService;
    private boolean itemsWereRetrieved = false;
    private int numberOfRetries = 0;
    private final int maxNumberOfRetries = 2;
    private final Object mLock = new Object();
    private Queue<BrowseItemWaitingInfo> queueOfListeners = new ConcurrentLinkedQueue<>();
    private boolean isDisposed = false;

    public NavigationPageModel(int key, ExecutorService executorService) {
        startIndex = key;
        mExecutorService = executorService;
        browseIRItemModels = new ArrayList<>();

        startRetrievingNavigationPage();
    }

    public NavigationPageModel(int key, List<NodeNavList.ListItem> listItems, ExecutorService executorService) {
        startIndex = key;
        mExecutorService = executorService;
        browseIRItemModels = new ArrayList<>();

        parseNavList(listItems);
    }

    public void getBrowseItemModelAsync(int position, IBrowseItemModelRetrieveListener listener) {
        if (itemsWereRetrieved) {
            if (position < browseIRItemModels.size()) {
                listener.onBrowseItemModelRetrieved(browseIRItemModels.get(position));
            } else {
                if (numberOfRetries < maxNumberOfRetries) {
                    FsLogger.log("Requested BrowseItem is not existent. page " + startIndex +
                            ", position. Restarting retrieval" + position, LogLevel.Error);

                    addListenerToQueue(position, listener);

                    numberOfRetries++;
                    startRetrievingNavigationPage();
                } else {
                    listener.onBrowseItemModelRetrieved(null);
                }
            }
        } else {
            addListenerToQueue(position, listener);
        }
    }

    public void dispose() {
        synchronized (mLock) {
            isDisposed = true;
            queueOfListeners.clear();
            browseIRItemModels.clear();
            mExecutorService = null;
        }
    }

    private void addListenerToQueue(int position, IBrowseItemModelRetrieveListener listener) {
        BrowseItemWaitingInfo browseItemWaitingInfo = new BrowseItemWaitingInfo(position, listener);
        synchronized (mLock) {
            queueOfListeners.add(browseItemWaitingInfo);
        }
    }

    private void startRetrievingNavigationPage() {
        Radio radio = NetRemoteManager.getInstance().getCurrentRadio();
        if (radio == null) {
            return;
        }

        NavPageRetrieverRunnable runnable = new NavPageRetrieverRunnable(startIndex, radio, new NavPageRetrieverRunnable.INavPageRetrieverListener() {
            @Override
            public void onNavPageRetrieved(final List<NodeNavList.ListItem> listItems) {
                NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!isDisposed) {
                            parseNavList(listItems);
                        }
                    }
                }, true);
            }
        });

        FsLogger.log("adding navigation runnable " + startIndex, LogLevel.Info);
        mExecutorService.submit(runnable);
    }

    private void parseNavList(List<NodeNavList.ListItem> navList) {
        for (NodeNavList.ListItem listItem : navList) {
            if (listItem.getType() == NodeListItem.FieldType.SearchDirectory) {
                searchItemWasFound = true;
                continue;
            }

            BrowseIRItemModel browseIRItemModel = new BrowseIRItemModel(listItem);
            browseIRItemModels.add(browseIRItemModel);
        }

        itemsWereRetrieved = true;
        notifyAllListenersFromQueue();
    }

    private void notifyAllListenersFromQueue() {
        synchronized (mLock) {
            while (queueOfListeners.peek() != null) {
                BrowseItemWaitingInfo waitingInfo = queueOfListeners.poll();
                getBrowseItemModelAsync(waitingInfo.position, waitingInfo.listener);

                waitingInfo.listener = null;
            }
        }
    }

    private class BrowseItemWaitingInfo {
        int position;
        IBrowseItemModelRetrieveListener listener;

        BrowseItemWaitingInfo(int position, IBrowseItemModelRetrieveListener listener) {
            this.position = position;
            this.listener = listener;
        }
    }
}

package com.apps_lib.multiroom.browse_ir.model;

import android.text.TextUtils;

import com.apps_lib.multiroom.browse_ir.events.NavigationResultEvent;
import com.apps_lib.multiroom.browse_ir.events.PlayStationEvent;
import com.apps_lib.multiroom.browse_ir.events.ShowLoadingProgressEvent;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavActionSelectItem;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavList;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavNumItems;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNavigationUtil;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.connection.ConnectionState;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.connection.IConnectionStateListener;
import com.frontier_silicon.components.NetRemoteManager;
import com.apps_lib.multiroom.nowPlaying.ModeChangedEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

/**
 * Created by lsuhov on 28/06/16.
 */

public class BrowseIRManager implements IConnectionStateListener {

    private int mTotalCountForCurrentList = 0;
    private BreadcrumbsKeeper mBreadcrumbsKeeper;
    private NavigationPagesStore mNavigationPagesStore;
    private String mUdnOfLastConnectedDevice = "";
    private ResetNavigationToCurrentListThread mNavigateToCurrentListThread;


    private static BrowseIRManager mInstance;

    public static BrowseIRManager getInstance() {
        if (mInstance == null) {
            mInstance = new BrowseIRManager();
        }

        return mInstance;
    }

    public static void reset() {
        mInstance.mBreadcrumbsKeeper.clear();
        mInstance.mNavigationPagesStore.clear();
        if (mInstance.mNavigateToCurrentListThread != null) {
            mInstance.mNavigateToCurrentListThread.cancel();
            mInstance.mNavigateToCurrentListThread = null;
        }
    }

    private BrowseIRManager() {
        mBreadcrumbsKeeper = new BreadcrumbsKeeper();
        mNavigationPagesStore = new NavigationPagesStore();

        Radio radio = NetRemoteManager.getInstance().getCurrentRadio();
        if (radio != null) {
            mUdnOfLastConnectedDevice = radio.getUDN();
        }

        EventBus.getDefault().register(this);

        ConnectionStateUtil.getInstance().addListener(this, false);
    }

    public int getTotalCountForCurrentList() {
        if (mNavigationPagesStore.searchItemWasFound()) {
            return mTotalCountForCurrentList > 0 ? mTotalCountForCurrentList - 1 : 0;
        } else {
            return mTotalCountForCurrentList;
        }
    }

    public void navigateToCurrentList(IIRNavigationListener listener) {
        Radio radio = NetRemoteManager.getInstance().getCurrentRadio();

        if (!validateNavigation(radio, listener)) {
            return;
        }

        prepareNavigation();

        mNavigateToCurrentListThread = new ResetNavigationToCurrentListThread(mBreadcrumbsKeeper, radio, listener);

        mNavigateToCurrentListThread.start();
    }

    private boolean validateNavigation(Radio radio, IIRNavigationListener listener) {
        if (radio == null) {
            if (listener != null) {
                listener.onNavigationCompleted(EIRNavigationResult.NavigationFailed);
            }
            return false;
        } else {
            if (mNavigateToCurrentListThread != null && mNavigateToCurrentListThread.isAlive()) {
                sendLoadingEvent();
                return false;
            }

            if (TextUtils.isEmpty(mUdnOfLastConnectedDevice) || !TextUtils.equals(mUdnOfLastConnectedDevice, radio.getUDN())) {
                mUdnOfLastConnectedDevice = radio.getUDN();
                return true;
            }

            if (mBreadcrumbsKeeper.hasSearch()) {
                return true;
            }

            if (mTotalCountForCurrentList == 0) {
                return true;
            }

            if (!mNavigationPagesStore.isEmpty()) {
                EventBus.getDefault().post(new NavigationResultEvent(EIRNavigationResult.Success));
                return false;
            }
        }

        return true;
    }

    public void search(String searchText, final IIRNavigationListener listener) {
        mBreadcrumbsKeeper.setSearchText(searchText);

        navigateToCurrentList(listener);
    }

    public void getBrowseItemModelAsync(int position, IBrowseItemModelRetrieveListener listener) {
        mNavigationPagesStore.getBrowseItemModelAsync(position, listener);
    }

    public void navigateToBrowseItem(BrowseIRItemModel browseIRItemModel) {
        if (browseIRItemModel == null) {
            return;
        }

        if (browseIRItemModel.isDirectory()) {
            navigateToDirectory(browseIRItemModel.getKey());
        } else if (browseIRItemModel.isPlayable()) {
            playNavItemKey(browseIRItemModel.getKey());
        }
    }

    public void navigateUp(IIRNavigationListener listener) {
        if (mBreadcrumbsKeeper.hasSearch()) {
            mBreadcrumbsKeeper.clearSearchText();
            mNavigationPagesStore.clear();
            clearTotalCountForCurrentList();
            navigateToCurrentList(listener);

        } else {
            navigateToDirectory(RadioNavigationUtil.INVALID_INT32_KEY);
        }
    }

    public int getNavDepth() {
        return mBreadcrumbsKeeper.size();
    }

    private void prepareNavigation() {
        sendLoadingEvent();

        mNavigationPagesStore.clear();
    }

    private void sendLoadingEvent() {
        EventBus.getDefault().post(new ShowLoadingProgressEvent());
    }

    private void navigateToDirectory(final Long key) {
        Radio radio = NetRemoteManager.getInstance().getCurrentRadio();
        if (radio == null) {
            return;
        }

        prepareNavigation();

        NavigateRunnable navigateRunnable = new NavigateRunnable(key,
                radio, new IIRNavigationListener() {
            @Override
            public void onNavigationCompleted(EIRNavigationResult result) {
                switch (result) {
                    case Success:
                    case TotalItemsNotRetrieved:
                        if (key.equals(RadioNavigationUtil.INVALID_INT32_KEY)) {
                            mBreadcrumbsKeeper.pop();
                        } else {
                            mBreadcrumbsKeeper.addToBreadcrumbs(key);
                        }
                        break;
                }

                EventBus.getDefault().post(new NavigationResultEvent(result));
            }
        });

        Thread thread = new Thread(navigateRunnable);
        thread.start();
    }

    private void playNavItemKey(Long key) {
        Radio radio = NetRemoteManager.getInstance().getCurrentRadio();
        if (radio == null) {
            return;
        }

        RadioNodeUtil.setNodeToRadioAsync(radio, new NodeNavActionSelectItem(key), new RadioNodeUtil.INodeSetResultListener() {
            @Override
            public void onNodeSetResult(boolean success) {
                if (success) {
                    EventBus.getDefault().post(new PlayStationEvent());
                }
            }
        });
    }

    void retrieveFirstPageSync(Radio radio) {
        NavPageRetrieverRunnable retrieverRunnable = new NavPageRetrieverRunnable(
                NavigationPagesStore.DEFAULT_START_KEY_ID, radio, new NavPageRetrieverRunnable.INavPageRetrieverListener() {
            @Override
            public void onNavPageRetrieved(List<NodeNavList.ListItem> listItems) {

                addNavListToStore(0, listItems);
            }
        });

        retrieverRunnable.run();
    }

    boolean retrieveTotalNumberOfItemsFromCurrentListSync(Radio radio) {
        NodeNavNumItems nodeNumItems = (NodeNavNumItems)radio.getNodeSyncGetter(NodeNavNumItems.class).get();

        if (nodeNumItems == null) {
            clearTotalCountForCurrentList();
            return false;
        }

        int totalNumber = nodeNumItems.getValue().intValue();
        if (totalNumber == -1) {
            clearTotalCountForCurrentList();
            return false;
        }

        mTotalCountForCurrentList = totalNumber;

        return true;
    }

    public void onSpeakerConnected(Radio radio) {
        String udn = radio.getUDN();

        if (!udn.equals(mUdnOfLastConnectedDevice)) {
            mBreadcrumbsKeeper.clear();
            mUdnOfLastConnectedDevice = udn;
        }
    }

    private void addNavListToStore(int pagePosition, List<NodeNavList.ListItem> listItems) {
        mNavigationPagesStore.addNavListToStore(pagePosition, listItems);
    }

    @Subscribe
    public void onModeChanged(ModeChangedEvent event) {
        mNavigationPagesStore.clear();
        mBreadcrumbsKeeper.clear();
    }

    @Override
    public void onStateUpdate(ConnectionState newState) {
        switch (newState) {
            case DISCONNECTED:
            case INVALID_SESSION:
            case NO_WIFI_OR_ETHERNET:
            case CONNECTED_TO_RADIO:
                mNavigationPagesStore.clear();
                break;
        }
    }

    private void clearTotalCountForCurrentList() {
        mTotalCountForCurrentList = 0;
    }
}

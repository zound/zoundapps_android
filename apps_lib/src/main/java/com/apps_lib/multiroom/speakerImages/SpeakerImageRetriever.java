package com.apps_lib.multiroom.speakerImages;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;

/**
 * Created by lsuhov on 04/08/16.
 */

public abstract class SpeakerImageRetriever {

    public int getImageIdForSSID(String ssid, ESpeakerImageSizeType sizeType) {
        ESpeakerModelType modelType = SpeakerManager.getInstance().getSpeakerModelFromSSID(ssid);

        return getImageIdForESpeakerModelType(modelType, sizeType);
    }

    public void retrieveImageIdForRadio(final Radio radio, final ESpeakerImageSizeType sizeType, final ISpeakerImageIdListener listener) {
        SpeakerModelMapper.getModelNameForRadio(radio, new IModelNameListener() {
            @Override
            public void onModelNameRetrieved(String modelName) {
                final int imageId = retrieveImageIdForModelName(modelName, radio.getFirmwareVersion(), sizeType);

                listener.onImageIdRetrieved(imageId);
            }
        });
    }


    private int retrieveImageIdForModelName(String modelName, String softwareVersion, ESpeakerImageSizeType sizeType) {
        ESpeakerModelType modelType = SpeakerManager.getInstance().getSpeakerModelFromModelName(modelName, softwareVersion);
        return getImageIdForESpeakerModelType(modelType, sizeType);
    }

    protected abstract int getImageIdForESpeakerModelType(ESpeakerModelType speakerModelType, ESpeakerImageSizeType sizeType);
}

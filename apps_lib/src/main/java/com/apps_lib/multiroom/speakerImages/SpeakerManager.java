package com.apps_lib.multiroom.speakerImages;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;

/**
 * Created by nbalazs on 09/05/2017.
 */

public class SpeakerManager {

    private SpeakerTypeDecoder mSpeakerTypeDecoder;

    private SpeakerImageRetriever mSpakerImageRetriver;

    private SpeakerManager() {}

    private static SpeakerManager instance;

    public static SpeakerManager getInstance() {
        return instance;
    }

    public static void init(SpeakerTypeDecoder speakerTypeDecoder, SpeakerImageRetriever speakerImageRetriever) {
        instance = new SpeakerManager();
        instance.mSpeakerTypeDecoder = speakerTypeDecoder;
        instance.mSpakerImageRetriver = speakerImageRetriever;
    }

    public final ESpeakerModelType getSpeakerModelFromSSID(String ssid) {
        return mSpeakerTypeDecoder.getSpeakerModelFromSSID(ssid);
    }

    public final ESpeakerModelType getSpeakerModelFromModelName(String modelName) {
        return mSpeakerTypeDecoder.getSpeakerModelFromModelName(modelName, "");
    }

    public final ESpeakerModelType getSpeakerModelFromModelName(String modelName, String softwareVersion) {
        return mSpeakerTypeDecoder.getSpeakerModelFromModelName(modelName, softwareVersion);
    }

    public final int getImageIdForSSID(String ssid, ESpeakerImageSizeType sizeType) {
        return mSpakerImageRetriver.getImageIdForSSID(ssid, sizeType);
    }

    public void retrieveImageIdForRadio(Radio radio, final ESpeakerImageSizeType sizeType, final ISpeakerImageIdListener listener) {
        mSpakerImageRetriver.retrieveImageIdForRadio(radio, sizeType, listener);
    }

    public void retrieveImageIdAndSoftwareVersionForRadio(Radio radio, final ESpeakerImageSizeType sizeType, ISpeakerImageIdListener listener) {
        mSpakerImageRetriver.retrieveImageIdForRadio(radio, sizeType, listener);
    }
}

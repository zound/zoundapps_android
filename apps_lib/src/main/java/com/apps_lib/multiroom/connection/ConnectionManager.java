package com.apps_lib.multiroom.connection;

import android.os.AsyncTask;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.CommonPreferences;
import com.frontier_silicon.components.common.FsComponentsConfig;
import com.frontier_silicon.components.common.TaskHelper;
import com.frontier_silicon.components.multiroom.IMultiroomGroupingListener;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by lsuhov on 13/12/2016.
 */

public class ConnectionManager implements IMultiroomGroupingListener {

    private static ConnectionManager mInstance;
    private final int CONNECT_TO_RADIO_TIMEOUT = 20_000;

    private long mConnectToRadioTaskStartTime = 0;
    private ConnectToServerAndSelectedSpeakerTask mConnectToAnotherRadioTask;
    private boolean mShouldAutoConnectToLastSpeaker = false;

    private Radio mServerRadio;
    private Radio mSelectedRadio;
    private String mLastSelectedDeviceSN;

    private ConnectionManager() {}

    public static ConnectionManager getInstance() {
        if (mInstance == null) {
            mInstance = new ConnectionManager();
        }

        return mInstance;
    }

    public boolean connect(MultiroomDeviceModel deviceModel, boolean failSilently,
                           final IConnectToSpeakerListener listener) {
        if (!connectToAnotherRadioIsAllowed())
            return false;

        MultiroomGroupManager.getInstance().removeListener(this);
        MultiroomGroupManager.getInstance().addListener(this);

        mConnectToAnotherRadioTask = new ConnectToServerAndSelectedSpeakerTask(deviceModel,
                failSilently, new IConnectToServerAndSelectedSpeakerListener() {
            @Override
            public void onConnected(boolean success, Radio serverRadio, boolean secondSpeakerWasConnected,
                                    MultiroomDeviceModel selectedSpeakerDeviceModel) {

                if (listener != null) {
                    listener.onConnectedToServerOrRadio(success, selectedSpeakerDeviceModel.mRadio);
                }

                EventBus.getDefault().post(new NewConnectionWasMadeEvent());
            }
        });

        TaskHelper.execute(mConnectToAnotherRadioTask);
        mConnectToRadioTaskStartTime = System.currentTimeMillis();

        return true;
    }

    private boolean connectToAnotherRadioIsAllowed() {
        return mConnectToAnotherRadioTask == null ||
                mConnectToAnotherRadioTask.getStatus() == AsyncTask.Status.FINISHED ||
                (System.currentTimeMillis() - mConnectToRadioTaskStartTime > CONNECT_TO_RADIO_TIMEOUT);
    }

    void setSelectedRadio(Radio selectedSpeakerDeviceModel) {
        mSelectedRadio = selectedSpeakerDeviceModel;
    }

    public Radio getSelectedRadio() {
        return mSelectedRadio;
    }

    void setServerRadio(Radio serverRadio) {
        mServerRadio = serverRadio;
    }

    public void setShouldAutoConnectToLastSpeaker(boolean autoConnect) {
        mShouldAutoConnectToLastSpeaker = autoConnect;
    }

    public boolean getShouldAutoConnectToLastSpeaker() {
        return mShouldAutoConnectToLastSpeaker;
    }

    @Override
    public void onGroupingUpdate() {
        if (mServerRadio == null || !NetRemoteManager.getInstance().checkConnection(mServerRadio)) {
            return;
        }

        MultiroomDeviceModel deviceModel = MultiroomGroupManager.getInstance().getDeviceByUdn(mSelectedRadio.getUDN());
        Radio newServerRadio = MultiroomGroupManager.getInstance().getServerOrUngroupedRadio(deviceModel);
        if (newServerRadio == null || newServerRadio.equals(mServerRadio)) {
            return;
        }

        FsLogger.log("New server was detected for " + mSelectedRadio, LogLevel.Warning);

        connect(deviceModel, false, null);
    }

    void setLastSelectedDeviceSN(String serialNumber) {
        mLastSelectedDeviceSN = serialNumber;
    }

    public String getLastSelectedDeviceSN() {
        return mLastSelectedDeviceSN;
    }

    public Radio getLastSelectedDevice() {
        return NetRemoteManager.getInstance().getRadioFromSN(mLastSelectedDeviceSN);
    }

    private String getNameOfSelectedSpeaker() {
        String friendlyName = "";
        if (mSelectedRadio == null) {
            return friendlyName;
        }
        Radio radio = mSelectedRadio;

        friendlyName = radio.getFriendlyName();

        return friendlyName;
    }

    public String getNameOfSelectedSpeakerUpperCase() {
        return getNameOfSelectedSpeaker().toUpperCase();
    }

    public boolean isInMultiMode() {
        if (mSelectedRadio == null || mServerRadio == null) {
            return false;
        }

        MultiroomDeviceModel deviceModel = MultiroomGroupManager.getInstance().getDeviceByUdn(mSelectedRadio.getUDN());
        return !MultiroomGroupManager.getInstance().isUngroupedDevicesGroup(deviceModel != null ? deviceModel.mGroupId : "");
    }

    public void saveLastConnectedDevice() {
        String snOfLastConnectedSpeaker = getLastSelectedDeviceSN();

        CommonPreferences.getInstance().setLastConnectedDeviceSN(snOfLastConnectedSpeaker);
    }

    public void restoreLastConnectedDevice() {
        String snOfLastConnectedSpeaker = CommonPreferences.getInstance().getLastConnectedDeviceSN();

        setLastSelectedDeviceSN(snOfLastConnectedSpeaker);
    }
}

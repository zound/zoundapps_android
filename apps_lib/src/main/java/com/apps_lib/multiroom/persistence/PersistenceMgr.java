package com.apps_lib.multiroom.persistence;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by nbalazs on 10/01/2018.
 */

public class PersistenceMgr {
    private static PersistenceMgr mInstance;

    private final String UNLOCKED_SPEAKERS_CHECKING_DATE ="UNLOCKED_SPEAKERS_CHECKIND_DATE";
    private final String UPDATE_SPEAKER_AVAILABLE_DATE = "UPDATE_SPEAKER_AVAILABLE_DATE";
    private final String SHOWING_UPDATE_SPEAKER_AVAILABLE_DATE = "SHOWING_UPDATE_SPEAKER_AVAILABLE_DATE";
    private final String LIST_OF_SPEAKERS_WITH_UPDATE_AVAILABLE = "LIST_OF_SPEAKERS_WITH_UPDATE_AVAILABLE";

    private Context mContext;

    public static PersistenceMgr getInstance() {
        if (mInstance == null) {
            mInstance = new PersistenceMgr();
        }
        return mInstance;
    }

    private PersistenceMgr() {}

    public void init(Context context) {
        mContext = context;
    }


    private SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    private SharedPreferences.Editor getEditor() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.edit();
    }

    public void setCheckingDateForUnlockedSpeakers(String key, String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(UNLOCKED_SPEAKERS_CHECKING_DATE + key, value).apply();
    }

    public String getCheckingDateForUnlocekdSpeakers(String key) {
        SharedPreferences preferences = getPreferences();
        return preferences.getString(UNLOCKED_SPEAKERS_CHECKING_DATE + key, "");
    }

    public void setMomentOfTheLastSpeakerUpdateCheck(String key, String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(UPDATE_SPEAKER_AVAILABLE_DATE + key, value).apply();
    }

    public String getMomentOfTheLastSpeakerUpdateCheck(String key) {
        SharedPreferences preferences = getPreferences();
        return preferences.getString(UPDATE_SPEAKER_AVAILABLE_DATE + key, "");
    }

    public void saveListOfUpdatebleSpeaker(List<String> serialNumbers) {
        // Retrive the old list
        SharedPreferences preferences = getPreferences();
        Set<String> oldSetOfSerialNumbers = preferences.getStringSet(LIST_OF_SPEAKERS_WITH_UPDATE_AVAILABLE, null);
        Set<String> newSetOfSerialNumbers = new HashSet<>(serialNumbers);
        if (oldSetOfSerialNumbers != null) {
            newSetOfSerialNumbers.addAll(oldSetOfSerialNumbers);
        }
        SharedPreferences.Editor editor = getEditor();
        editor.putStringSet(LIST_OF_SPEAKERS_WITH_UPDATE_AVAILABLE, newSetOfSerialNumbers).apply();
    }

    public boolean checkIfSpeakerIsUpdatable(String serialNumber) {
        SharedPreferences preferences = getPreferences();
        Set<String> oldSetOfSerialNumbers = preferences.getStringSet(LIST_OF_SPEAKERS_WITH_UPDATE_AVAILABLE, null);
        return oldSetOfSerialNumbers != null && oldSetOfSerialNumbers.contains(serialNumber);
    }

    public void setMomentOfShowingTheUpdateSpeakerMsg(String key, String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(SHOWING_UPDATE_SPEAKER_AVAILABLE_DATE + key, value).apply();
    }

    public String getMomentOfShowingTheUpdateSpeakerMsg(String key) {
        SharedPreferences preferences = getPreferences();
        return preferences.getString(SHOWING_UPDATE_SPEAKER_AVAILABLE_DATE + key, "");
    }

    public void removeSpeakerFromUpdateList(String serialNumber) {
        SharedPreferences preferences = getPreferences();
        Set<String> serialNumbers = preferences.getStringSet(LIST_OF_SPEAKERS_WITH_UPDATE_AVAILABLE, null);
        serialNumbers.remove(serialNumber);
        SharedPreferences.Editor editor = getEditor();
        editor.putStringSet(LIST_OF_SPEAKERS_WITH_UPDATE_AVAILABLE, serialNumbers).apply();
    }
}

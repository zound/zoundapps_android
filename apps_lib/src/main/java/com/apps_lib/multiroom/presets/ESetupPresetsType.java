package com.apps_lib.multiroom.presets;

/**
 * Created by lsuhov on 29/07/16.
 */

public enum ESetupPresetsType {
    Spotify,
    IR,
    Mixed
}

package com.apps_lib.multiroom.presets.spotify;

import android.text.TextUtils;

import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;
import com.apps_lib.multiroom.Preferences;
import com.apps_lib.multiroom.presets.BlobDecoderFactory;
import com.apps_lib.multiroom.presets.IBlobDecoder;
import com.apps_lib.multiroom.presets.PresetSubType;
import com.apps_lib.multiroom.presets.PresetTypeNames;
import com.apps_lib.multiroom.presets.PresetItemModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SpotifyPlaylistJSONParser {

    public static List<PresetItemModel> getPlaylists(String jsonContent) {
        List<PresetItemModel> playlists = new ArrayList<>();

        if (TextUtils.isEmpty(jsonContent)) {
            return playlists;
        }

        try {
            JSONObject jsonObject = new JSONObject(jsonContent);

            JSONArray playlistsJSONArray = jsonObject.getJSONArray("presets");
            for (int i = 0; i < playlistsJSONArray.length(); i++) {
                if (i == Preferences.NUMBER_OF_PRESETS) {
                    break;
                }

                JSONObject playlistInfo = playlistsJSONArray.getJSONObject(i);

                PresetItemModel presetItemModel = new PresetItemModel();
                presetItemModel.presetName.set(playlistInfo.getString("name"));
                presetItemModel.blob = playlistInfo.getString("blob");
                FsLogger.log("Downloaded blob: " + presetItemModel.blob, LogLevel.Info);

                presetItemModel.presetType.set(PresetTypeNames.Spotify);
                presetItemModel.presetIndex = i;

                String type = playlistInfo.getString("type");
                presetItemModel.presetSubType.set(PresetSubType.presetSubTypeFromString(type));

                JSONArray imagesJsonArray = playlistInfo.getJSONArray("images");
                presetItemModel.artworkUrl.set(getImageUrlFromJsonArray(imagesJsonArray));

                IBlobDecoder blobDecoder = BlobDecoderFactory.getBlobDecoder(presetItemModel.presetType.get());
                if (blobDecoder != null) {
                    blobDecoder.decodeBlob(presetItemModel);
                }

                playlists.add(presetItemModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return playlists;
    }

    public static String getImageUrlFromPlaylistObject(String jsonContent) {
        try {
            JSONObject jsonObject = new JSONObject(jsonContent);

            JSONArray imagesJsonArray = jsonObject.getJSONArray("images");
            return getImageUrlFromJsonArray(imagesJsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "";
    }

    private static String getImageUrlFromJsonArray(JSONArray imagesJsonArray) {
        if (imagesJsonArray.length() > 0) {
            try {
                JSONObject jsonObject = imagesJsonArray.getJSONObject(0);
                return jsonObject.getString("url");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return "";
    }
}

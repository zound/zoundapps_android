package com.apps_lib.multiroom.presets.spotify;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.BaseSpotifyLoggedInState;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSpotifyLoggedInState;
import com.frontier_silicon.NetRemoteLib.Node.NodeSpotifyLoginUsingOauthToken;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.apps_lib.multiroom.R;
import com.frontier_silicon.components.common.CommonPreferences;
import com.frontier_silicon.components.common.ExternalAppsOpener;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.loggerlib.FsLogger;
import com.apps_lib.multiroom.presets.IPresetArtworkUrlListener;
import com.apps_lib.multiroom.presets.IPresetTypeManager;
import com.apps_lib.multiroom.presets.IPresetsRetrieverListener;
import com.apps_lib.multiroom.presets.PresetItemModel;
import com.apps_lib.multiroom.presets.PresetSubType;
import com.apps_lib.multiroom.presets.PresetTypeNames;
import com.spotify.sdk.android.authentication.AuthenticationClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by lsuhov on 22/04/16.
 */
public class SpotifyManager implements IPresetTypeManager {
    private static SpotifyManager mInstance;

    private SpotifyAuthenticator mSpotifyAuthenticator;
    private SpotifyUserModel mUserModel;
    private Context mContext;

    private SpotifyManager() {}

    public static SpotifyManager getInstance() {
        if (mInstance == null) {
            mInstance = new SpotifyManager();
        }

        return mInstance;
    }

    public void init(Context context, String redirectURL, String clientID, String serverEndpoint) {
        mContext = context;
        mSpotifyAuthenticator = new SpotifyAuthenticator();
        mSpotifyAuthenticator.init(redirectURL, clientID, serverEndpoint);
    }

    public SpotifyAuthenticator getSpotifyAuthenticator() {
        return mSpotifyAuthenticator;
    }

    public void getCurrentSpotifyUserInfo(final ISpotifyAccountListener listener) {

        String url = "https://api.spotify.com/v1/me";

        doRequest(url, new IHttpResponseListener() {
            @Override
            public void onResponseReceived(final String response) {
                if (TextUtils.isEmpty(response)) {
                    if (listener != null) {
                        listener.onUserAccountRetrieved(false, false);
                    }
                } else {
                    mUserModel = new SpotifyUserModel();
                    boolean result = mUserModel.parseUserResponse(response);
                    boolean hasUserPremiumAccount = mUserModel.hasUserPremiumAccount();
                    checkPremiumAccount(hasUserPremiumAccount);
                    if (listener != null) {
                        listener.onUserAccountRetrieved(result, hasUserPremiumAccount);
                    }
                }
            }
        });
    }

    public String getAccountName() {
        return mUserModel != null ? mUserModel.getAccountName() : null;
    }

    public void getSpotifyRecommendedPlaylistsAsync(final IPresetsRetrieverListener listener) {
        String url = "https://exp.wg.spotify.com/v1/me/presets?format=blob&count=7&platform=speaker&partner=zound";

        doRequest(url, new IHttpResponseListener() {
                    @Override
                    public void onResponseReceived(String response) {
                        List<PresetItemModel> playlists = SpotifyPlaylistJSONParser.getPlaylists(response);
                        if (listener != null) {
                            listener.onPresetsRetrieved(playlists);
                        }
                    }
                });
    }

    private void retrievePlaylist(String userId, String playlistId, final ISpotifyPlaylistListener listener) {
        String url = "https://api.spotify.com/v1/users/" + userId + "/playlists/" + playlistId;
        String queryParams = "?fields=href,name,owner,images";
        url += queryParams;

        doRequest(url, new IHttpResponseListener() {
            @Override
            public void onResponseReceived(String response) {
                Log.i("SpotifyTest", "RESP: " + response);

                if (listener != null) {
                    listener.onPlaylistRetrieved(response);
                }
            }
        });
    }

    private void retrieveArtist(String artistId, final ISpotifyArtistListener listener) {
        String url = "https://api.spotify.com/v1/artists/" + artistId;
        String queryParams = "?fields=href,name,owner,images";
        url += queryParams;

        doRequest(url, new IHttpResponseListener() {
            @Override
            public void onResponseReceived(String response) {
                Log.i("SpotifyTest", "RESP: " + response);

                if (listener != null) {
                    listener.onArtistRetrieved(response);
                }
            }
        });
    }

    private List<PresetItemModel> filterSpotifyPresets(List<PresetItemModel> presetItemModels) {
        List<PresetItemModel> spotifyPresets = new ArrayList<>();

        for (int i = 0; i < presetItemModels.size(); i++) {
            PresetItemModel presetItemModel = presetItemModels.get(i);
            String presetType = presetItemModel.presetType.get();
            if (!TextUtils.isEmpty(presetType) && presetType.contentEquals(PresetTypeNames.Spotify)) {
                spotifyPresets.add(presetItemModel);
            }
        }

        return spotifyPresets;
    }

    private void doRequest(final String url, final IHttpResponseListener listener) {
        mSpotifyAuthenticator.getAccessTokenAsync(new IAccessTokenListener() {
            @Override
            public void onAccessTokenRetrieved(String accessToken) {
                Request request = new Request.Builder()
                        .url(url)
                        .header("Authorization", accessToken)
                        .build();

                NetRemote.getOkHttpClient().newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {

                        NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                            @Override
                            public void run() {
                                if (listener != null) {
                                    listener.onResponseReceived(null);
                                }
                            }
                        }, true);
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        final String result = response.isSuccessful() ? response.body().string() : "";
                        response.close();

                        NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    if (listener != null) {
                                        listener.onResponseReceived(result);
                                    }
                                } else {
                                    if (listener != null) {
                                        listener.onResponseReceived(null);
                                    }
                                }
                            }
                        }, true);
                    }
                });
            }
        });
    }

    @Override
    public void retrieveArtworkUrlAsync(final PresetItemModel presetItemModel, final IPresetArtworkUrlListener listener) {
        if (!TextUtils.isEmpty(presetItemModel.artworkUrl.get()) && !presetItemModel.isCurrentPresetReplaced) {
            if (listener != null) {
                listener.onArtworkUrlRetrieved(presetItemModel.artworkUrl.get());
            }
            return;
        }

        presetItemModel.isCurrentPresetReplaced = false;

        String url = CommonPreferences.getInstance().getString(presetItemModel.getKeyForUrl());
        if (!TextUtils.isEmpty(url)) {
            presetItemModel.artworkUrl.set(url);
            if (listener != null) {
                listener.onArtworkUrlRetrieved(url);
            }
            return;
        }

        if (presetItemModel.presetSubType.get() == PresetSubType.SpotifyPlaylist) {
            retrieveArtworkUrlFromPlaylist(presetItemModel, listener);
        } else if(presetItemModel.presetSubType.get() == PresetSubType.SpotifyArtist) {
            retrieveArtworkUrlFromArtist(presetItemModel);
        } else {
            if (listener != null) {
                listener.onArtworkUrlRetrieved(null);
            }
        }
    }

    private void retrieveArtworkUrlFromArtist(final PresetItemModel presetItemModel) {
        if (presetItemModel.artistId != null) {
            retrieveArtist(presetItemModel.artistId, new ISpotifyArtistListener() {
                @Override
                public void onArtistRetrieved(String artistString) {
                    if (TextUtils.isEmpty(artistString)) {
                        return;
                    }

                    String artistArtworkUrl = SpotifyPlaylistJSONParser.getImageUrlFromPlaylistObject(artistString);
                    presetItemModel.artworkUrl.set(artistArtworkUrl);

                    CommonPreferences.getInstance().putString(presetItemModel.getKeyForUrl(), presetItemModel.artworkUrl.get());
                }
            });
        }
    }

    private void retrieveArtworkUrlFromPlaylist(final PresetItemModel presetItemModel, final IPresetArtworkUrlListener listener) {
        if (TextUtils.isEmpty(presetItemModel.userId) || TextUtils.isEmpty(presetItemModel.presetId)) {
            if (listener != null) {
                listener.onArtworkUrlRetrieved(null);
            }
            return;
        }

        retrievePlaylist(presetItemModel.userId, presetItemModel.presetId, new ISpotifyPlaylistListener() {
            @Override
            public void onPlaylistRetrieved(String playlistString) {
                if (TextUtils.isEmpty(playlistString)) {
                    return;
                }

                String playlistArtworkUrl = SpotifyPlaylistJSONParser.getImageUrlFromPlaylistObject(playlistString);
                presetItemModel.artworkUrl.set(playlistArtworkUrl);

                CommonPreferences.getInstance().putString(presetItemModel.getKeyForUrl(), presetItemModel.artworkUrl.get());

                if (listener != null) {
                    listener.onArtworkUrlRetrieved(playlistArtworkUrl);
                }
            }
        });
    }

    public void sendAccessTokenToRadioIfNeededAsync(final Radio radio, final RadioNodeUtil.INodeSetResultListener listener) {
        final String accessToken = mSpotifyAuthenticator.getAccessToken();
        if (TextUtils.isEmpty(accessToken)) {
            return;
        }

        RadioNodeUtil.getNodeFromRadioAsync(radio, NodeSpotifyLoggedInState.class, new RadioNodeUtil.INodeResultListener() {
            @Override
            public void onNodeResult(NodeInfo node) {
                NodeSpotifyLoggedInState nodeLoginState = (NodeSpotifyLoggedInState)node;
                if (node == null) {
                    FsLogger.log("SpotifyManager: couldn't get NodeSpotifyLoggedInState");
                    listener.onNodeSetResult(false);
                    return;
                }

                if (nodeLoginState.getValueEnum() == BaseSpotifyLoggedInState.Ord.LOGGED_IN) {
                    listener.onNodeSetResult(false);
                    return;
                }

                NodeSpotifyLoginUsingOauthToken nodeToken = new NodeSpotifyLoginUsingOauthToken(accessToken);

                RadioNodeUtil.setNodeToRadioAsync(radio, nodeToken, new RadioNodeUtil.INodeSetResultListener() {
                    @Override
                    public void onNodeSetResult(boolean success) {
                        listener.onNodeSetResult(success);
                    }
                });
            }
        });
    }

    public void checkPremiumAccount(boolean hasPremiumAccount) {
        if(!hasPremiumAccount) {
            AuthenticationClient.clearCookies(mContext);
            mSpotifyAuthenticator.dispose();
        }
    }

    public boolean checkIfSpotifyAppIsInstalled(Activity activity) {
        String spotifyAppUri = activity.getString(R.string.spotify_app_package_name);

        return ExternalAppsOpener.isAppInstalled(spotifyAppUri, activity);
    }

    public interface ISpotifyPlaylistListener {
        void onPlaylistRetrieved(String playlistString);
    }

    public interface ISpotifyArtistListener {
        void onArtistRetrieved(String artistString);
    }

}

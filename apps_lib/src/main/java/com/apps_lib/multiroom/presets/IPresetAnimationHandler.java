package com.apps_lib.multiroom.presets;

import com.apps_lib.multiroom.util.ui.AnimationEndListener;

/**
 * Created by nbalazs on 12/09/2016.
 */

public interface IPresetAnimationHandler {

    void startAnimationForPresetPlaylistDelete(AnimationEndListener animationEndListener);

    void startAnimationForPresetRadioStationDelete(AnimationEndListener animationEndListener);

    void startAnimationForPresetPlaylistAdded(AnimationEndListener animationEndListener, boolean succeeded);

    void startAnimationForPresetRadioStationAdded(AnimationEndListener animationEndListener, boolean succeeded);

    void startAnimationForPlayingEmptyPreset(AnimationEndListener animationEndListener);

    void startAnimationForUndo(AnimationEndListener animationEndListener, boolean succeeded);

    void startAnimationForRedo(AnimationEndListener animationEndListener, boolean succeeded);

    void showUndoButton();

    void hideUndoButton();

    void showRedoButton();

    void hideRedoButton();
}

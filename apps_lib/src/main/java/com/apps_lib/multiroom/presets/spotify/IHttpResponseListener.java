package com.apps_lib.multiroom.presets.spotify;

/**
 * Created by lsuhov on 02/06/16.
 */
public interface IHttpResponseListener {
    void onResponseReceived(String response);
}

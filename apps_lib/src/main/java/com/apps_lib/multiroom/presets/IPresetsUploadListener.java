package com.apps_lib.multiroom.presets;

import java.util.List;

/**
 * Created by lsuhov on 20/05/16.
 */
public interface IPresetsUploadListener {
    void onUploadingPreset(PresetItemModel presetItemModel, int presetIndex);
    void onComplete(List<PresetItemModel> presetItemModelList, boolean result);
}

package com.apps_lib.multiroom.presets;

/**
 * Created by nbalazs on 04/10/2016.
 */

public interface IBlobEncoder {

    String encodeToString(byte[] data);

    String encodeToString(String data);
}

package com.apps_lib.multiroom.presets;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresets;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNavigationUtil;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.common.nodeCommunication.IListNodeListener;
import com.apps_lib.multiroom.Preferences;
import com.apps_lib.multiroom.presets.spotify.SpotifyManager;

import java.util.List;

/**
 * Created by lsuhov on 22/05/16.
 */
public class PresetsDownloaderTask extends AsyncTask<Void, Void, Boolean> {

    private List<PresetItemModel> mPresetItemModels;
    private Radio mRadio;
    private IPresetsUpdateListener mListener;

    public PresetsDownloaderTask(List<PresetItemModel> presetItemModelList, Radio radio, IPresetsUpdateListener listener) {
        mPresetItemModels = presetItemModelList;
        mRadio = radio;
        mListener = listener;
    }

    public synchronized void setListener(IPresetsUpdateListener listener) {
        this.mListener = listener;
    }

    public Radio getRadio() {
        return mRadio;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        for (int i = 0; i < Preferences.NUMBER_OF_PRESETS; i++) {
            mPresetItemModels.get(i).isLoading.set(true);
        }

        RadioNavigationUtil.resetNavigationToRootIfNeeded(mRadio);

        RadioNodeUtil.getListNodeItems(mRadio, NodeNavPresets.class, -1, 7, true, new IListNodeListener() {
            @Override
            public void onListNodeResult(List resultList, boolean isListComplete) {
                int count = resultList != null ? resultList.size() : 0;

                for (int i = 0; i < Preferences.NUMBER_OF_PRESETS; i++) {
                    PresetItemModel presetItemModel = mPresetItemModels.get(i);
                    presetItemModel.isLoading.set(false);

                    if (i >= count) {
                        break;
                    }

                    NodeNavPresets.ListItem listItem = (NodeNavPresets.ListItem) resultList.get(i);

                    presetItemModel.presetName.set(listItem.getName());
                    presetItemModel.presetType.set(listItem.getType());
                    presetItemModel.blob = listItem.getBlob();
                    presetItemModel.playlistUrl = listItem.getPlaylistUrl();
                    presetItemModel.presetIndex = listItem.getKey().intValue();

                    if (!TextUtils.isEmpty(presetItemModel.presetType.get())) {
                        if (presetItemModel.presetType.get().contentEquals(PresetTypeNames.IR)) {
                            presetItemModel.presetSubType.set(PresetSubType.InternetRadio);
                            presetItemModel.artworkUrl.set(listItem.getArtworkUrl());
                        } else if (presetItemModel.presetType.get().contentEquals(PresetTypeNames.Spotify)) {
                            decodeBlob(presetItemModel);
                            if (presetItemModel.presetSubType.get() == PresetSubType.SpotifyPlaylist || presetItemModel.presetSubType.get() == PresetSubType.SpotifyArtist) {
                                SpotifyManager.getInstance().retrieveArtworkUrlAsync(presetItemModel, null);
                            } else {
                                presetItemModel.artworkUrl.set(listItem.getArtworkUrl());
                            }
                        }
                    } else {
                        presetItemModel.artworkUrl.set(listItem.getArtworkUrl());
                    }
                }
            }
        });

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        synchronized (this) {
            if (mListener != null) {
                mListener.onComplete(result);
            }
        }

        dispose();

        super.onPostExecute(result);
    }


    private void decodeBlob(PresetItemModel presetItemModel) {
        IBlobDecoder blobDecoder = BlobDecoderFactory.getBlobDecoder(presetItemModel.presetType.get());
        if (blobDecoder != null) {
            blobDecoder.decodeBlob(presetItemModel);
        }
    }


    private void dispose() {
        mListener = null;
        mRadio = null;
        mPresetItemModels = null;
    }
}

package com.apps_lib.multiroom.presets;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.text.TextUtils;

/**
 * Created by lsuhov on 24/04/16.
 */
public class PresetItemModel {

    public ObservableField<String> artworkUrl = new ObservableField<>("");
    public ObservableField<String> presetName = new ObservableField<>("");
    public ObservableField<String> presetType = new ObservableField<>("");
    public ObservableField<PresetSubType> presetSubType = new ObservableField<>();
    public ObservableBoolean isLoading = new ObservableBoolean(false);
    public String userId;
    public String artistId;
    public String presetId;
    public String blob = "";
    public String playlistUrl = "";
    public int presetIndex;
    public boolean isCurrentPresetReplaced = false;

    public PresetItemModel() {}

    public PresetItemModel(String name, String type, String blob, String artworkUrl, PresetSubType subType,
                           int presetIndex) {
        presetName.set(name);
        presetType.set(type);
        this.artworkUrl.set(artworkUrl);
        this.blob = blob;
        presetSubType.set(subType);
        this.presetIndex = presetIndex;
    }

    public int getFriendlyPresetIndex() {
        return presetIndex + 1;
    }

    public String getKeyForUrl() {
        if (!TextUtils.isEmpty(presetId)) {
            return presetId;
        } else {
            return blob;
        }
    }
}

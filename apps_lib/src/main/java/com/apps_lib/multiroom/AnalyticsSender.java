package com.apps_lib.multiroom;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.connection.ConnectionState;
import com.frontier_silicon.components.connection.ConnectionStateModel;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.connection.IConnectionStateListener;
import com.apps_lib.multiroom.connection.ConnectionManager;

/**
 * Created by cvladu on 12/10/16.
 */

public class AnalyticsSender implements IConnectionStateListener {

    private static AnalyticsSender mInstance;

    private AnalyticsSender() {}

    public static AnalyticsSender getInstance() {
        if (mInstance == null) {
            mInstance = new AnalyticsSender();
        }

        return mInstance;
    }

    public void startListeningToConnectionStateEvents() {
        ConnectionStateUtil.getInstance().addListener(this, false);
    }

    @Override
    public void onStateUpdate(ConnectionState newState) {
        AnalyticsSender.ConnectionLostReason connectionLostReason = null;

        switch (newState) {

            case DISCONNECTED:
                String lastSelectedDeviceSN = ConnectionManager.getInstance().getLastSelectedDeviceSN();
                Radio radio = NetRemoteManager.getInstance().getRadioFromSN(lastSelectedDeviceSN);

                if (radio != null && !radio.isMissing()) {
                    ConnectionStateModel connectionStateModel = ConnectionStateUtil.getInstance().getConnectionStateModel();
                    if (connectionStateModel.mNotificationTimeout) {
                        connectionLostReason = AnalyticsSender.ConnectionLostReason.NOTIFICATION_TIMEOUT;
                    } else {
                        connectionLostReason = AnalyticsSender.ConnectionLostReason.DISCONNECTED;
                    }
                } else {
                    connectionLostReason = AnalyticsSender.ConnectionLostReason.SPEAKER_MISSING;
                }
                break;
            case INVALID_SESSION:
                connectionLostReason = AnalyticsSender.ConnectionLostReason.SESSION_STOLEN;
                break;
        }

        if (connectionLostReason != null) {
            sendConnectionLostAnalytics(connectionLostReason);
        }
    }

    public enum ConnectionLostReason {
        SPEAKER_MISSING,
        DISCONNECTED,
        NOTIFICATION_TIMEOUT,
        SESSION_STOLEN,
    }


    public static void sendRefreshingScreenAnalytics() {
        CustomEvent customEvent = new CustomEvent("ForceRefreshHomeScreen");


        Answers.getInstance().logCustom(customEvent);
    }

    public static void sendConnectionLostAnalytics(ConnectionLostReason reason) {
        CustomEvent customEvent = new CustomEvent("ConnectionLost");
        switch (reason) {
            case SPEAKER_MISSING:
                customEvent.putCustomAttribute("Reason", "Speaker missing");
                break;
            case DISCONNECTED:
                customEvent.putCustomAttribute("Reason", "Disconnected");
                break;
            case NOTIFICATION_TIMEOUT:
                customEvent.putCustomAttribute("Reason", "Notification Timeout");
                break;
            case SESSION_STOLEN:
                customEvent.putCustomAttribute("Reason", "Session stolen");
                break;
            default:
                break;
        }

        Answers.getInstance().logCustom(customEvent);
    }

    public static void sendConnectedFromHomeScreenAnalytics(boolean success, long duration) {
        CustomEvent customEvent = new CustomEvent("ConnectedFromHomeScreen").putCustomAttribute("Status", success ? "Connected" : "Failed");
        if (success) {
            customEvent.putCustomAttribute("Duration", duration);
        }
        Answers.getInstance().logCustom(customEvent);
    }
}

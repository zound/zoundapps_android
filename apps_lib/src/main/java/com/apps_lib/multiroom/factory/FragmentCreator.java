package com.apps_lib.multiroom.factory;

/**
 * Created by nbalazs on 10/05/2017.
 */

public abstract class FragmentCreator {
    public abstract java.lang.Class getScanExistingSpeakersFragment();

    public abstract java.lang.Class getHomeFragment();

    public abstract java.lang.Class getConnectionLostFragment();

    public abstract java.lang.Class getNoWiFiFragment();

    public abstract java.lang.Class getScanNewSpeakerFragment();

    public abstract java.lang.Class getNoSpeakerFoundFragment(int tag);

}

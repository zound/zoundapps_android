package com.apps_lib.multiroom.factory;

/**
 * Created by nbalazs on 10/05/2017.
 */

public abstract class ActivityCreator {

    public abstract java.lang.Class getHomeActivity();

    public abstract java.lang.Class getSetupActivity();

    public abstract java.lang.Class getVolumeActivity();

    public abstract java.lang.Class getSourcesActivity();

    public abstract java.lang.Class getSettingsActivity();

    public abstract java.lang.Class getAboutScreenActivity();

    public abstract java.lang.Class getCastTosActivity();

    public abstract java.lang.Class getPresetAddingFailedActivity();

    public abstract java.lang.Class getSetPresetsActivity();

    public abstract java.lang.Class getFinalSetupActivity();

    public abstract java.lang.Class getPresetsListActivity();

    public abstract java.lang.Class getSpotifyLinkingSuccessActivity();

    public abstract java.lang.Class getSpotifyAuthenticationActivity();
}

package com.apps_lib.multiroom.source;

/**
 * Created by nbalazs on 16/05/2017.
 */

public interface IPresetStatus {
    void onPresetResponse(boolean success);
}

package com.apps_lib.multiroom.source;

import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;

public enum SourceTab {
    PresetTab,
    AuxinTab,
    RCATab,
    BluetoothTab,
    CloudTab;

    public static int getValue(SourceTab tab) {
        int returnValue = -1;

        switch (tab) {
            case PresetTab:
                returnValue = 0;
                break;
            case CloudTab:
                returnValue = -1;
                break;
            case BluetoothTab:
                returnValue = -2;
                break;
            case RCATab:
                returnValue = isRCAPresent() ? -3 : 0;
                break;
            case AuxinTab:
                returnValue = isRCAPresent() ? -4 : -3;
                break;
            default:
                break;
        }

        return returnValue;
    }

    private static boolean isRCAPresent() {
        NodeSysCapsValidModes modes = SourcesManager.getInstance().getSpeakerModes();
        return modes == null ? false : modes.containsMode(NodeSysCapsValidModes.Mode.RCA);
    }
}

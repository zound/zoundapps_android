package com.apps_lib.multiroom.source;

import android.text.TextUtils;

import com.apps_lib.multiroom.Preferences;
import com.apps_lib.multiroom.connection.ConnectionManager;
import com.apps_lib.multiroom.connection.IPresetDownloaderAtConnection;
import com.apps_lib.multiroom.nowPlaying.NowPlayingDataModel;
import com.apps_lib.multiroom.nowPlaying.NowPlayingManager;
import com.apps_lib.multiroom.presets.IPresetsUpdateListener;
import com.apps_lib.multiroom.presets.PresetItemModel;
import com.apps_lib.multiroom.presets.PresetTypeNames;
import com.apps_lib.multiroom.presets.PresetsManager;
import com.apps_lib.multiroom.presets.spotify.SpotifyManager;
import com.frontier_silicon.NetRemoteLib.Node.BaseSpotifyLoggedInState;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetCurrentPreset;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetListversion;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayAddPresetStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodeSpotifyLoggedInState;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Radio.IGetNodeCallback;
import com.frontier_silicon.NetRemoteLib.Radio.INodeNotificationCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNavigationUtil;
import com.frontier_silicon.components.common.RadioNodeUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuhov on 20/05/16.
 */
public class SourcesManager implements INodeNotificationCallback {
    private static SourcesManager mInstance;

    private Radio mRadio;
    private ISourcesListener mListener;
    private IPresetDownloaderAtConnection mPresetDownloaderListener;

    public List<PresetItemModel> mPresetItemModels;

    private NodeSysCapsValidModes mAvailableModes = null;

    private IPresetStatus mPresetStatusListener;

    public static SourcesManager getInstance() {
        if (mInstance == null) {
            mInstance = new SourcesManager();
        }

        return mInstance;
    }

    private SourcesManager() {
        mPresetItemModels = new ArrayList<>();

        addEmptyPresets();
    }

    private void addEmptyPresets() {
        for (int i = 0; i < Preferences.NUMBER_OF_PRESETS; i++) {
            mPresetItemModels.add(new PresetItemModel());
        }
    }

    public void bindToRadio(IPresetDownloaderAtConnection listener) {
        mPresetDownloaderListener = listener;

        unbindFromPreviousRadio();

        Radio radio = ConnectionManager.getInstance().getSelectedRadio();
        if (radio == null) {
            return;
        }

        mRadio = radio;
        activateNotificationSystem();

        updateListOfPresets();
        updateCurrentlyPlayingPreset(true);

        getAvailableModes(true);
    }

    private void getAvailableModes(final boolean firstTime) {
        mRadio.getListNode(NodeSysCapsValidModes.class, true, new IGetNodeCallback() {
            @Override
            public void getNodeResult(NodeInfo node) {
                mAvailableModes = (NodeSysCapsValidModes) node;
            }

            @Override
            public void getNodeError(Class nodeType, NodeErrorResponse error) {
                if (firstTime) {
                    getAvailableModes(false);
                }
            }
        });
    }

    public void setListener(ISourcesListener listener) {
        mListener = listener;
    }

    public PresetItemModel getPresetAtPosition(int position) {
        if (position < mPresetItemModels.size()) {
            return mPresetItemModels.get(position);
        }

        //This can happen when index is 99 - related to recalling of preset from client
        return null;
    }

    public void selectPresetAsync(int index, RadioNodeUtil.INodeSetResultListener listener) {
        PresetsManager.getInstance().selectPresetAsync(index, mRadio, listener);
    }

    private void unbindFromPreviousRadio() {
        deactivateNotificationSystem();
        clearPresets();
    }

    private void clearPresets() {
        for (int i = 0; i < mPresetItemModels.size(); i++) {
            clearPresetModel(mPresetItemModels.get(i));
        }
    }

    private void clearPresetModel(PresetItemModel presetItemModel) {
        presetItemModel.presetName.set("");
        presetItemModel.presetType.set("");
        presetItemModel.presetSubType.set(null);
        presetItemModel.artworkUrl.set("");
        presetItemModel.blob = "";
        presetItemModel.playlistUrl = "";
    }

    private void activateNotificationSystem() {
        if (mRadio.isConnectionOk()) {
            mRadio.addNotificationListener(this);
        }
    }

    private void deactivateNotificationSystem() {
        if (mRadio != null) {
            mRadio.removeNotificationListener(this);
        }
    }

    @Override
    public void onNodeUpdated(NodeInfo node) {
        if (node instanceof NodePlayAddPresetStatus) {
            updateListOfPresets();
            NodePlayAddPresetStatus nodeStatus = (NodePlayAddPresetStatus) node;
            handleAddPresetStatus(nodeStatus);
        } else if (node instanceof NodeNavPresetListversion) {
            updateListOfPresets();
            updateCurrentlyPlayingPreset(false);
        } else if (node instanceof NodeNavPresetCurrentPreset) {
            updateCurrentlyPlayingPreset(false);
        }
    }

    private void updateCurrentlyPlayingPreset(boolean sync) {
        Radio selectedRadio = ConnectionManager.getInstance().getSelectedRadio();

        PresetsManager.getInstance().getIndexOfCurrentlyPlayingPreset(selectedRadio, sync, new RadioNodeUtil.INodeResultListener() {
            @Override
            public void onNodeResult(NodeInfo node) {
                NowPlayingDataModel dataModel = NowPlayingManager.getInstance().getNowPlayingDataModel();

                if (node == null) {
                    dataModel.indexOfCurrentlyPlayingPreset.set(-1);
                } else {
                    int presetIndex = ((NodeNavPresetCurrentPreset)node).getValue().intValue();

                    //index 99 means that preset is recalled from client
                    dataModel.indexOfCurrentlyPlayingPreset.set(presetIndex >=0 ? presetIndex : -1);
                }
            }
        });
    }

    private void handleAddPresetStatus(NodePlayAddPresetStatus node) {
        if (node == null || mPresetStatusListener == null) {
            return;
        }

        if (node.getValueEnum().ordinal() == 1) {
            mPresetStatusListener.onPresetResponse(false);
        } else {
            mPresetStatusListener.onPresetResponse(true);
        }
    }

    public void setPresetStatusListener(IPresetStatus presetStatusListener) {
        mPresetStatusListener = presetStatusListener;
    }

     public void updateListOfPresets() {
        PresetsManager.getInstance().getPresetsFromRadio(mPresetItemModels, mRadio, new IPresetsUpdateListener() {
            @Override
            public void onComplete(boolean result) {
                if (result) {
                    checkIfSpotifyAccountIsNeededButMissing();

                    if (mPresetDownloaderListener != null) {
                        mPresetDownloaderListener.onPresetDownloaderFinished();
                    }
                }
            }
        });
    }

    private void checkIfSpotifyAccountIsNeededButMissing() {

        boolean spotifyPresetIsStored = false;
        for (PresetItemModel presetItemModel : mPresetItemModels) {
            if (!TextUtils.isEmpty(presetItemModel.presetType.get()) &&
                    PresetTypeNames.Spotify.contentEquals(presetItemModel.presetType.get())) {
                spotifyPresetIsStored = true;
                break;
            }
        }

        if (spotifyPresetIsStored && mListener != null && mRadio != null) {
            RadioNodeUtil.getNodeFromRadioAsync(mRadio, NodeSpotifyLoggedInState.class, new RadioNodeUtil.INodeResultListener() {
                @Override
                public void onNodeResult(NodeInfo node) {
                    NodeSpotifyLoggedInState loggedInStateNode = (NodeSpotifyLoggedInState) node;
                    if (loggedInStateNode == null || loggedInStateNode.getValueEnum() == BaseSpotifyLoggedInState.Ord.LOGGED_IN) {
                        return;
                    }

                    boolean isSpotifyLinked = SpotifyManager.getInstance().getSpotifyAuthenticator().isSpotifyLinked();

                    if (!isSpotifyLinked) {
                        if (mRadio == null) {
                            return;
                        }
                        SpotifyManager.getInstance().sendAccessTokenToRadioIfNeededAsync(mRadio, new RadioNodeUtil.INodeSetResultListener() {
                            @Override
                            public void onNodeSetResult(boolean success) {
                            }
                        });

                        if (mListener != null) {
                            mListener.spotifyAccountNeeded();
                        }
                    }
                }
            });
        }
    }

    public void deletePresetAtPositionAsync(final int presetIndex) {
        RadioNavigationUtil.enableNavigationAsync(mRadio, new RadioNavigationUtil.IEnableRadioRequestStatusListener() {
            @Override
            public void onEnableFinished(NodeNavStatus nodeNavStatus) {
                PresetsManager.getInstance().deletePreset(presetIndex, mRadio, new RadioNodeUtil.INodeSetResultListener() {
                    @Override
                    public void onNodeSetResult(boolean success) {
                    }
                });
            }
        });
    }

    public void deletePresetAtPositionAsyncWithListener(final int presetIndex, final RadioNodeUtil.INodeSetResultListener listener) {
        RadioNavigationUtil.enableNavigationAsync(mRadio, new RadioNavigationUtil.IEnableRadioRequestStatusListener() {
            @Override
            public void onEnableFinished(NodeNavStatus nodeNavStatus) {
                PresetsManager.getInstance().deletePreset(presetIndex, mRadio, new RadioNodeUtil.INodeSetResultListener() {
                    @Override
                    public void onNodeSetResult(boolean success) {
                        listener.onNodeSetResult(success);
                    }
                });
            }
        });
    }

    public void addCurrentlyPlayingSourceToPreset(final int presetIndex, final RadioNodeUtil.INodeSetResultListener listener) {
        PresetItemModel presetItemModel = mPresetItemModels.get(presetIndex);
        presetItemModel.isCurrentPresetReplaced = true;

        PresetsManager.getInstance().addCurrentlyPlayingSourceToPreset(presetIndex, mRadio, new RadioNodeUtil.INodeSetResultListener() {
            @Override
            public void onNodeSetResult(boolean success) {
                if (listener != null) {
                    listener.onNodeSetResult(success);
                }
            }
        });
    }

    public NodeSysCapsValidModes getSpeakerModes() {
        return mAvailableModes;
    }
}

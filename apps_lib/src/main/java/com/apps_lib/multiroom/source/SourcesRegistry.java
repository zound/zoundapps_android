package com.apps_lib.multiroom.source;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nbalazs on 06/09/2017.
 */

public class SourcesRegistry {

    private int PAGE_COUNT;
    private int FIRST_PAGE;
    private int LOOPS = 1000;

    private static SourcesRegistry mInstance;

    private final List<SourceEntry> listOfSources = new ArrayList<>();

    // Call this to initialise the carousel
    public static void init() {
        mInstance = new SourcesRegistry();
    }

    public static SourcesRegistry getInstance() {
        return mInstance;
    }

    private SourcesRegistry() { }

    public final void register(SourceTab tab, int resourceID) {
        listOfSources.add(new SourceEntry(tab, resourceID, listOfSources.size()));
    }

    public final void register(SourceTab tab, String text) {
        listOfSources.add(new SourceEntry(tab, text, listOfSources.size()));
    }

    public final void defineCarouselSize() {
        PAGE_COUNT = listOfSources.size();
        FIRST_PAGE = PAGE_COUNT * LOOPS / 2;
    }

    public final int getResourceIdBasedOnVirtualPosition(int virtualPosition) {
        return listOfSources.get(virtualPosition).mResourceID;
    }

    public final SourceTab getSourceTabBasedOnVirtualPosition(int virtualPosition) {
        return  listOfSources.get(virtualPosition).mTab;
    }

    public final String getTextBasedOnVirtualPosition(int virtualPosition) {
        return listOfSources.get(virtualPosition).mText;
    }

    public final int getFirstPage() {
        return FIRST_PAGE;
    }

    public final int getPageCount() {
        return PAGE_COUNT;
    }

    public final int getTotalPageNumbers() {
        return PAGE_COUNT * LOOPS;
    }

    public final int getVirtualPosition(int realPosition) {
        return realPosition % PAGE_COUNT;
    }

    private class SourceEntry {
        SourceTab mTab;
        int mResourceID;
        int mIndex;
        String mText;

        SourceEntry(SourceTab tab, int resourceID, int index) {
            mTab = tab;
            mResourceID = resourceID;
            mIndex = index;
            mText = null;
        }

        SourceEntry(SourceTab tab, String text, int index) {
            mTab = tab;
            mResourceID = -1;
            mIndex = index;
            mText = text;
        }
    }
}

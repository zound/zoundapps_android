package com.apps_lib.multiroom.source;

import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Created by nbalazs on 16/01/2017.
 */
public abstract class SourceFragment extends Fragment {

    public static final float LAYOUT_ALPHA_100 = 1.0f;

    public static final float LAYOUT_ALPHA_75 = 0.75f;

    public static final float LAYOUT_ALPHA_25 = 0.25f;

    public static final float LAYOUT_ALPHA_5 = 0.05f;

    public static final float LAYOUT_ALPHA_0 = 0.0f;

    public static final float LAYOUT_SCALE_100 = 1.0f;

    public static final float LAYOUT_SCALE_90 = 0.90f;

    public enum EFragmentPosition{
        PRIMARY,
        SECONDARY
    }

    public enum ESourceFragmentType{
        PRESET_SOURCE_FRAGMENT,
        SIMPLE_SOURCE_FRAGMENT
    }

    public abstract void setFragmentAlphaForOffset(float currentOffset, EFragmentPosition fragmentPosition);

    public abstract void setFragmentAlphaForPosition(EFragmentPosition fragmentPosition);

    public abstract void setFadeOutAlpha(float fadeOutAlpha, boolean adjustAlphaIfNeeded);

    public abstract void onOffsetChanged(float offset, int offsetPixel, boolean leftToRight, boolean rightToLeft);

    protected abstract void setViewClickable(boolean clickable);

    public abstract ESourceFragmentType getFragmentType();

    public abstract void setScaleToOffset(float currentOffset, EFragmentPosition fragmentPosition);

    protected final void changeViewVisibilityIfNeeded(View view) {
        if (view != null) {
            float alpha = view.getAlpha();
            if (alpha < LAYOUT_ALPHA_5) {
                view.setVisibility(View.GONE);
            } else {
                view.setVisibility(View.VISIBLE);
            }
        }
    }
}

package com.apps_lib.multiroom;

import android.app.Dialog;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.apps_lib.multiroom.util.BlurBuilder;

/**
 * Created by nbalazs on 20/03/2017.
 */
public class AnimatedDialogFragmentBase extends DialogFragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        Window window = dialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
            if (attributes != null) {
                attributes.windowAnimations = R.style.DialogFragmentAnimation;
            }
        }
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            BitmapDrawable backgroundDrawable = BlurBuilder.getInstance().getBlurredAndZoomedScreenshotOfActivity(getActivity());
            if (backgroundDrawable != null) {
                backgroundDrawable.setColorFilter(ContextCompat.getColor(getContext(), R.color.blurred_background_color_filter), PorterDuff.Mode.SRC_ATOP);
                window.setBackgroundDrawable(backgroundDrawable);
            }
        }
    }
}

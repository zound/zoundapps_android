package com.apps_lib.multiroom;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by lsuhov on 20/05/16.
 */
public class Preferences {

    private static Preferences mInstance;

    private final String MULTIROOM_CAST_DIALOG_WAS_SHOWN_KEY = "MULTIROOM_CAST_DIALOG_WAS_SHOWN_KEY";

    private Preferences() {}

    public static Preferences getInstance() {
        if (mInstance == null) {
            mInstance = new Preferences();
        }
        return mInstance;
    }

    public final static int NUMBER_OF_PRESETS = 7;

    private Context mContext;

    public void init(Context context) {
        mContext = context;
    }

    private SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    private SharedPreferences.Editor getEditor() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.edit();
    }

    public boolean getMultiroomCastDialogWasShown() {
        SharedPreferences sharedPreferences = getPreferences();
        return sharedPreferences.getBoolean(MULTIROOM_CAST_DIALOG_WAS_SHOWN_KEY, false);
    }

    public void setMultiroomCastDialogWasShown(boolean wasShown) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(MULTIROOM_CAST_DIALOG_WAS_SHOWN_KEY, wasShown);

        editor.apply();
    }

}

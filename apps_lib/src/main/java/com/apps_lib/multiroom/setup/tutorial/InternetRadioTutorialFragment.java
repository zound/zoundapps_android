package com.apps_lib.multiroom.setup.tutorial;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps_lib.multiroom.R;

/**
 * Created by lsuhov on 14/04/16.
 */
public class InternetRadioTutorialFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return DataBindingUtil.inflate(inflater, R.layout.fragment_setup_tutorial_internet_radio, container, false).getRoot();
    }
}

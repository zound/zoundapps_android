package com.apps_lib.multiroom.setup.normalSetup;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.databinding.ActivityHiddenSsidEncChooserBinding;
import com.apps_lib.multiroom.UEActivityBase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by nbalazs on 17/10/2016.
 */
public class HiddenSSIDEncTypeChooserActivity extends UEActivityBase {

    private ActivityHiddenSsidEncChooserBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = LayoutInflater.from(this).inflate(R.layout.activity_hidden_ssid_enc_chooser, null);
        mBinding = DataBindingUtil.bind(rootView);
        setContentView(rootView);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.ssid_enc_types_chooser_title);

        initListView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initListView() {
        final String[] encTypeList = getResources().getStringArray(R.array.enc_types_array);
        final List<String> encTypeListArray = new ArrayList<>(Arrays.asList(encTypeList));
        HiddenSSIDTextAdapter adapter = new HiddenSSIDTextAdapter(this, R.layout.auth_enc_list_item, encTypeListArray);
        mBinding.encOptionsListView.setAdapter(adapter);
        mBinding.encOptionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                finishWithResult(encTypeListArray.get(position));
            }
        });
    }

    private void finishWithResult(String securityType) {
        Intent data = new Intent();
        data.putExtra("ENC_TYPE_AS_STRING", securityType);
        setResult(RESULT_OK, data);
        finish();
    }
}

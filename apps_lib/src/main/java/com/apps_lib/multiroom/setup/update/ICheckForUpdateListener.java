package com.apps_lib.multiroom.setup.update;

public interface ICheckForUpdateListener {
	void onAvailableUpdate();
	void onNoAvailableUpdate();
	void onCheckFailed();
}

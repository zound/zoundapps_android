package com.apps_lib.multiroom.setup.normalSetup;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.databinding.ActivityTroubleshootingBinding;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;

/**
 * Created by nbalazs on 22/03/2017.
 */

public class TroubleshootingActivity extends UEActivityBase {

    private static final String SUPPORT_EMAIL_ADDRESS = "support@urbanears.com";

    private ActivityTroubleshootingBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = LayoutInflater.from(this).inflate(R.layout.activity_troubleshooting, null);
        mBinding = DataBindingUtil.bind(view);
        setContentView(view);

        setupControls();
    }

    private void setupControls() {
        mBinding.buttonRestartApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.startSetupActivity(TroubleshootingActivity.this);
            }
        });

        mBinding.buttonContactSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TroubleshootingActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        intent.setData(Uri.parse("mailto:" + SUPPORT_EMAIL_ADDRESS));
                        startActivity(intent);
                    }
                });
            }
        });

        mBinding.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationHelper.goToHomeAndReconnectToInitialWiFi(TroubleshootingActivity.this);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
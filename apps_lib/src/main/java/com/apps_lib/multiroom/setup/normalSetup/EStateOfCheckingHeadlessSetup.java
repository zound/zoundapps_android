package com.apps_lib.multiroom.setup.normalSetup;

public enum EStateOfCheckingHeadlessSetup {
	None,
	ConfiguringAudioDevice,
	ConfiguringWithWPS,
	NodesWereNotSetCorrectly,
	ConnectingToSelectedWiFi,
	WaitOnConnectingWithWPS,
	CouldntConnectToWiFi,
	SearchingTheAudioDevice,
	CouldntFindDeviceWithSSDP,
	SuccesfullSetup
}

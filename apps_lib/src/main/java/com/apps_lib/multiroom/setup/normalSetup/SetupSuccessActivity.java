package com.apps_lib.multiroom.setup.normalSetup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.databinding.ActivitySetupSuccessBinding;
import com.apps_lib.multiroom.setup.presets.ChoosePresetsTypeActivity;
import com.apps_lib.multiroom.setup.tutorial.SetupTutorialActivity;
import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.SpeakerImageRetriever;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;

/**
 * Created by lsuhov on 13/04/16.
 */
public class SetupSuccessActivity extends UEActivityBase {

    private ActivitySetupSuccessBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = LayoutInflater.from(this).inflate(R.layout.activity_setup_success, null);
        mBinding = DataBindingUtil.bind(view);
        setContentView(view);

        setupAppBar();

        setTitle(R.string.setup_android_title_success);

        setupControls();
    }

    @Override
    public void onBackPressed() {
        //Don't let to go back
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavigationHelper.goToHomeAndClearActivityStack(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupControls() {
        mBinding.buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToActivity(SetupSuccessActivity.this, ChoosePresetsTypeActivity.class, NavigationHelper.AnimationType.SlideToLeft);
            }
        });

        mBinding.goToCloudTutorialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToActivity(SetupSuccessActivity.this, SetupTutorialActivity.class, NavigationHelper.AnimationType.SlideToLeft);
            }
        });

        String nameOfSpeaker = SetupManager.getInstance().getFriendlyName();
        if (!TextUtils.isEmpty(nameOfSpeaker)) {
            mBinding.nameOfSpeakerTextView.setText(nameOfSpeaker);
        }

        int imageId = SpeakerManager.getInstance().getImageIdForSSID(SetupManager.getInstance().getSSIDOfConfiguredRadio(),
                ESpeakerImageSizeType.Large);
        mBinding.speakerImageView.setImageResource(imageId);
    }
}

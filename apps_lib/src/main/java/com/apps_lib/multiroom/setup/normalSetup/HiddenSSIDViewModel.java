package com.apps_lib.multiroom.setup.normalSetup;

import android.app.Activity;
import android.databinding.ObservableField;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.databinding.ActivityHiddenSsidBinding;
import com.frontier_silicon.components.setup.RadioNetworkConfig.EConnectionType;
import com.frontier_silicon.components.setup.RadioNetworkConfig.RadioNetworkConfigParams;
import com.frontier_silicon.components.setup.RadioNetworkConfig.StaticIPAddressesParam;
import com.apps_lib.multiroom.NavigationHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by nbalazs on 13/10/2016.
 */
public class HiddenSSIDViewModel {

    private Activity mActivity;
    private ActivityHiddenSsidBinding mBinding;
    private StaticIPAddressesParam mStaticIPAddresses = null;
    private boolean mUseManualSetup = false;
    public ObservableField<String> mAuthType;
    public ObservableField<String> mEncType;


    HiddenSSIDViewModel(Activity activity, ActivityHiddenSsidBinding binding) {
        mActivity = activity;
        mBinding = binding;
        mAuthType = new ObservableField<>(mActivity.getResources().getStringArray(R.array.auth_types_array)[0]);
        mEncType = new ObservableField<>(mActivity.getResources().getStringArray(R.array.enc_types_array)[0]);
    }

    @SuppressWarnings("unused")
    public void onConnectClick(View view) {
        RadioNetworkConfigParams configParams = new RadioNetworkConfigParams();

        configParams.mConnectionType = EConnectionType.WiFi;

        configParams.mSSID = mBinding.networkName.getText().toString();
        configParams.mWiFiPasscode = mBinding.passwordText.getText().toString();
        configParams.mAuthType = getSelectedAuthType();
        configParams.mEncryptionType = getSelectedEncType();

        if (!mUseManualSetup) {
            configParams.mUseDhcp = true;
        } else {
            if (extractStaticIPs()) {
                Toast.makeText(mActivity, "Mandatory data is missing!", Toast.LENGTH_SHORT).show();
                return;
            }
            configParams.mUseDhcp = false;
            configParams.mStaticIPAdresses = mStaticIPAddresses;
        }

        SetupManager.getInstance().setNetworkConfigParams(configParams);
        notifyOnConfigReadyToCommit();

    }

    private long getSelectedAuthType() {
        String selected = mAuthType.get();
        List<String> authType = new ArrayList<>(Arrays.asList(mActivity.getResources().getStringArray(R.array.auth_types_array)));
        return authType.indexOf(selected);
    }

    private long getSelectedEncType() {
        String selected = mEncType.get();
        List<String> encType = new ArrayList<>(Arrays.asList(mActivity.getResources().getStringArray(R.array.enc_types_array)));
        return encType.indexOf(selected);
    }

    private void notifyOnConfigReadyToCommit() {
        try {
            IConfigNetworkListener activityCallback = (IConfigNetworkListener) mActivity;
            activityCallback.onConfigReadyToCommit();
        } catch (ClassCastException e) {
            throw new ClassCastException(mActivity.toString() + " must implement IConfigNetworkListener");
        }
    }

    @SuppressWarnings("unused")
    public void onOpenAuthTypeChooserActivityClicked(View view) {
        NavigationHelper.goToActivityForResult(mActivity, HiddenSSIDAuthTypeChooserActivity.class, NavigationHelper.AnimationType.SlideToLeft, null, HiddenSSIDActivity.REQUEST_CODE_AUTH);
    }

    @SuppressWarnings("unused")
    public void onOpenEncTypeChooserActivityClicked(View view) {
        NavigationHelper.goToActivityForResult(mActivity, HiddenSSIDEncTypeChooserActivity.class, NavigationHelper.AnimationType.SlideToLeft, null, HiddenSSIDActivity.REQUEST_CODE_ENC);
    }

    public void onShowPasswordClicked(View view) {
        if (view.getId() == mBinding.showPasswordLabel.getId()) {
            if (mBinding.showPasswordCheckBox.isChecked()) {
                mBinding.showPasswordCheckBox.setChecked(false);
            } else {
                mBinding.showPasswordCheckBox.setChecked(true);
            }
        }
        if (mBinding.showPasswordCheckBox.isChecked()) {
            mBinding.passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        } else {
            mBinding.passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        mBinding.passwordText.setSelection(mBinding.passwordText.getText().length());
    }

    @SuppressWarnings("unused")
    public void onUseDhcpClicked(View view) {
        if (view.getId() == mBinding.useDhcpLabel.getId()) {
            if (mBinding.useDhcpCheckBox.isChecked()) {
                mBinding.useDhcpCheckBox.setChecked(false);
            } else {
                mBinding.useDhcpCheckBox.setChecked(true);
            }
        }

        if (mBinding.useDhcpCheckBox.isChecked()) {
            mBinding.dhcpLayout.setVisibility(View.GONE);
            mUseManualSetup = false;
        } else {
            mBinding.dhcpLayout.setVisibility(View.VISIBLE);
            mUseManualSetup = true;
        }
    }

    private boolean extractStaticIPs() {
        String ip = mBinding.ipAddress.getText().toString();
        String gateway = mBinding.gatewayAddress.getText().toString();
        String subnet = mBinding.subnetMask.getText().toString();
        String dns1 = mBinding.primaryAddress.getText().toString();
        String dns2 = mBinding.secondaryAddress.getText().toString();

        boolean isMissingMandatoryIP = (TextUtils.isEmpty(ip) || TextUtils.isEmpty(gateway) || TextUtils.isEmpty(subnet));

        if (!isMissingMandatoryIP) {
            mStaticIPAddresses = new StaticIPAddressesParam();
            mStaticIPAddresses.mIPAdress = ip;
            mStaticIPAddresses.mGatewayAdress = gateway;
            mStaticIPAddresses.mSubnetMask = subnet;
            mStaticIPAddresses.mPrimaryDNSAdress = dns1;
            mStaticIPAddresses.mSecondaryDNSAdress = dns2;
        }

        return isMissingMandatoryIP;
    }
}

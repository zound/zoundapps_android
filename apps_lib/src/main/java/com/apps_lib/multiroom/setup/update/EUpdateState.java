package com.apps_lib.multiroom.setup.update;

/**
 * Created by lsuhov on 10/08/16.
 */

public enum EUpdateState {
    None,
    CheckingUpdate,
    DownloadingUpdate,
    Updating,
    Finished,
    Error
}

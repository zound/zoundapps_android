package com.apps_lib.multiroom.setup.tutorial;

/**
 * Created by lsuhov on 14/04/16.
 */
public interface INavigationListener {
    void goToNextPage();
    void goToPreviousPage();
}

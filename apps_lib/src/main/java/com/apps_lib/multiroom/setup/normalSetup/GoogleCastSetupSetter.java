package com.apps_lib.multiroom.setup.normalSetup;

import com.frontier_silicon.NetRemoteLib.Node.NodeCastTos;
import com.frontier_silicon.NetRemoteLib.Node.NodeCastUsageReport;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;

/**
 * Created by lsuhov on 04/06/16.
 */
class GoogleCastSetupSetter {

    private Radio mRadio;

    GoogleCastSetupSetter(Radio radio) {
        mRadio = radio;
    }

    boolean setCastAcceptance() {
        if (mRadio == null) {
            return false;
        }

        NodeCastTos nodeCastTosStatus = (NodeCastTos) mRadio.getNodeSyncGetter(NodeCastTos.class).get();

        if (nodeCastTosStatus != null && nodeCastTosStatus.getValueEnum() != NodeCastTos.Ord.ACTIVE) {
            mRadio.setNode(new NodeCastTos(NodeCastTos.Ord.ACTIVE), true);
        }

        NodeCastUsageReport nodeCastUsageReport = (NodeCastUsageReport) mRadio.getNodeSyncGetter(NodeCastUsageReport.class).get();

        if(nodeCastUsageReport != null && nodeCastUsageReport.getValueEnum() != NodeCastUsageReport.Ord.ACTIVE) {
            mRadio.setNode(new NodeCastUsageReport(NodeCastUsageReport.Ord.ACTIVE), true);
        }

        return true;
    }
}

package com.apps_lib.multiroom.setup.update;

import com.frontier_silicon.NetRemoteLib.Node.NodeSysIsuControl;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysIsuState;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.FsComponentsConfig;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

/**
 * Created by lsuhov on 10/08/16.
 * Taken from Undok and striped some of the functionality
 */

public class CheckForUpdateRunnable implements Runnable {
    private final Object mLock = new Object();
    private ICheckForUpdateListener mUpdateListener;
    private Radio mRadio = null;

    private NodeSysIsuState mStateNode = null;
    private volatile int mCalledCallbacks = 0;

    public CheckForUpdateRunnable(Radio radio, ICheckForUpdateListener updateListener) {
        mRadio = radio;

        setNewListener(updateListener);
    }

    @Override
    public void run() {

        boolean startedCheckForUpdate = checkUpdateState(mRadio, true);
        if (startedCheckForUpdate) {
            waitToCheckUpdateAndReturnState(mRadio);

            checkUpdateState(mRadio, false);

            if (mCalledCallbacks < 1) {
                onUpdateNotAvailable();
            }
        }
    }

    public void resetListener() {
        synchronized (mLock) {
            mUpdateListener = null;
        }
    }

    private boolean checkUpdateState(final Radio radio, final boolean startCheckIfNeeded) {
        FsLogger.log(">>> CheckForUpdateRunnable: ISU state check for " + radio);

        boolean startedCheckForUpdate = false;

        NodeSysIsuState stateNode = (NodeSysIsuState) radio.getNodeSyncGetter(NodeSysIsuState.class).get();

        if (stateNode != null) {

            FsLogger.log(">>> CheckForUpdateRunnable: ISU state for " + radio + " " + stateNode.getValueEnum());

            switch (stateNode.getValueEnum()) {
                case UPDATE_AVAILABLE:
                    onAvailableUpdate();
                    break;

                case IDLE:
                case UPDATE_NOT_AVAILABLE:
                    if (startCheckIfNeeded) {
                        startedCheckForUpdate = startScanningForUpdate(radio);
                    } else {
                        onUpdateNotAvailable();
                    }
                    break;

                case CHECK_IN_PROGRESS:
                case CHECK_FAILED:
                    if (startCheckIfNeeded) {
                        startedCheckForUpdate = startScanningForUpdate(radio);
                    } else {
                        onCheckFailed();
                    }
                    break;

                default:
                    onCheckFailed();
                    break;
            }
        } else {
            onCheckFailed();
        }

        return startedCheckForUpdate;
    }

    private boolean startScanningForUpdate(final Radio radio) {
        return radio.getNodeSyncSetter(new NodeSysIsuControl(NodeSysIsuControl.Ord.CHECK_FOR_UPDATE)).set();
    }

    private NodeSysIsuState waitToCheckUpdateAndReturnState(Radio radio) {
        mStateNode = null;
        int count = 0;

        do {
            getUpdateState(radio);

            try {
                Thread.sleep(FsComponentsConfig.MILLISECONDS_TO_WAIT_BETWEEN_CHECKING_ISU);
            } catch (InterruptedException e) {
                if (count >= FsComponentsConfig.MAX_RETRIES_FOR_CHECKING_ISU)
                    break;
            }

            FsLogger.log(">>> CheckForUpdateRunnable: wait checking " + radio + " " + count, LogLevel.Warning);

            count++;
        } while (isUpdateInProgress(mStateNode) && count < FsComponentsConfig.MAX_RETRIES_FOR_CHECKING_ISU);

        return mStateNode;
    }

    private boolean isUpdateInProgress(NodeSysIsuState stateNode) {
        boolean updateInProgress = true;

        if (stateNode != null) {
            updateInProgress = (mStateNode.getValueEnum() == NodeSysIsuState.Ord.CHECK_IN_PROGRESS);
        }

        return updateInProgress;
    }

    private void getUpdateState(final Radio radio) {
        mStateNode = (NodeSysIsuState) radio.getNodeSyncGetter(NodeSysIsuState.class).get();
    }

    private void onAvailableUpdate() {
        synchronized (mLock) {
            if (mUpdateListener != null) {
                mUpdateListener.onAvailableUpdate();
                mCalledCallbacks++;
                disposeIfPossible();
            }
        }
    }

    private void onUpdateNotAvailable() {

        synchronized (mLock) {
            if (mUpdateListener != null) {
                mUpdateListener.onNoAvailableUpdate();
                mCalledCallbacks++;
                disposeIfPossible();
            }
        }
    }

    private void onCheckFailed() {
        synchronized (mLock) {
            if (mUpdateListener != null) {
                mUpdateListener.onCheckFailed();
                mCalledCallbacks++;
                disposeIfPossible();
            }
        }
    }

    private void setNewListener(ICheckForUpdateListener updateListener) {
        synchronized (mLock) {
            mUpdateListener = updateListener;
        }
    }

    private void disposeIfPossible() {
        if (mCalledCallbacks < 1)
            return;

        FsLogger.log(">>> CheckForUpdateRunnable: disposeIfPossible " + mRadio, LogLevel.Error);

        synchronized (mLock) {
            mRadio = null;
        }

        resetListener();
    }
}

package com.apps_lib.multiroom.setup.normalSetup;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.PowerManager;
import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.databinding.ActivitySetupFinalizingBinding;
import com.apps_lib.multiroom.setup.update.EUpdateState;
import com.apps_lib.multiroom.setup.update.UpdateActivity;
import com.apps_lib.multiroom.setup.update.UpdateManager;
import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.frontier_silicon.NetRemoteLib.AccessPointUtil;
import com.frontier_silicon.components.common.FsComponentsConfig;

public class SetupFinalizingActivity extends UEActivityBase {

    private ActivitySetupFinalizingBinding mBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootview = LayoutInflater.from(this).inflate(R.layout.activity_setup_finalizing, null);
        setContentView(rootview);
        mBinding = DataBindingUtil.bind(rootview);

        setupAppBar();
        setTitle(R.string.setup_android_finalizing_setup_title);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setupControls();
    }

    @Override
    protected void onResume() {
        super.onResume();

        setupNetwork();

        PowerManager.getINSTANCE().keepScreenAlive();
    }

    @Override
    protected void onPause() {
        super.onPause();

        PowerManager.getINSTANCE().removeLock();
    }

    @Override
    public void onBackPressed() {
        //Don't let to go back
    }

    private void setupControls() {
        int imageId = SpeakerManager.getInstance().getImageIdForSSID(SetupManager.getInstance().getSSIDOfConfiguredRadio(),
                ESpeakerImageSizeType.Large);
        mBinding.speakerImageView.setImageResource(imageId);
    }

    synchronized protected void setupNetwork() {
        if (!SetupManager.getInstance().setupDeviceThreadIsRunning())
            resetProgressBar();

        mBinding.settingUpAudioSystemTextView.setText(getString(R.string.setup_android_setting_up_audio_system,
                SetupManager.getInstance().getFriendlyName()));

        SetupManager.getInstance().setupDevice(new IStateOfCheckingHeadlessSetupListener() {

            @Override
            public void onStateOfCheckingHeadlessSetupChanged(final EStateOfCheckingHeadlessSetup state) {
                Activity view = SetupFinalizingActivity.this;
                view.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onStateChanged(state);
                    }
                });
            }

            @Override
            public void onProgress(final int progress) {
                Activity view = SetupFinalizingActivity.this;
                view.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onProgressChanged(progress);
                    }
                });
            }
        });
    }

    private void onStateChanged(EStateOfCheckingHeadlessSetup configState) {
        switch (configState) {
            case None:
                showProgressDescription("");
                resetProgressBar();
                break;
            case ConfiguringAudioDevice:
                showProgressDescription(getString(R.string.setup_android_configuring));
                break;
            case ConfiguringWithWPS:
                mBinding.setupProgressBar.setMax(FsComponentsConfig.MAX_SECOND_TO_CONFIG_WITH_WPS +
                        FsComponentsConfig.MAX_SECONDS_TO_CONNECT_WITH_WPS);
                break;
            case NodesWereNotSetCorrectly:
                openSetupFailed();
                break;
            case ConnectingToSelectedWiFi:
                showProgressDescription(getString(R.string.setup_android_connecting_to_wifi));
                break;
            case WaitOnConnectingWithWPS:
                showProgressDescription("");

                AccessPointUtil.startAccesPointsScan();
                break;
            case CouldntConnectToWiFi:
                openSetupFailed();
                break;
            case SearchingTheAudioDevice:
                showProgressDescription(getString(R.string.setup_android_searching_configured_device));
                AccessPointUtil.startAccesPointsScan();
                break;
            case CouldntFindDeviceWithSSDP:
                openSetupFailed();
                break;
            case SuccesfullSetup:
                openSetupSuccessful();
                AccessPointUtil.startAccesPointsScan();
                break;
            default:
                break;
        }
    }

    private void showProgressDescription(String text) {
        mBinding.textSetupStatus.setText(text);
    }

    private void onProgressChanged(int progress) {
        mBinding.setupProgressBar.setProgress(progress);
    }

    private void resetProgressBar() {
        onProgressChanged(0);
        mBinding.setupProgressBar.setMax(getMaxTimeOfNormalSetup());
    }

    private int getMaxTimeOfNormalSetup() {
        return FsComponentsConfig.MAX_SECONDS_TO_CONFIGURE_THE_DEVICE +
                2*FsComponentsConfig.MAX_SECONDS_TO_WAIT_FOR_CONNECTING_TO_AP +
                FsComponentsConfig.MAX_SECONDS_TO_LOOK_FOR_CONFIGURED_RADIO;
    }

    private void openSetupSuccessful() {
        UpdateManager.getInstance().updateState.set(EUpdateState.None);
        NavigationHelper.goToActivity(SetupFinalizingActivity.this, UpdateActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }

    private void openSetupFailed() {
        NavigationHelper.goToActivity(SetupFinalizingActivity.this, SetupFailedActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }
}

package com.apps_lib.multiroom.volume;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Resources;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.ObservableField;
import android.databinding.ObservableFloat;
import android.databinding.ObservableInt;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.SeekBar;

import com.apps_lib.multiroom.databinding.ListItemVolumeSpeakerBinding;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysMode;
import com.frontier_silicon.NetRemoteLib.Radio.INodeNotificationCallback;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.apps_lib.multiroom.R;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;
import com.apps_lib.multiroom.myHome.speakers.SpeakerModel;
import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.ISpeakerImageIdListener;

public class VolumeSpeakerViewModel extends BaseObservable implements SeekBar.OnSeekBarChangeListener, INodeNotificationCallback {

    private static int MAX_VIRTUAL_STEP_VALUE = 100;
    private static int CHECK_TIME_LIMIT = 1000;

    private Activity mActivity;
    private SpeakerModel mSpeakerModel;
    private VolumeModel mVolumeModel;
    private Observable.OnPropertyChangedCallback mPropertyChangedCallback;
    private boolean isSeeking = false;
    private ListItemVolumeSpeakerBinding mBinding;
    public ObservableFloat seekbarsOpacity = new ObservableFloat(VolumesManager.UNMUTED_OPACITY);

    public ObservableField<BitmapDrawable> speakerImage = new ObservableField<>();

    // Volume Values
    private int volumeMaxValue = 0;
    public ObservableInt volumeMaxLayoutValue = new ObservableInt(MAX_VIRTUAL_STEP_VALUE);
    public ObservableInt volumeSeekLayoutValue = new ObservableInt(0);

    private long mLastVolumeChangeMoment;

    VolumeSpeakerViewModel(Activity activity, SpeakerModel speakerModel, ListItemVolumeSpeakerBinding binding) {
        mActivity = activity;
        mSpeakerModel = speakerModel;
        mBinding = binding;

        init();
    }

    private void init() {
        if (mSpeakerModel.deviceModel == null || mSpeakerModel.deviceModel.mRadio == null) {
            FsLogger.log("Device model or Radio from VolumeSpeakerViewModel is null", LogLevel.Error);
            return;
        }

        String udn = mSpeakerModel.deviceModel.mRadio.getUDN();
        mVolumeModel = VolumesManager.getInstance().getVolumeModel(udn);

        // Disable volume settings if the device is casting
        mSpeakerModel.deviceModel.mRadio.getMode(new Radio.IRadioModeCallback() {
            @Override
            public void getCurrentMode(NodeSysCapsValidModes.Mode currentMode) {
                setVolumeProgressEnabled(currentMode);
            }
        });

        addListeners();
        updateAll();
    }

    private void addListeners() {
        mPropertyChangedCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (observable == mVolumeModel.steps) {
                    updateVolumeSteps();
                } else if (observable == mVolumeModel.value) {
                    updateVolumeValue();
                } else if (observable == mVolumeModel.muted) {
                    updateMuted();
                }
            }
        };

        mVolumeModel.steps.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mVolumeModel.muted.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mVolumeModel.value.addOnPropertyChangedCallback(mPropertyChangedCallback);

        mSpeakerModel.deviceModel.mRadio.addNotificationListener(this);
    }

    private void updateAll() {
        updateSpeakerImage();
        requestVolumeStepsIfNeeded();
        updateVolumeSteps();
        updateVolumeValue();
        updateMuted();
    }

    private void updateMuted() {
        seekbarsOpacity.set(mVolumeModel.muted.get() ? VolumesManager.MUTED_OPACITY : VolumesManager.UNMUTED_OPACITY);
    }

    private void updateVolumeSteps() {
        volumeMaxValue = mVolumeModel.steps.get() - 1;
        volumeMaxLayoutValue.set(MAX_VIRTUAL_STEP_VALUE - MAX_VIRTUAL_STEP_VALUE % volumeMaxValue);
    }

    private void requestVolumeStepsIfNeeded() {
        if (!mVolumeModel.stepsRetrieved) {
            VolumesManager.getInstance().retrieveVolumeSteps(mSpeakerModel.deviceModel, mVolumeModel);
        }
    }

    private void updateVolumeValue() {
        if (isSeeking || !shouldUpdateLayout()) {
            return;
        }

        final int newValue = realVolumeStepToVirtual(mVolumeModel.value.get());
        if (!isTheSameVirtualVolumeValue(volumeSeekLayoutValue.get(), newValue)) {
            final ObjectAnimator animation = android.animation.ObjectAnimator.ofInt(mBinding.volumeSeekBar, "progress", newValue);
            animation.setDuration(250);
            animation.setInterpolator(new AccelerateDecelerateInterpolator());
            animation.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    volumeSeekLayoutValue.set(newValue);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    animation.start();
                }
            });
        }

    }

    private void updateSpeakerImage() {
        Radio radio = getRadio();

        SpeakerManager.getInstance().retrieveImageIdForRadio(radio, ESpeakerImageSizeType.Volume, new ISpeakerImageIdListener() {
            @Override
            public void onImageIdRetrieved(int imageId) {
                if (mActivity == null) {
                    return;
                }
                Resources resources = mActivity.getResources();

                Bitmap speakerImageBitmap = BitmapFactory.decodeResource(resources, imageId);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(resources, speakerImageBitmap);

                speakerImage.set(bitmapDrawable);
            }
        });
    }

    @Bindable
    public String getRadioName() {

        Radio radio = getRadio();

        String radioName = "";
        if (radio != null) {
            radioName = radio.getFriendlyName();
        }
        return radioName;
    }

    public String getSoloMulti() {
        String soloMulti = mActivity.getResources().getString(mSpeakerModel.isInMultiMode ? R.string.multi_caps : R.string.solo_caps);
        soloMulti = soloMulti.substring(0, 1).toUpperCase() + soloMulti.substring(1);
        return soloMulti;
    }

    private Radio getRadio() {
        if (mSpeakerModel.deviceModel != null && mSpeakerModel.deviceModel.mRadio != null) {
            return mSpeakerModel.deviceModel.mRadio;
        }
        return null;
    }

    public void dispose() {
        mVolumeModel.steps.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mVolumeModel.muted.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mVolumeModel.value.removeOnPropertyChangedCallback(mPropertyChangedCallback);

        mSpeakerModel.deviceModel.mRadio.removeNotificationListener(this);

        mPropertyChangedCallback = null;
        mVolumeModel = null;
        mActivity = null;
        mSpeakerModel = null;
        speakerImage.set(null);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if (!isSeeking) {
            return;
        }

        int id = seekBar.getId();

        if (id == mBinding.volumeSeekBar.getId()) {
            setVolume(i);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isSeeking = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isSeeking = false;
    }

    private void setVolume(int progress) {
        if (!isTheSameVirtualVolumeValue(volumeSeekLayoutValue.get(), progress) && isSeeking) {
            int realVolumeValue = virtualVolumeStepToReal(progress);
            VolumesManager.getInstance().setVolumeValueToRadio(mSpeakerModel.deviceModel.mRadio, realVolumeValue);
            setMomentOfVolumeSet();
        }
    }

    @Override
    public void onNodeUpdated(NodeInfo node) {
        if (node instanceof NodeSysMode) {
            NodeSysCapsValidModes.Mode mode = mSpeakerModel.deviceModel.mRadio.getModeSync();
            setVolumeProgressEnabled(mode);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setVolumeProgressEnabled(NodeSysCapsValidModes.Mode currentMode) {
        if (currentMode == NodeSysCapsValidModes.Mode.Cast) {
            seekbarsOpacity.set(VolumesManager.MUTED_OPACITY);
            mBinding.volumeSeekBar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        } else {
            seekbarsOpacity.set(VolumesManager.UNMUTED_OPACITY);
            mBinding.volumeSeekBar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });
        }
    }

    private int realVolumeStepToVirtual(int realVolumeStep) {
        return volumeMaxValue != 0 ? (MAX_VIRTUAL_STEP_VALUE / volumeMaxValue) * realVolumeStep : 0;
    }

    private int virtualVolumeStepToReal(int virtualVolumeStep) {
        if (volumeMaxValue == 0) {
            return 0;
        }

        int virtualStepPerValue = MAX_VIRTUAL_STEP_VALUE / volumeMaxValue;

        return virtualVolumeStep / virtualStepPerValue;
    }

    private boolean isTheSameVirtualVolumeValue(int actualValue, int newValue) {
        return volumeMaxValue != 0 && virtualVolumeStepToReal(actualValue) == virtualVolumeStepToReal(newValue);
    }

    private void setMomentOfVolumeSet(){
        mLastVolumeChangeMoment = System.currentTimeMillis();
    }

    private boolean shouldUpdateLayout() {
        return (System.currentTimeMillis() - mLastVolumeChangeMoment) > CHECK_TIME_LIMIT;
    }
}
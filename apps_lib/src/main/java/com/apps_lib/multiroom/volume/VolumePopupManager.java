package com.apps_lib.multiroom.volume;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.apps_lib.multiroom.MainApplication;
import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.databinding.SideKeyVolumeDialogBinding;
import com.apps_lib.multiroom.util.DisposableTimerTask;
import com.apps_lib.multiroom.widgets.NoJumpSeekBar;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.TaskHelper;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;

import java.util.Timer;

/**
 * Created by cvladu on 09/11/16.
 */
public class VolumePopupManager {
    private Activity mActivity;
    private Dialog mDialog;
    private Timer mTimer;
    private DisposableTimerTask mTimerTask;
    private SideKeyVolumeDialogBinding mBinding;
    private VolumePopupViewModel volumeSpeakerViewModel;
    private MultiroomDeviceModel multiroomDeviceModel;
    private boolean increaseVolume;

    public void attachActivity(Activity activity) {
        mActivity = activity;
    }

    public boolean dispatchKeyEvent(KeyEvent event, boolean showPopup) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();

        multiroomDeviceModel = MultiroomGroupManager.getInstance().getCurrentSelectedDevice();
        Radio radio;

        if (multiroomDeviceModel == null) {
            return false;
        }
        radio = multiroomDeviceModel.mRadio;

        if (ignoreVolumeButtons(keyCode)) {
            return true;
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN && radio != null && NetRemoteManager.getInstance().checkConnection()) {
                    if (mDialog == null) {
                        startActualVolumeTask(showPopup);
                        increaseVolume = true;
                    } else if (!mDialog.isShowing()) {
                        startActualVolumeTask(showPopup);
                        increaseVolume = true;
                        stopTimerTask();
                        startTimer();
                    } else {
                        showDialogIfNeeded(showPopup);
                        mBinding.volumeSeekBar.increase(true);
                        stopTimerTask();
                        startTimer();
                    }

                    return true;

                } else return false;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN && radio != null && NetRemoteManager.getInstance().checkConnection()) {
                    if (mDialog == null) {
                        startActualVolumeTask(showPopup);
                        increaseVolume = false;
                    } else if (!mDialog.isShowing()) {
                        startActualVolumeTask(showPopup);
                        increaseVolume = false;
                        stopTimerTask();
                        startTimer();
                    } else {
                        showDialogIfNeeded(showPopup);
                        mBinding.volumeSeekBar.increase(false);
                        stopTimerTask();
                        startTimer();
                    }

                    return true;

                } else return false;

            default:
                return false;
        }
    }

    private boolean ignoreVolumeButtons(int keyCode) {
        MainApplication application = (MainApplication) mActivity.getApplication();
        if (application != null && multiroomDeviceModel.mRadio != null && application.getCurrentApplication() == MainApplication.ECurrentApplication.MARSHALL_APP
                && (keyCode == KeyEvent.KEYCODE_VOLUME_UP || keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
            NodeSysCapsValidModes.Mode currentMode = multiroomDeviceModel.mRadio.getModeSync();
            if (currentMode == NodeSysCapsValidModes.Mode.Cast) {
                return true;
            }
        }
        return false;
    }

    private void startActualVolumeTask(boolean showPopup) {
        ActualVolumeTask actualVolumeTask = new ActualVolumeTask(showPopup);
        TaskHelper.execute(actualVolumeTask);
    }

    private void dismissVolumeDialog() {
        startTimer();
    }

    private void startTimer() {
        mTimer = new Timer();
        mTimerTask = new HideVolumeDialogTimerTask();
        mTimer.schedule(mTimerTask, 2000, 2000);

    }

    private class HideVolumeDialogTimerTask extends DisposableTimerTask {

        @Override
        public void run() {
            if (mDialog != null && mDialog.isShowing()) {
                mDialog.dismiss();
                mDialog.setOnKeyListener(null);
                stopTimerTask();
            }
        }
    }

    private void stopTimerTask() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }

        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask.dispose();
            mTimerTask = null;
        }
    }


    public void disposeDialog() {
        stopTimerTask();
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    private void showDialogIfNeeded(boolean showPopup) {
        if (showPopup) {
            mDialog.show();
        }
    }

    private class ActualVolumeTask extends AsyncTask<Void, Void, Integer> {

        private boolean mShowPopup;

        ActualVolumeTask(boolean showPopup) {
            mShowPopup = showPopup;
        }

        @Override
        protected void onPreExecute() {
            if (mDialog == null) {
                initVolumeDialog(multiroomDeviceModel.mRadio);
                volumeSpeakerViewModel.isVisible.set(false);
                setKeyListener();
            } else {
                setKeyListener();
                showDialogIfNeeded(mShowPopup);
                volumeSpeakerViewModel.isVisible.set(false);
            }
        }

        @Override
        protected Integer doInBackground(Void... params) {
            return MultiroomGroupManager.getInstance().getMainVolumeInSteps();
        }

        @Override
        protected void onPostExecute(Integer volume) {
            mBinding.volumeSeekBar.setProgress(volume);
            mBinding.volumeSeekBar.increase(increaseVolume);
            volumeSpeakerViewModel.isVisible.set(true);
        }

        private void initVolumeDialog(Radio radio) {
            mDialog = new Dialog(mActivity, R.style.VolumeDialog);
            Window window = mDialog.getWindow();
            if (window != null) {
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                window.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL);
                window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
                window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE  | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED);
            }


            if (radio != null && NetRemoteManager.getInstance().checkConnection()) {
                volumeSpeakerViewModel = new VolumePopupViewModel(mActivity, multiroomDeviceModel);

                mBinding = DataBindingUtil.inflate(LayoutInflater.from(mActivity), R.layout.side_key_volume_dialog, null, false);
                mDialog.setContentView(mBinding.getRoot());
                mBinding.setViewModel(volumeSpeakerViewModel);
                mBinding.volumeSeekBar.setOnSeekBarChangeListener(volumeSpeakerViewModel);
                mBinding.volumeSeekBar.setOnValueChangeListener(new NoJumpSeekBar.IOnValueChangeListener() {
                    @Override
                    public void onValueChanged(final int newValue) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    mBinding.volumeProgress.setProgress(newValue, true);
                                } else {
                                mBinding.volumeProgress.setProgress(newValue);
                                }
                            }
                        });
                    }
                });

                mBinding.volumeSeekBar.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        stopTimerTask();
                        startTimer();
                        return false;
                    }
                });
            }

            showDialogIfNeeded(mShowPopup);
            dismissVolumeDialog();
        }
    }

    private void setKeyListener() {
        mDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dlg, int keyCode, KeyEvent event) {
                mActivity.dispatchKeyEvent(event);
                return true;
            }
        });
    }
}

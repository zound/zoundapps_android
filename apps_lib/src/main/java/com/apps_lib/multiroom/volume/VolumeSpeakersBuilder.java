package com.apps_lib.multiroom.volume;

import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupState;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;
import com.apps_lib.multiroom.myHome.speakers.SpeakerModel;
import com.apps_lib.multiroom.myHome.speakers.SpeakerType;
import com.apps_lib.multiroom.myHome.speakers.SpeakersBuilder;

/**
 * Created by lsuhov on 22/06/16.
 */
public class VolumeSpeakersBuilder extends SpeakersBuilder {

    public VolumeSpeakersBuilder() {
        super(false);
    }

    @Override
    public void addOtherMultiModelsAfterHeader() {
        MultiroomDeviceModel serverDeviceModel = null;

        for (SpeakerModel speakerModel : mMultiHomeSpeakers) {
            MultiroomDeviceModel deviceModel = speakerModel.deviceModel;
            if (deviceModel == null) {
                continue;
            }

            if (deviceModel.mGroupRole == NodeMultiroomGroupState.Ord.SERVER) {
                serverDeviceModel = deviceModel;
            }
        }

        SpeakerModel masterVolumeModel = new SpeakerModel();
        masterVolumeModel.type = SpeakerType.MasterVolume;
        masterVolumeModel.deviceModel = serverDeviceModel;

        mFinalListOfHomeSpeakerModels.add(masterVolumeModel);
    }
}

package com.zoundindustries.multiroom.about;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivityEndUserLicenceBinding;

/**
 * Created by nbalazs on 10/02/2017.
 */
public class EndUserLicenceActivity extends UEActivityBase {

    private ActivityEndUserLicenceBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = LayoutInflater.from(this).inflate(R.layout.activity_end_user_licence, null);
        mBinding = DataBindingUtil.bind(rootView);
        setContentView(rootView);

        setupAppBar();
        enableUpNavigation();
        setTitle(getResources().getString(R.string.end_user_license_agreement_caps));

        init();
    }

    private void init() {
        mBinding.buttonGoToWebSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.openWebViewActivityForURL(EndUserLicenceActivity.this, NavigationHelper.AnimationType.SlideToLeft, getResources().getString(R.string.link_connected_eula));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

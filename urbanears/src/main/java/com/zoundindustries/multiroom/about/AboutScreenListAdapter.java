package com.zoundindustries.multiroom.about;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zoundindustries.multiroom.R;

import java.util.List;

/**
 * Created by nbalazs on 09/09/2016.
 */

public class AboutScreenListAdapter extends ArrayAdapter<String>{

    public AboutScreenListAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        if (convertView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_about_screen, null);
        } else {
            rowView = convertView;
        }
        TextView item = (TextView)rowView.findViewById(R.id.titleTextView);
        item.setText(getItem(position));
        return rowView;
    }
}

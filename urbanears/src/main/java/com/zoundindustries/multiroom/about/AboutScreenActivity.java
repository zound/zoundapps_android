package com.zoundindustries.multiroom.about;

import android.content.pm.PackageInfo;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivityAboutScreenBinding;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by nbalazs on 08/09/2016.
 */
public class AboutScreenActivity extends UEActivityBase {

    private ActivityAboutScreenBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = LayoutInflater.from(this).inflate(R.layout.activity_about_screen, null);
        mBinding = DataBindingUtil.bind(rootView);
        setContentView(rootView);

        setupAppBar();
        enableUpNavigation();
        setTitle(getResources().getString(R.string.hamburger_about));

        initAboutListView();

        setVersion();
    }

    private void initAboutListView() {
        Resources res = getResources();
        String[] aboutMenuItems = new String[]{
                res.getString(R.string.about_agreement),
                res.getString(R.string.about_open_source_software),
                res.getString(R.string.about_privacy_policy)};

        AboutScreenListAdapter aboutScreenListAdapter =
                new AboutScreenListAdapter(this,
                        R.layout.list_item_about_screen,
                        new ArrayList<>(Arrays.asList(aboutMenuItems)));

        View listHeaderView = LayoutInflater.from(this).inflate(R.layout.list_item_header, null);
        View listFooterView = LayoutInflater.from(this).inflate(R.layout.list_item_footer, null);
        mBinding.aboutListView.addHeaderView(listHeaderView, null, false);
        mBinding.aboutListView.addFooterView(listFooterView, null, false);
        mBinding.aboutListView.setAdapter(aboutScreenListAdapter);
        mBinding.aboutListView.setOnItemClickListener((adapterView, view, position, l) -> {
            switch (position) {
                case 1:
                    NavigationHelper.goToActivity(AboutScreenActivity.this,
                            EndUserLicenceActivity.class, NavigationHelper.AnimationType.SlideToLeft);
                    break;
                case 2:
                    NavigationHelper.goToActivity(AboutScreenActivity.this,
                            FreeAndOpenSourceSoftwareActivity.class, NavigationHelper.AnimationType.SlideToLeft);
                    break;
                case 3:
                    NavigationHelper.openWebViewActivityForURL(
                            AboutScreenActivity.this,
                            NavigationHelper.AnimationType.SlideToLeft,
                            getResources().getString(R.string.link_connected_privacy_policy_urbanears));
                default:
                    break;
            }
        });
    }

    private void setVersion() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            mBinding.textViewVersion.setText(mBinding.textViewVersion.getText() + " " + version);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

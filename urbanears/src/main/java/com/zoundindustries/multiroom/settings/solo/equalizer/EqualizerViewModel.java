package com.zoundindustries.multiroom.settings.solo.equalizer;

import android.app.Activity;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.view.View;
import android.widget.SeekBar;

import com.apps_lib.multiroom.settings.solo.equalizer.EqualizerManager;
import com.apps_lib.multiroom.settings.solo.equalizer.EqualizerNodesUtil;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.zoundindustries.multiroom.R;


/**
 * Created by lsuhov on 19/07/16.
 */

public class EqualizerViewModel implements SeekBar.OnSeekBarChangeListener {

    public ObservableField<String> maxStepString = new ObservableField<>();
    public ObservableField<String> middleStepString = new ObservableField<>();
    public ObservableField<String> minStepString = new ObservableField<>();
    public ObservableInt maxStep = new ObservableInt(100);
    public ObservableInt bassProgress = new ObservableInt();
    public ObservableInt trebleProgress = new ObservableInt();

    private Activity mActivity;
    private Radio mRadio;
    private boolean isSeeking = false;
    private EqualizerManager equalizerManager;

    EqualizerViewModel(Activity activity, Radio radio) {
        mActivity = activity;
        mRadio = radio;
    }

    public void onResume() {
        equalizerManager = EqualizerManager.getInstance();
        setSeekbarValues();

    }

    public void setSeekbarValues() {
        minStepString.set(mActivity.getString(R.string.db, Long.toString(equalizerManager.min)));
        middleStepString.set(mActivity.getString(R.string.db, Long.toString(equalizerManager.middle)));
        maxStepString.set(mActivity.getString(R.string.db, Long.toString(equalizerManager.max)));
        maxStep.set((int)(equalizerManager.max + Math.abs(equalizerManager.max)));

        bassProgress.set(equalizerManager.bassValueForSeekbar);
        trebleProgress.set(equalizerManager.trebleValueForSeekbar);
    }

    private long transformSeekBarValueToNodeValue(int value) {
        return value + equalizerManager.min;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if (!isSeeking) {
            return;
        }
        int idOfSeekBar = seekBar.getId();
        int progress = seekBar.getProgress();
        long valueForNodes = transformSeekBarValueToNodeValue(progress);

        if (idOfSeekBar == R.id.bassSeekBar) {
            if (progress == bassProgress.get()) {
                return;
            }
            bassProgress.set(progress);
            EqualizerNodesUtil.sendBassValue(mRadio, valueForNodes);
        } else if (idOfSeekBar == R.id.trebleSeekBar) {
            if (progress == trebleProgress.get()) {
                return;
            }
            trebleProgress.set(progress);
            EqualizerNodesUtil.sendTrebleValue(mRadio, valueForNodes);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isSeeking = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isSeeking = false;
    }

    @SuppressWarnings("unused")
    public void onResetClicked(View view) {
        bassProgress.set(Math.abs(equalizerManager.minStep));
        trebleProgress.set(Math.abs(equalizerManager.minStep));

        EqualizerNodesUtil.sendBassValue(mRadio, 0);
        EqualizerNodesUtil.sendTrebleValue(mRadio, 0);
    }
}

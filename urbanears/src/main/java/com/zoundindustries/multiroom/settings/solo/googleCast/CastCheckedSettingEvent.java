package com.zoundindustries.multiroom.settings.solo.googleCast;

/**
 * Created by lsuhov on 08/06/16.
 */
public class CastCheckedSettingEvent {
    GoogleCastSettingModel model;

    public CastCheckedSettingEvent(GoogleCastSettingModel model) {
        this.model = model;
    }
}

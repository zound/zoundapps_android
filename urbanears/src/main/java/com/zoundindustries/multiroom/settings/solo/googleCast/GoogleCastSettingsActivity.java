package com.zoundindustries.multiroom.settings.solo.googleCast;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.frontier_silicon.NetRemoteLib.Node.NodeCastUsageReport;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.settings.RadioFromBundleExtractor;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivitySettingsGoogleCastBinding;
import com.zoundindustries.multiroom.settings.solo.SettingsViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuhov on 08/06/16.
 */
public class GoogleCastSettingsActivity extends UEActivityBase {

    private ActivitySettingsGoogleCastBinding mBinding;
    private Radio mRadio;
    private GoogleCastSettingAdapter mAdapter;
    private SettingsViewModel mSettingsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings_google_cast);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.chromecast_built_in_settings_caps);

        mRadio = RadioFromBundleExtractor.extractRadio(getIntent().getExtras(), GoogleCastSettingsActivity.class);

        mSettingsViewModel = new SettingsViewModel(this, mRadio);

        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        setupControls();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSettingsViewModel.startListeningToDiscovery();
        EventBus.getDefault().register(this);

        initAdapter();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSettingsViewModel.stopListening();
        EventBus.getDefault().unregister(this);
    }

    private void setupControls() {
        List<GoogleCastSettingModel> mGoogleCastSettingModels = new ArrayList<>();
        mAdapter = new GoogleCastSettingAdapter(this, R.layout.list_item_settings_google_cast, mGoogleCastSettingModels);
        View listHeaderView = LayoutInflater.from(this).inflate(R.layout.list_item_header, null);
        View listFooterView = LayoutInflater.from(this).inflate(R.layout.list_item_footer, null);
        mBinding.settingsListView.addHeaderView(listHeaderView, null, false);
        mBinding.settingsListView.addFooterView(listFooterView, null, false);
        mBinding.settingsListView.setAdapter(mAdapter);
        mBinding.settingsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GoogleCastSettingModel castSettingModel = mAdapter.getItem(position-1);

                onCastSettingItemClicked(castSettingModel);
            }
        });
    }

    private void onCastSettingItemClicked(GoogleCastSettingModel castSettingModel) {
        if (castSettingModel.settingType != SettingType.Link) {
            return;
        }

        if (TextUtils.isEmpty(castSettingModel.url)) {
            return;
        }

        NavigationHelper.openWebViewActivityForURL(this, NavigationHelper.AnimationType.SlideToLeft, castSettingModel.url);
    }

    @Subscribe
    public void onSettingChecked(CastCheckedSettingEvent event) {
        GoogleCastSettingModel model = event.model;
        if (model.googleCastSettingType == GoogleCastSettingType.ShareUsageData) {
            NodeCastUsageReport.Ord ord = model.isChecked ? NodeCastUsageReport.Ord.ACTIVE :
                    NodeCastUsageReport.Ord.INACTIVE;

            if (mSettingsViewModel.startHomeActivityIfNeeded()) {
                return;
            }

            mRadio.setNode(new NodeCastUsageReport(ord), false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initAdapter() {
        List<GoogleCastSettingModel> googleCastSettingModels = GoogleCastManager.mGoogleCastSettingsModels;
        mAdapter.clear();
        mAdapter.addAll(googleCastSettingModels);
        mAdapter.notifyDataSetChanged();
    }
}

package com.zoundindustries.multiroom.settings.solo.aboutSpeaker;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.settings.RadioFromBundleExtractor;
import com.apps_lib.multiroom.setup.update.CheckForUpdateRunnable;
import com.apps_lib.multiroom.setup.update.ICheckForUpdateListener;
import com.apps_lib.multiroom.util.DisposableTimerTask;
import com.frontier_silicon.NetRemoteLib.Node.NodeCastVersion;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysInfoVersion;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanConnectedSSID;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanMacAddress;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanRssi;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivitySettingsAboutSpeakerBinding;
import com.zoundindustries.multiroom.settings.solo.SettingsViewModel;

import java.util.Map;
import java.util.Timer;

/**
 * Created by lsuhov on 08/06/16.
 */
public class AboutSpeakerActivity extends UEActivityBase {

    public static final int EXTRA_SHIFT_FOR_WIFI_TEXT = 4;

    private Radio mRadio;
    private ActivitySettingsAboutSpeakerBinding mBinding;
    private String mWifiNetworkName;
    private Timer mTimer;
    private DisposableTimerTask mTimerTask;
    private SettingsViewModel mSettingsViewModel;
    private boolean mActivityWasCreated;
    private CheckForUpdateRunnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings_about_speaker);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.about_this_speaker_caps);

        mRadio = RadioFromBundleExtractor.extractRadio(getIntent().getExtras(), AboutSpeakerActivity.class);

        mSettingsViewModel = new SettingsViewModel(this, mRadio);

        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        setupControls();
        mActivityWasCreated = true;

        checkForUpdate();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSettingsViewModel.startListeningToDiscovery();

        if (mActivityWasCreated) {
            //not
            mActivityWasCreated = false;
            return;
        }
        startStrengthTimer();
    }

    @Override
    protected void onPause() {
        mSettingsViewModel.stopListening();
        stopStrengthTimer();

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (runnable != null) {
            runnable.resetListener();
        }

        super.onDestroy();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupControls() {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }
        setSpeakerName();

        RadioNodeUtil.getNodesFromRadioAsync(mRadio, new Class[]{NodeSysInfoVersion.class, NodeCastVersion.class,
                        NodeSysNetWlanConnectedSSID.class, NodeSysNetWlanRssi.class, NodeSysNetWlanMacAddress.class},
                false, new RadioNodeUtil.INodesResultListener() {
                    @Override
                    public void onNodesResult(Map<Class, NodeInfo> nodes) {
                        NodeSysNetWlanConnectedSSID ssidNode = (NodeSysNetWlanConnectedSSID) nodes.get(NodeSysNetWlanConnectedSSID.class);
                        if (ssidNode != null) {
                            mWifiNetworkName = RadioNodeUtil.getHumanReadableSSID(ssidNode.getValue());
                        }
                        NodeSysNetWlanRssi signalStrengthNode = (NodeSysNetWlanRssi) nodes.get(NodeSysNetWlanRssi.class);
                        if (signalStrengthNode != null) {
                            showWifiWithStrength(signalStrengthNode.getValue());
                        }

                        startStrengthTimer();

                        setIPAddress();
                        setMACAddress((NodeSysNetWlanMacAddress) nodes.get(NodeSysNetWlanMacAddress.class));
                        setModelName();
                        setSoftwareVersion((NodeSysInfoVersion) nodes.get(NodeSysInfoVersion.class));
                        setGoogleCastVersion((NodeCastVersion) nodes.get(NodeCastVersion.class));
                    }
                });

        setUpdateButton();
    }

    void setSpeakerName() {
        mBinding.speakerNameTextView.setText(
                getSpan(getResources().getString(R.string.speaker_name_with_value,
                        mRadio.getFriendlyName()),
                        getShiftForSpans(mRadio.getFriendlyName())));
    }

    private void setGoogleCastVersion(NodeCastVersion nodeCastVersion) {
        if (nodeCastVersion != null) {
            mBinding.googleCastVersionTextView.setText(
                    getSpan(getString(R.string.chromecast_built_in_version_with_value,
                            nodeCastVersion.getValue()),
                            getShiftForSpans(nodeCastVersion.getValue())));

            mBinding.googleCastVersionTextView.setVisibility(View.VISIBLE);
        } else {
            mBinding.googleCastVersionTextView.setVisibility(View.GONE);
        }
    }

    private void setModelName() {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        mBinding.speakerModelTextView.setText(
                getSpan(getString(R.string.model_name_with_value,
                        getModelNameWithoutUnderscore(mRadio.getModelName())),
                        getModelNameWithoutUnderscore(mRadio.getModelName()).length()));
    }

    private String getModelNameWithoutUnderscore(String modelName) {
        String speakerModel = "";

        if (modelName.contains("_")) {
            String[] array = modelName.split("_");
            int length = array.length;
            if (length == 3 && array[1].equalsIgnoreCase("Gold") && array[2].equalsIgnoreCase("Fish")) {
                speakerModel += array[0] + " " + array[1] + array[2].toLowerCase();
            } else {
                for (int i = 0; i < length; i++) {
                    speakerModel += array[i] + " ";
                }
            }

        } else {
            speakerModel = modelName;
        }

        return speakerModel;
    }

    private void setMACAddress(NodeSysNetWlanMacAddress nodeSysNetWlanMacAddress) {
        if (nodeSysNetWlanMacAddress == null) {
            return;
        }

        mBinding.speakerMACTextView.setText(
                getSpan(getString(R.string.mac_address_with_value,
                        nodeSysNetWlanMacAddress.getValue()),
                        getShiftForSpans(nodeSysNetWlanMacAddress.getValue())));
    }

    private void setIPAddress() {
        mBinding.speakerIpTextView.setText(
                getSpan(getString(R.string.ip_address_with_value,
                        mRadio.getIpAddress()),
                        getShiftForSpans(mRadio.getIpAddress())));
    }

    private void setSoftwareVersion(NodeSysInfoVersion nodeSysInfoVersion) {
        if (nodeSysInfoVersion == null) {
            return;
        }

        mBinding.softwareVersionTextView.setText(
                getSpan(getString(R.string.software_version_with_value,
                        nodeSysInfoVersion.getValue()),
                        getShiftForSpans(nodeSysInfoVersion.getValue())));
    }

    private void setUpdateButton() {
        mBinding.buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpeakerUpdateNoteFragment speakerUpdateNoteFragment = SpeakerUpdateNoteFragment.newInstance(mSettingsViewModel);
                FragmentManager fragmentManager = getSupportFragmentManager();
                speakerUpdateNoteFragment.show(fragmentManager, "OldUpdateWarningDialogFragment");
            }
        });
    }

    private void startStrengthTimer() {
        stopStrengthTimer();

        mTimerTask = new WiFiStrengthTimerTask();
        mTimer = new Timer();
        mTimer.schedule(mTimerTask, 3000, 3000);
    }

    private void stopStrengthTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask.dispose();
            mTimerTask = null;
        }
    }

    private class WiFiStrengthTimerTask extends DisposableTimerTask {

        @Override
        public void run() {
            if (mIsDisposed) {
                cancel();
                return;
            }

            if (!AboutSpeakerActivity.this.isFinishing()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getWifiStrengthAndShowIt();
                    }
                });
            }
        }
    }

    private void getWifiStrengthAndShowIt() {
        if (!isFinishing()) {
            if (mSettingsViewModel.startHomeActivityIfNeeded()) {
                return;
            }
            RadioNodeUtil.getNodeFromRadioAsync(mRadio, NodeSysNetWlanRssi.class, new RadioNodeUtil.INodeResultListener() {
                @Override
                public void onNodeResult(NodeInfo node) {
                    NodeSysNetWlanRssi signalStrengthNode = (NodeSysNetWlanRssi) node;
                    if (signalStrengthNode != null) {
                        showWifiWithStrength(signalStrengthNode.getValue());
                    }
                }
            });
        }
    }

    private void showWifiWithStrength(long rawWiFiStrength) {
        mBinding.wifiNetworkNameTextView.setText(
                getSpan(getString(R.string.wifi_network_name_with_value,
                        mWifiNetworkName,
                        rawWiFiStrength),
                        getSpanShiftForWifiStrength(rawWiFiStrength)));
    }

    private int getSpanShiftForWifiStrength(long rawWiFiStrength) {
        return getShiftForSpans(mWifiNetworkName) +
                Long.toString(rawWiFiStrength).length() + EXTRA_SHIFT_FOR_WIFI_TEXT;
    }

    private int getShiftForSpans(String resourceArgument) {
        return resourceArgument == null ? 0 : resourceArgument.length();
    }

    private Spannable getSpan(String textWithValue, int shiftValue) {
        Spannable spannable = new SpannableString(textWithValue);

        spannable.setSpan(
                new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)),
                spannable.length() - shiftValue,
                spannable.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannable;
    }

    private void checkForUpdate() {
        runnable = new CheckForUpdateRunnable(mRadio, new ICheckForUpdateListener() {
            @Override
            public void onAvailableUpdate() {
                runOnUiThread(() -> {
                    if (mBinding != null) {
                        mBinding.buttonUpdate.setVisibility(View.VISIBLE);
                    }
                });
            }

            @Override
            public void onNoAvailableUpdate() {
            }

            @Override
            public void onCheckFailed() {
            }
        });
        new Thread(runnable).start();
    }
}

package com.zoundindustries.multiroom.settings.multi.streamingQuality;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;

import com.apps_lib.multiroom.UEActivityBase;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivityStreamingQualityBinding;

/**
 * Created by nbalazs on 06/12/2016.
 */
public class StreamingQualityActivity extends UEActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityStreamingQualityBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_streaming_quality);

        StreamingQualityViewModel mViewModel = new StreamingQualityViewModel(this, mBinding);

        mBinding.setViewModel(mViewModel);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.streaming_quality_caps);

        setupControls();
    }

    private void setupControls() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

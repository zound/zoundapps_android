package com.zoundindustries.multiroom.settings.solo.dateTime;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zoundindustries.multiroom.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by nbalazs on 13/12/2016.
 */

class TimeZoneListAdapter extends ArrayAdapter<String>{

    private int mSelectedItemIndex;
    private List<String> mTimeZoneList;
    private List<String> mSelectedTimeZoneList;

    TimeZoneListAdapter(Context context, String[] values, int selectedItemIndex) {
        super(context, -1, values);
        mSelectedItemIndex = selectedItemIndex;
        mTimeZoneList = Arrays.asList(values);
        mSelectedTimeZoneList = new ArrayList<>(mTimeZoneList);
    }

    @Override
    public int getCount() {
        return mSelectedTimeZoneList.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return mSelectedTimeZoneList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view;
        if (convertView != null) {
            view = convertView;
        } else {
            view = LayoutInflater.from(getContext()).inflate(R.layout.list_item_time_zone, null);
        }

        TextView textView = (TextView)view.findViewById(R.id.timeZoneName);
        String text = getItem(position);
        if (text != null) {
            text = text.replace('/', '-');
            text = text.replace('_', ' ');
        }
        textView.setText(text);
        if (position == mSelectedItemIndex) {
            view.findViewById(R.id.timeZoneIsSelected).setVisibility(View.VISIBLE);
        } else {
            view.findViewById(R.id.timeZoneIsSelected).setVisibility(View.INVISIBLE);
        }

        return view;
    }

    void filter(String criteria) {
        criteria = criteria.toLowerCase();
        mSelectedTimeZoneList.clear();
        if (criteria.length() == 0) {
            mSelectedTimeZoneList.addAll(mTimeZoneList);
        } else {
            for (String timeZone : mTimeZoneList) {
                if (timeZone.toLowerCase().contains(criteria)) {
                    mSelectedTimeZoneList.add(timeZone);
                }
            }
        }
        notifyDataSetChanged();
    }

    final String  getSelectedItem(int position) {
        return position < mSelectedTimeZoneList.size() ? mSelectedTimeZoneList.get(position) : null;
    }

    final int getSelectedItemsCount() {
        return mSelectedTimeZoneList.size();
    }
}

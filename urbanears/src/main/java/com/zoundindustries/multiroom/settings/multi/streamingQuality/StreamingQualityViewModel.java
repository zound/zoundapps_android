package com.zoundindustries.multiroom.settings.multi.streamingQuality;

import android.app.Activity;
import android.view.View;
import android.widget.Toast;

import com.apps_lib.multiroom.multiroom.MultiroomRebuildGroups;
import com.apps_lib.multiroom.settings.multi.streamingQuality.StreamingQualityMgr;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivityStreamingQualityBinding;

import java.util.List;

/**
 * Created by nbalazs on 06/12/2016.
 */

public class StreamingQualityViewModel implements StreamingQualityMgr.IStreamingQualityChangeListener{
    private Activity mActivity;
    private ActivityStreamingQualityBinding mBinding;

    StreamingQualityViewModel(Activity activity, ActivityStreamingQualityBinding binding) {
        mActivity = activity;
        mBinding = binding;

        initLayout();
    }

    public void onHighStreamingQualityButtonClicked(View view) {
        if (view.getId() == mBinding.textViewHighRadioBtn.getId() || view.getId() == mBinding.radioButtonHigh.getId()) {
            mBinding.radioButtonHigh.setChecked(true);
            mBinding.radioButtonLow.setChecked(false);
        }
        writeNodeMultiroomDeviceTransportOptimisation(false);   // set optimization disabled
    }

    public void onLowStreamingQualityButtonClicked(View view) {
        if (view.getId() == mBinding.textViewLowRadioBtn.getId() || view.getId() == mBinding.radioButtonLow.getId()) {
            mBinding.radioButtonHigh.setChecked(false);
            mBinding.radioButtonLow.setChecked(true);
        }
        writeNodeMultiroomDeviceTransportOptimisation(true);    // set optimization enabled
    }

    private void initLayout() {
        List<Radio> radios = NetRemoteManager.getInstance().getRadios();
        boolean checkHigh = !StreamingQualityMgr.getInstance().getGeneralOptimizationStatus(radios);
        mBinding.radioButtonHigh.setChecked(checkHigh);
        mBinding.radioButtonLow.setChecked(!checkHigh);
    }

    private void writeNodeMultiroomDeviceTransportOptimisation(boolean optimizationEnabled) {
        List<Radio> radios = NetRemoteManager.getInstance().getRadios();
        StreamingQualityMgr.getInstance().changeStreamingQuality(radios, this, optimizationEnabled);
    }

    @Override
    public void onStreamingQualityChanged(boolean success) {
        if (success) {
            MultiroomRebuildGroups.getInstance().rebuildMultiroomGroupsAndDevices();
        } else {
            Toast.makeText(mActivity, mActivity.getString(R.string.optimisation_error_msg), Toast.LENGTH_SHORT).show();
        }
    }
}

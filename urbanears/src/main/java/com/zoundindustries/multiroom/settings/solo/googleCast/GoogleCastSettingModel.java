package com.zoundindustries.multiroom.settings.solo.googleCast;

/**
 * Created by lsuhov on 08/06/16.
 */
public class GoogleCastSettingModel {
    public GoogleCastSettingType googleCastSettingType;
    public SettingType settingType;
    public String name;
    public String url;
    public boolean isChecked;

    public GoogleCastSettingModel(GoogleCastSettingType type, SettingType settingType, String name, String url) {
        this.googleCastSettingType = type;
        this.settingType = settingType;
        this.name = name;
        this.url = url;
    }

    public GoogleCastSettingModel(GoogleCastSettingType type, SettingType settingType, String name, boolean isChecked) {
        this.googleCastSettingType = type;
        this.settingType = settingType;
        this.name = name;
        this.isChecked = isChecked;
    }
}

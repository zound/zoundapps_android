package com.zoundindustries.multiroom.settings.solo;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.myHome.speakers.SpeakerNameManager;
import com.apps_lib.multiroom.settings.RadioFromBundleExtractor;
import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.ISpeakerImageIdListener;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.apps_lib.multiroom.util.MessageHandlerUtil;
import com.apps_lib.multiroom.widgets.EditTextWithBackEvent;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysInfoFriendlyName;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.ErrorSanitizerMessageType;
import com.frontier_silicon.components.common.FriendlyNameSanitizer;
import com.frontier_silicon.components.common.FsComponentsConfig;
import com.frontier_silicon.components.common.GuiUtils;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivitySettingsChangeNameBinding;

/**
 * Created by lsuhov on 07/06/16.
 */
public class ChangeFriendlyNameActivity extends UEActivityBase {

    private ActivitySettingsChangeNameBinding mBinding;
    private Radio mRadio;
    private FriendlyNameSanitizer mFriendlyNameSanitizer;
    private SettingsViewModel mSettingsViewModel;
    private boolean mIsStarted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings_change_name);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.change_name_caps);

        mRadio = RadioFromBundleExtractor.extractRadio(getIntent().getExtras(), ChangeFriendlyNameActivity.class);

        mSettingsViewModel = new SettingsViewModel(this, mRadio);

        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        setupControls();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mIsStarted = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSettingsViewModel.startListeningToDiscovery();
        initEditText();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mSettingsViewModel.stopListening();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mFriendlyNameSanitizer != null) {
            mFriendlyNameSanitizer.hideError();
        }

        mIsStarted = false;
    }

    @Override
    public void onDestroy() {

        if (mFriendlyNameSanitizer != null) {
            mFriendlyNameSanitizer.dispose();
            mFriendlyNameSanitizer = null;
        }

        super.onDestroy();
    }

    private void setupControls() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);
        if (displayMetrics.ydpi < 225.0f) {
            mBinding.editTextFriendlyName.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        } else {
            mBinding.editTextFriendlyName.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        }

        mFriendlyNameSanitizer = new FriendlyNameSanitizer(FsComponentsConfig.MAX_LENGTH_OF_FRIENDLY_NAME_WHEN_GOOGLE_CAST_IS_PRESENT);
        mFriendlyNameSanitizer.appendSanitizerToEditText(mBinding.editTextFriendlyName, this,
                new FriendlyNameSanitizer.IFriendlyNameSanitizerListener() {

                    @Override
                    public void onSubmitFriendlyName(String friendlyName) {
                        submitNewName();
                    }

                    @Override
                    public void onNewFriendlyName(String friendlyName) {
                    }

                    @Override
                    public void onBadFriendlyName(ErrorSanitizerMessageType errorMessageType) {
                        //updateNextButtonState();
                        MessageHandlerUtil.handleMessageErrorType(ChangeFriendlyNameActivity.this, errorMessageType, mBinding.editTextFriendlyName);
                    }
                });

        mBinding.buttonRename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitNewName();
            }
        });
        updateSpeakerImage();
    }

    private void updateSpeakerImage() {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        SpeakerManager.getInstance().retrieveImageIdForRadio(mRadio, ESpeakerImageSizeType.Large, new ISpeakerImageIdListener() {
            @Override
            public void onImageIdRetrieved(int imageId) {
                if (isFinishing()) {
                    return;
                }
                mBinding.speakerImageView.setImageResource(imageId);
            }
        });
    }

    private void initEditText() {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        String friendlyName = mRadio.getFriendlyName();
        mFriendlyNameSanitizer.mIsManualChange = true;
        mBinding.editTextFriendlyName.setText(friendlyName);
        mBinding.editTextFriendlyName.requestFocus();
        mBinding.editTextFriendlyName.selectAll();
        mBinding.buttonRename.setVisibility(View.GONE);
        mBinding.editTextFriendlyName.setOnEditTextImeBackListener(new EditTextWithBackEvent.EditTextImeBackListener() {
            @Override
            public void onImeBack(EditTextWithBackEvent ctrl, String text) {
                mBinding.focusableLayout.requestFocus();
            }
        });
        mBinding.editTextFriendlyName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (view.getId() == mBinding.editTextFriendlyName.getId()) {
                    if (hasFocus) {
                        mBinding.buttonRename.setVisibility(View.GONE);
                    } else {
                        mBinding.buttonRename.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        GuiUtils.showKeyboardForGivenEditText(mBinding.editTextFriendlyName, this);

    }

    private void submitNewName() {
        final String newName = mBinding.editTextFriendlyName.getText().toString();
        if (TextUtils.isEmpty(newName)) {
            return;
        }

        mBinding.progressActivating.setVisibility(View.VISIBLE);

        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        RadioNodeUtil.setNodeToRadioAsync(mRadio, new NodeSysInfoFriendlyName(newName),
                new RadioNodeUtil.INodeSetResultListener() {
                    @Override
                    public void onNodeSetResult(boolean success) {
                        if (mIsStarted) {
                            mBinding.progressActivating.setVisibility(View.GONE);
                            if (mSettingsViewModel.startHomeActivityIfNeeded()) {
                                return;
                            }
                            if (success) {
                                GuiUtils.showToast(getString(R.string.new_name_stored), ChangeFriendlyNameActivity.this);
                                mRadio.setFriendlyName(newName);
                                SpeakerNameManager.getInstance().updateNameForRadio(mRadio);
                                onBackPressed();
                            } else {
                                GuiUtils.showToast(getString(R.string.something_went_wrong), ChangeFriendlyNameActivity.this);
                            }
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

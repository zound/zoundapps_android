package com.zoundindustries.multiroom.settings.solo.googleCast;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.zoundindustries.multiroom.R;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by lsuhov on 08/06/16.
 */
public class GoogleCastSettingAdapter extends ArrayAdapter<GoogleCastSettingModel> {

    private int mLayoutId;

    public GoogleCastSettingAdapter(Context context, int resource, List<GoogleCastSettingModel> models) {
        super(context, resource, models);

        mLayoutId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        Context context = getContext();

        final GoogleCastSettingModel model = getItem(position);

        if (v == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(mLayoutId, null);
        }

        TextView textView = (TextView) v.findViewById(R.id.settingTextView);
        textView.setText(model.name);

        View nextImageView = v.findViewById(R.id.imageViewChevron);
        Switch settingSwitch = (Switch)v.findViewById(R.id.settingSwitch);
        final TextView switchOnOff = (TextView) v.findViewById(R.id.switchOnOff);

        boolean isCheckbox = model.settingType == SettingType.CheckBox;
        nextImageView.setVisibility(isCheckbox ? View.GONE : View.VISIBLE);
        settingSwitch.setVisibility(isCheckbox ? View.VISIBLE : View.GONE);
        switchOnOff.setVisibility(isCheckbox ? View.VISIBLE : View.GONE);

        if (isCheckbox) {
            settingSwitch.setChecked(model.isChecked);
            switchOnOff.setText(model.isChecked ? getContext().getString(R.string.on_caps) : getContext().getString(R.string.off_caps));
            settingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    model.isChecked = isChecked;
                    switchOnOff.setText(isChecked ? getContext().getString(R.string.on_caps) : getContext().getString(R.string.off_caps));
                    EventBus.getDefault().post(new CastCheckedSettingEvent(model));
                }
            });
        }

        return v;
    }
}

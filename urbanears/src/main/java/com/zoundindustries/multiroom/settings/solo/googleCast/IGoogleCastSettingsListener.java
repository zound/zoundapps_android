package com.zoundindustries.multiroom.settings.solo.googleCast;

import java.util.List;

/**
 * Created by lsuhov on 18/07/16.
 */
interface IGoogleCastSettingsListener {
    void onGoogleCastSettingsRetrieved(List<GoogleCastSettingModel> googleCastSettingModels);
}

package com.zoundindustries.multiroom;

import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.MainApplication;
import com.apps_lib.multiroom.factory.FragmentFactory;
import com.apps_lib.multiroom.persistence.PersistenceMgr;
import com.apps_lib.multiroom.presets.spotify.SpotifyManager;
import com.apps_lib.multiroom.setup.presets.DefaultIRPresetListCreatorTask;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.zoundindustries.multiroom.factory.UrbanearsActivityCreator;
import com.zoundindustries.multiroom.factory.UrbanearsFragmentCreator;
import com.zoundindustries.multiroom.speakerImages.UrbanearsSpeakerImageRetriever;
import com.zoundindustries.multiroom.speakerImages.UrbanearsSpeakerTypeDecoder;

/**
 * Created by nbalazs on 02/05/2017.
 */

public class UrbanearsApplication extends MainApplication{

    private final String PRESET_SERVER_URL = "https://bitbucket.org/zoundindustries/radiopresets/src/master/urbanears_radio_presets.json";

    private final String REDIRECT_URL = "urbanears://spotify-callback";
    private final String CLIENT_ID = "52080f4d24784796ba37ae5213229f25";
    private final String SERVER_ENDPOINT = "https://urbanears-dev-spotify.herokuapp.com";

    @Override
    public ECurrentApplication getCurrentApplication() {
        return ECurrentApplication.URBEANEARS_APP;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        PersistenceMgr.getInstance().init(this);
        SpeakerManager.init(new UrbanearsSpeakerTypeDecoder(), new UrbanearsSpeakerImageRetriever());
        ActivityFactory.init(new UrbanearsActivityCreator());
        FragmentFactory.init(new UrbanearsFragmentCreator());
        DefaultIRPresetListCreatorTask.init(PRESET_SERVER_URL, R.raw.radio_presets);
        SpotifyManager.getInstance().init(getApplicationContext(), REDIRECT_URL, CLIENT_ID, SERVER_ENDPOINT);
    }
}

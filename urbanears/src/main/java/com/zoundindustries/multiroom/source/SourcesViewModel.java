package com.zoundindustries.multiroom.source;

import android.app.Activity;
import android.databinding.Observable;
import android.graphics.Bitmap;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.apps_lib.multiroom.nowPlaying.AlbumCoverDownloader;
import com.apps_lib.multiroom.nowPlaying.IAlbumCoverListener;
import com.apps_lib.multiroom.nowPlaying.NowPlayingDataModel;
import com.apps_lib.multiroom.nowPlaying.NowPlayingManager;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.zoundindustries.multiroom.GuiUtilsUrbanears;
import com.zoundindustries.multiroom.databinding.ActivitySourcesBinding;

/**
 * Created by nbalazs on 09/03/2017.
 */

class SourcesViewModel implements IAlbumCoverListener {

    private Activity mActivity;
    private Observable.OnPropertyChangedCallback mNowPlayingPropertyChangedCallback;

    SourcesViewModel() {}

    public void init(Activity activity) {
        mActivity = activity;
        addListeners();
        updateAlbumCover();
    }

    public void dispose() {
        mActivity = null;
        removeListeners();
    }

    private void addListeners() {
        final NowPlayingDataModel nowPlayingDataModel = NowPlayingManager.getInstance().getNowPlayingDataModel();

        mNowPlayingPropertyChangedCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if (sender == nowPlayingDataModel.playInfoText ||
                        sender == nowPlayingDataModel.album ||
                        sender == nowPlayingDataModel.artist ||
                        sender == nowPlayingDataModel.playStatus) {
                    updateAlbumCover();
                }
            }
        };

        nowPlayingDataModel.playInfoText.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.album.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.artist.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.playStatus.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
    }

    private void removeListeners() {
        final NowPlayingDataModel nowPlayingDataModel = NowPlayingManager.getInstance().getNowPlayingDataModel();

        nowPlayingDataModel.playInfoText.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.album.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.artist.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.playStatus.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);

        mNowPlayingPropertyChangedCallback = null;
    }

    private void updateAlbumCover() {
        Radio radio = NetRemoteManager.getInstance().getCurrentRadio();
        if (radio != null && canGetAlbumCover()) {
            AlbumCoverDownloader coverDownloader = new AlbumCoverDownloader();
            coverDownloader.getAlbumArtwork(radio, this, mActivity.getApplicationContext());
        }
    }

    private boolean canGetAlbumCover() {
        NowPlayingDataModel nowPlayingDataModel = NowPlayingManager.getInstance().getNowPlayingDataModel();
        boolean result = !(nowPlayingDataModel.isError() ||
                nowPlayingDataModel.currentMode.get() == NodeSysCapsValidModes.Mode.AuxIn);

        result &= (nowPlayingDataModel.isPlaying() || nowPlayingDataModel.isPaused() ||
                nowPlayingDataModel.isBuffering() || nowPlayingDataModel.isStopped());

        return result;
    }

    @Override
    public void onAlbumCoverUpdate(Bitmap albumCover) {
        // Nothing to do here, just let it go
    }
}

package com.zoundindustries.multiroom.source;

import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;

import com.apps_lib.multiroom.presets.IPresetAnimationHandler;
import com.apps_lib.multiroom.source.IRotationHandler;
import com.apps_lib.multiroom.source.SourceFragment;
import com.apps_lib.multiroom.util.ui.AnimationEndListener;
import com.apps_lib.multiroom.util.ui.AnimationHelper;
import com.apps_lib.multiroom.util.ui.ButtonParamsHelper;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.anim.Rotate3dAnimation;
import com.zoundindustries.multiroom.databinding.FragmentSourcePresetBinding;

public class PresetSourceFragment extends SourceFragment
        implements IPresetAnimationHandler, IRotationHandler {

    public static final String PRESET_INDEX_KEY = "preset_index";

    private int mPresetIndex = 0;

    private FragmentSourcePresetBinding mBinding;

    private float mFadeOutAlpha = SourceFragment.LAYOUT_ALPHA_0;

    private PresetSourceViewModel mViewModel;

    private boolean mRightToLeftAnimationIsRunning = false;
    private boolean mLeftToRightAnimationIsRunning = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_source_preset,
                container, false);

        mViewModel = new PresetSourceViewModel();
        mBinding.setViewModel(mViewModel);

        View rootView = mBinding.getRoot();
        rootView.setEnabled(false);
        rootView.setClickable(false);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mPresetIndex = bundle.getInt(PRESET_INDEX_KEY);
        }

        mBinding.getViewModel().init(this.getActivity(), mPresetIndex, this, this);
    }

    @Override
    public void onDestroyView() {
        if (mBinding != null) {
            mBinding.getViewModel().dispose();
            mBinding = null;
        }

        super.onDestroyView();
    }


    @Override
    public void startAnimationForPresetPlaylistDelete(AnimationEndListener animationEndListener) {
        if (getActivity() == null) {
            return;
        }

        animatePresets(ContextCompat
                        .getDrawable(getContext(), R.drawable.ic_preset_failed),
                getResources().getString(R.string.playlist_deleted),
                animationEndListener);
        mBinding.deletePresetButton.setVisibility(View.GONE);
    }

    @Override
    public void startAnimationForPresetRadioStationDelete(AnimationEndListener animationEndListener) {
        if (getActivity() == null) {
            return;
        }

        animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_failed),
                getResources().getString(R.string.radio_station_deleted),
                animationEndListener);
        mBinding.deletePresetButton.setVisibility(View.GONE);
    }

    @Override
    public void startAnimationForPresetPlaylistAdded(AnimationEndListener animationEndListener, boolean succeeded) {
        if (getActivity() == null) {
            return;
        }

        if (succeeded) {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_succeeded),
                    getResources().getString(R.string.playlist_added),
                    animationEndListener);
        } else {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_failed),
                    getResources().getString(R.string.adding_playlist_failed),
                    animationEndListener);
        }
    }

    @Override
    public void startAnimationForPresetRadioStationAdded(AnimationEndListener animationEndListener,
                                                         boolean succeeded) {
        if (getActivity() == null) {
            return;
        }

        if (succeeded) {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_succeeded),
                    getResources().getString(R.string.radio_station_added),
                    animationEndListener);
        } else {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_failed),
                    getResources().getString(R.string.adding_a_radio_station_failed),
                    animationEndListener);
        }
    }

    @Override
    public void startAnimationForPlayingEmptyPreset(AnimationEndListener animationEndListener) {
        if (getActivity() == null) {
            return;
        }

        animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_failed),
                getResources().getString(R.string.preset_is_empty),
                animationEndListener);
    }

    @Override
    public void startAnimationForUndo(AnimationEndListener animationEndListener,
                                      boolean succeeded) {
        if (getActivity() == null) {
            return;
        }

        if (succeeded) {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_succeeded),
                    getResources().getString(R.string.preset_anim_undo_redo_succeeded),
                    animationEndListener);
        } else {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_failed),
                    getResources().getString(R.string.preset_anim_undo_redo_failed),
                    animationEndListener);
        }
    }

    @Override
    public void startAnimationForRedo(AnimationEndListener animationEndListener,
                                      boolean succeeded) {
        if (getActivity() == null) {
            return;
        }
        if (succeeded) {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_succeeded),
                    getResources().getString(R.string.preset_anim_undo_redo_succeeded),
                    animationEndListener);
        } else {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_failed),
                    getResources().getString(R.string.preset_anim_undo_redo_failed),
                    animationEndListener);
        }
    }

    @Override
    public void showUndoButton() {
        hideShowButtons(mBinding.addPresetButton, mBinding.undoAddPresetButton);
    }

    @Override
    public void hideUndoButton() {
        hideShowButtons(mBinding.undoAddPresetButton, mBinding.addPresetButton);
    }

    @Override
    public void showRedoButton() {
        hideShowButtons(mBinding.deletePresetButton, mBinding.redoDeletePresetButton);
    }

    @Override
    public void hideRedoButton() {
        hideShowButtons(mBinding.redoDeletePresetButton, mBinding.deletePresetButton);
    }

    public void hideShowButtons(final ImageButton toBeHidden,
                                final ImageButton toBeShown) {
        toBeHidden.startAnimation(
                AnimationHelper.getAnimationSet(getContext(),
                        new AnimationEndListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                                toBeHidden.setClickable(false);
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                toBeHidden.setVisibility(View.GONE);
                            }
                        },
                        R.anim.preset_undo_redo_fade_out)
        );

        toBeShown.startAnimation(
                AnimationHelper.getAnimationSet(getContext(),
                        new AnimationEndListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                                toBeShown.setClickable(false);
                                toBeShown.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                toBeShown.setClickable(true);
                            }
                        },
                        R.anim.preset_undo_redo_fade_in)
        );
    }

    private void animatePresets(Drawable icon, String text,
                                final AnimationEndListener finishedListener) {
        if (mBinding == null) {
            return;
        }

        mBinding.artworkAnimationViewIcon.setImageDrawable(icon);
        mBinding.artworkAnimationViewTitle.setText(text);

        mBinding.artworkAnimationViewBackground.startAnimation(
                AnimationHelper.getAnimationSet(getContext(), onAnimationEnd -> {
                            // whole idea of starting animation on animation end seems wrong to me,
                            // but to not break previous logic, this is somehow a cleaner solution
                            mBinding.artworkAnimationViewBackground
                                    .startAnimation(AnimationHelper.getAnimationSet(getContext(),
                                            R.anim.preset_fade_out_from_alpha_70));
                            mBinding.artworkAnimationViewForeground
                                    .startAnimation(AnimationHelper.getAnimationSet(getContext(),
                                            finishedListener,
                                            R.anim.preset_fade_out_from_alpha_100_slower,
                                            R.anim.preset_scale_down_to_0px));
                        },
                        R.anim.preset_fade_in_to_alpha_70));

        mBinding.artworkAnimationViewForeground.startAnimation(
                AnimationHelper.getAnimationSet(getContext(),
                        R.anim.preset_fade_in_to_alpha_100,
                        R.anim.preset_scale_down_to_1px));
    }

    @Override
    public void setFragmentAlphaForOffset(float currentOffset, EFragmentPosition fragmentPosition) {
        View rootView = getView();

        if (rootView != null) {
            View buttonsLayout = rootView.findViewById(R.id.buttonsLayout);
            float layoutAlpha;
            float buttonsLayoutAlpha;
            float alpha = LAYOUT_ALPHA_100 - mFadeOutAlpha;

            if (fragmentPosition == EFragmentPosition.PRIMARY) {
                layoutAlpha = 1f - (currentOffset * alpha);
//                buttonsLayoutAlpha = 1f - (currentOffset * LAYOUT_ALPHA_100);
            } else {
                layoutAlpha = currentOffset * alpha + mFadeOutAlpha;
//                buttonsLayoutAlpha = currentOffset * LAYOUT_ALPHA_100;
            }

            buttonsLayoutAlpha = ButtonParamsHelper
                    .generateAlphaValue(currentOffset, fragmentPosition);

            rootView.setAlpha(layoutAlpha);
            buttonsLayout.setAlpha(buttonsLayoutAlpha);

            if (buttonsLayoutAlpha < LAYOUT_ALPHA_75) {
                setViewClickable(false);
            } else {
                setViewClickable(true);
            }

            changeViewVisibilityIfNeeded(buttonsLayout);

            rootView.invalidate();
        }
    }

    @Override
    public void setFragmentAlphaForPosition(EFragmentPosition fragmentPosition) {
        View rootView = getView();

        if (rootView != null) {
            View buttonsLayout = rootView.findViewById(R.id.buttonsLayout);

            switch (fragmentPosition) {
                case PRIMARY:
                    rootView.setAlpha(LAYOUT_ALPHA_100);
                    buttonsLayout.setAlpha(LAYOUT_ALPHA_100);
                    setViewClickable(true);
                    break;
                case SECONDARY:
                    rootView.setAlpha(mFadeOutAlpha);
                    buttonsLayout.setAlpha(LAYOUT_ALPHA_0);
                    setViewClickable(false);
                    break;
                default:
                    break;
            }

            changeViewVisibilityIfNeeded(buttonsLayout);
        }
    }

    @Override
    public final void setFadeOutAlpha(float fadeOutAlpha, boolean adjustAlphaIfNeeded) {
        mFadeOutAlpha = fadeOutAlpha;

        if (adjustAlphaIfNeeded) {
            View rootView = getView();

            if (rootView != null) {
                float startAlpha = rootView.getAlpha();
                float alphaDifference = startAlpha - mFadeOutAlpha;

                if (Math.abs(alphaDifference) > 0) {
                    final int steps = 5;

                    for (int i = 0; i < steps; i++) {
                        float nextAlpha = alphaDifference / steps * (steps - i - 1);
                        if (alphaDifference > 0) {
                            rootView.setAlpha(nextAlpha);
                        } else {
                            rootView.setAlpha(alphaDifference + nextAlpha);
                        }
                        rootView.invalidate();
                    }
                }

                if (mFadeOutAlpha < LAYOUT_ALPHA_75) {
                    setViewClickable(false);
                }

                rootView.setAlpha(mFadeOutAlpha);
                changeViewVisibilityIfNeeded(rootView.findViewById(R.id.buttonsLayout));
                rootView.invalidate();
            }
        }
    }

    @Override
    public void onOffsetChanged(float offset, int offsetPixel, boolean leftToRight, boolean rightToLeft) {
        mBinding.buttonsLayout.setTranslationX(offsetPixel);
    }

    @Override
    public final ESourceFragmentType getFragmentType() {
        return ESourceFragmentType.PRESET_SOURCE_FRAGMENT;
    }

    @Override
    public void setViewClickable(boolean clickable) {
        View rootView = getView();
        if (rootView != null) {
            rootView.setEnabled(clickable);
            rootView.setClickable(clickable);
        }
    }

    @Override
    public void startLeftToRightRotation(final Drawable drawable) {

        if (mRightToLeftAnimationIsRunning) {
            return;
        }

        mLeftToRightAnimationIsRunning = true;

        final float centerX = mBinding.artworkImageView.getWidth() / 2.0f;
        final float centerY = mBinding.artworkImageView.getHeight() / 2.0f;

        final Animation rotateOut = new Rotate3dAnimation(0, 90, centerX, centerY, 0, true);
        rotateOut.setDuration(375);
        rotateOut.setFillAfter(true);
        rotateOut.setInterpolator(new LinearInterpolator());

        final Animation rotateIn = new Rotate3dAnimation(-90, 0, centerX, centerY, 0, true);
        rotateIn.setDuration(375);
        rotateIn.setFillAfter(true);
        rotateIn.setInterpolator(new LinearInterpolator());

        rotateOut.setAnimationListener((AnimationEndListener) animation -> {
            mViewModel.artwork.set(drawable);
            rotateIn.setAnimationListener((AnimationEndListener)
                    anim -> mLeftToRightAnimationIsRunning = false);
            mBinding.artworkImageView.startAnimation(rotateIn);
        });

        mBinding.artworkImageView.startAnimation(rotateOut);
    }

    @Override
    public void startRightToLeftRotation(final Drawable drawable) {

        if (mLeftToRightAnimationIsRunning) {
            return;
        }

        mRightToLeftAnimationIsRunning = true;

        final float centerX = mBinding.artworkImageView.getWidth() / 2.0f;
        final float centerY = mBinding.artworkImageView.getHeight() / 2.0f;

        final Animation rotateOut = new Rotate3dAnimation(0, -90, centerX, centerY, 0, true);
        rotateOut.setDuration(375);
        rotateOut.setFillAfter(true);
        rotateOut.setInterpolator(new LinearInterpolator());

        final Animation rotateIn = new Rotate3dAnimation(90, 0, centerX, centerY, 0, true);
        rotateIn.setDuration(375);
        rotateIn.setFillAfter(true);
        rotateIn.setInterpolator(new LinearInterpolator());

        rotateOut.setAnimationListener((AnimationEndListener) animation -> {
            mViewModel.artwork.set(drawable);
            rotateIn.setAnimationListener((AnimationEndListener)
                    anim -> mRightToLeftAnimationIsRunning = false);
            mBinding.artworkImageView.startAnimation(rotateIn);
        });

        mBinding.artworkImageView.startAnimation(rotateOut);
    }

    @Override
    public final void setScaleToOffset(float currentOffset, EFragmentPosition fragmentPosition) {
        View rootView = getView();

        if (rootView != null) {
            View viewToScale = rootView.findViewById(R.id.artworkContainer);
            if (viewToScale != null) {
                float layoutScale;
                float scale = LAYOUT_SCALE_100 - LAYOUT_SCALE_90;

                if (fragmentPosition == EFragmentPosition.PRIMARY) {
                    layoutScale = 1f - (currentOffset * scale);
                } else {
                    layoutScale = currentOffset * scale + LAYOUT_SCALE_90;
                }

                viewToScale.setScaleX(layoutScale);
                viewToScale.setScaleY(layoutScale);

                viewToScale.invalidate();
            }
        }
    }
}

package com.zoundindustries.multiroom.source;

import android.bluetooth.BluetoothAdapter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.FragmentSourceBluetoothBinding;

/**
 * Created by lsuhov on 11/05/16.
 */
public class BluetoothSourceFragment extends SimpleSourceFragment {

    private FragmentSourceBluetoothBinding mBinding;
    private BluetoothSourceViewModel mViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_source_bluetooth, container, false);

        mViewModel = new BluetoothSourceViewModel(getActivity(), this);
        mViewModel.init();

        mBinding.setViewModel(mViewModel);

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter != null) {
            mBinding.bluetoothNotificationMsgText.setText(getActivity().getString(R.string.bluetooth_notification_msg,
                    bluetoothAdapter.getName()));
        }

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mBinding != null) {
            if (mBinding.getViewModel() != null) {
                mBinding.getViewModel().onResume();
            }
        }
    }

    @Override
    public void onDestroyView() {
        if (mBinding != null) {
            if (mBinding.getViewModel() != null) {
                mBinding.getViewModel().dispose();
            }
            mBinding = null;
        }

        super.onDestroyView();
    }

    @Override
    protected void setViewClickable(boolean clickable) {
        mViewModel.setViewClickable(clickable);
    }
}

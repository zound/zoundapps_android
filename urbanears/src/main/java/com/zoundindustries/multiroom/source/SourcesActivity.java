package com.zoundindustries.multiroom.source;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.nowPlaying.NowPlayingManager;
import com.apps_lib.multiroom.presets.DownloadPresetsEvent;
import com.apps_lib.multiroom.source.ISourcesListener;
import com.apps_lib.multiroom.source.SourceTab;
import com.apps_lib.multiroom.source.SourcesManager;
import com.apps_lib.multiroom.source.SourcesRegistry;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.connection.ConnectionState;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.connection.IConnectionStateListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivitySourcesBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by lsuhov on 08/05/16.
 */
public class SourcesActivity extends UEActivityBase implements IConnectionStateListener, ISourcesListener {

    public final static String SPEAKER_JUST_CONNECTED = "SPEAKER_JUST_CONNECTED";

    public ActivitySourcesBinding mBinding;

    private SourcesViewModel mSourcesViewModel;

    private int mSourcesIndex = 0;
    private int mTabsIndex = 0;

    private boolean mIsPaused = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sources);

        mSourcesViewModel = new SourcesViewModel();

        setupAppBar();

        enableUpNavigation();

        setupControls();

        SourcesManager.getInstance().setListener(this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        setViewPagersCurrentPage();
        mSourcesViewModel.init(this);
        attachActivityToVolumePopup();
        ConnectionStateUtil.getInstance().addListener(this, true);

        mIsPaused = false;
    }

    public boolean isPaused() {
        return mIsPaused;
    }

    @Subscribe
    public void onSpotifyTokenRetrieved(DownloadPresetsEvent event) {
        SourcesManager.getInstance().updateListOfPresets();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getCurrentIndexesFromViewPagers();
        mSourcesViewModel.dispose();
        ConnectionStateUtil.getInstance().removeListener(this);

        mIsPaused = true;
    }

    void setViewPagersCurrentPage() {
        mBinding.sourcesViewPager.setCurrentItem(mSourcesIndex);
        mBinding.sourceTabsViewPager.setCurrentItem(mTabsIndex);
    }

    private void getCurrentIndexesFromViewPagers() {
        mSourcesIndex = mBinding.sourcesViewPager.getCurrentItem();
        mTabsIndex = mBinding.sourceTabsViewPager.getCurrentItem();
    }

    private void setupControls() {
        setTitleForCurrentRadio();

        setupViewPagers();

        makeTabsFromAroundCurrentTabVisible();

        setupVolumeButton();

        mBinding.slidingLayout.addPanelSlideListener(new com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener() {
            /**
             * Called when a sliding pane's position changes.
             *
             * @param panel       The child view that was moved
             * @param slideOffset The new offset of this sliding pane within its range, from 0-1
             */
            public void onPanelSlide(View panel, float slideOffset) {
                mBinding.footerFragmentContainer.setAlpha(1 - slideOffset);
                mBinding.nowPlayingContainer.setAlpha(slideOffset);

                if (slideOffset == 1) {
                    mBinding.footerFragmentContainer.setVisibility(View.GONE);
                } else if (slideOffset == 0) {
                    mBinding.nowPlayingContainer.setVisibility(View.GONE);
                } else {
                    mBinding.footerFragmentContainer.setVisibility(View.VISIBLE);
                    mBinding.nowPlayingContainer.setVisibility(View.VISIBLE);
                }
            }

            /**
             * Called when a sliding panel state changes
             *
             * @param panel The child view that was slid to an collapsed position
             */
            public void onPanelStateChanged(View panel, com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState previousState, com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState newState) {
            }
        });
    }

    private void setTitleForCurrentRadio() {
        Radio radio = NetRemoteManager.getInstance().getCurrentRadio();
        if (radio != null) {
            setTitle(radio.getFriendlyName());
        }
    }

    private void setupVolumeButton() {
        ImageButton volumeButton = (ImageButton) findViewById(R.id.buttonVolume);
        volumeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationHelper.goToVolumeActivity(SourcesActivity.this);
            }
        });
    }

    private void setupViewPagers() {
        mBinding.sourcesViewPager.setOffscreenPageLimit(3);
        mBinding.sourcesViewPager.setPageMargin(getPageMarginInPixel());
        mBinding.sourcesViewPager.setClipToPadding(false);

        initSourcesRegistry();

        SourcesAdapter mAdapter = new SourcesAdapter(getSupportFragmentManager());
        mBinding.sourcesViewPager.setAdapter(mAdapter);
        mBinding.sourcesViewPager.addOnPageChangeListener(new SourcesPageChangeListener(this, (TextView) findViewById(R.id.titleTextView)));

        SourcesInteractionHandler interactionHandler = new SourcesInteractionHandler(mBinding.sourcesViewPager, mAdapter);
        mBinding.sourcesViewPager.addOnPageChangeListener(interactionHandler);
        mBinding.sourcesViewPager.setOnTouchListener(interactionHandler);

        SourceTabsAdapter tabsAdapter = new SourceTabsAdapter();
        mBinding.sourceTabsViewPager.setAdapter(tabsAdapter);
        mBinding.sourceTabsViewPager.setBottomViewPager(mBinding.sourcesViewPager);

        mBinding.sourcesViewPager.setCurrentItem(SourcesRegistry.getInstance().getFirstPage());
        mBinding.sourceTabsViewPager.setCurrentItem(SourcesRegistry.getInstance().getFirstPage());

        mSourcesIndex = getAdapterPosition();
        mTabsIndex = mSourcesIndex;
    }

    private void initSourcesRegistry() {
        SourcesRegistry.init();

        SourcesRegistry.getInstance().register(SourceTab.PresetTab, "1");
        SourcesRegistry.getInstance().register(SourceTab.PresetTab, "2");
        SourcesRegistry.getInstance().register(SourceTab.PresetTab, "3");
        SourcesRegistry.getInstance().register(SourceTab.PresetTab, "4");
        SourcesRegistry.getInstance().register(SourceTab.PresetTab, "5");
        SourcesRegistry.getInstance().register(SourceTab.PresetTab, "6");
        SourcesRegistry.getInstance().register(SourceTab.PresetTab, "7");

        boolean isAuxPresent = false;

        if (SourcesManager.getInstance().getSpeakerModes() != null) {
            for (NodeSysCapsValidModes.ListItem item : SourcesManager.getInstance().getSpeakerModes().getEntries()) {
                NodeSysCapsValidModes.Mode mode = NodeSysCapsValidModes.ConvertIdToEnum(item.getId());
                if (mode == NodeSysCapsValidModes.Mode.AuxIn) {
                    isAuxPresent = true;
                }
            }
        } else {
            NavigationHelper.goToHomeAndClearActivityStack(this);
        }

        if (isAuxPresent) {
            SourcesRegistry.getInstance().register(SourceTab.AuxinTab, R.drawable.ic_small_auxin);
            SourcesRegistry.getInstance().register(SourceTab.BluetoothTab, R.drawable.ic_small_bluetooth);
            SourcesRegistry.getInstance().register(SourceTab.CloudTab, R.drawable.ic_small_cloud);
        } else {
            SourcesRegistry.getInstance().register(SourceTab.BluetoothTab, R.drawable.ic_small_bluetooth);
            SourcesRegistry.getInstance().register(SourceTab.CloudTab, R.drawable.ic_small_cloud);
        }

        SourcesRegistry.getInstance().defineCarouselSize();
    }

    private int getPageMarginInPixel() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        float widthPixels = metrics.widthPixels;
        float scaleFactor = metrics.density;
        float widthDp = widthPixels / scaleFactor;

        float marginInDp2 = 41.0f / 100.0f * widthDp;

        int retValue = Math.round(marginInDp2 * (getResources().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT)) * (-1);

        int diff = Math.abs(metrics.densityDpi + retValue);

        if (diff > 400) {
            retValue += 200;
        } else if (diff > 350) {
            retValue += 150;
        } else if (diff > 250) {
            retValue += 100;
        } else if (diff > 150) {
            retValue += 50;
        } else {
            retValue += 40;
        }

//        Log.i("Norbi", "widthPixels=" + metrics.widthPixels + "   densityDpi=" + metrics.densityDpi + "    xdpi=" + metrics.xdpi + "    scaledDensity=" + metrics.scaledDensity + "     retValue=" + retValue);

        return retValue;
    }

    private void makeTabsFromAroundCurrentTabVisible() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        Display display = getWindowManager().getDefaultDisplay();
        display.getMetrics(displayMetrics);

        int widthPixels = displayMetrics.widthPixels;
        int padding = (int) (widthPixels / 2.65);

        mBinding.sourceTabsViewPager.setPadding(padding, 0, padding, 0);
        mBinding.sourceTabsViewPager.requestLayout();
    }

    @Override
    protected void onDestroy() {

        mBinding.sourceTabsViewPager.dispose();
        EventBus.getDefault().unregister(this);

        super.onDestroy();
    }

    @Override
    public void onStateUpdate(ConnectionState newState) {
        switch (newState) {
            case DISCONNECTED:
            case INVALID_SESSION:
            case NOT_CONNECTED_TO_RADIO:
            case NO_WIFI_OR_ETHERNET:
                NavigationHelper.goToHome(SourcesActivity.this);
                this.finish();
                break;
        }
    }

    @Override
    public void spotifyAccountNeeded() {
        // Nothing to do here
    }

    private int getAdapterPosition() {
        long lastKnownPresetIndex;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getBoolean(SPEAKER_JUST_CONNECTED, false)) {
            lastKnownPresetIndex = calculateAdapterPositionBasedOnPresetIndexAndMode();
        } else {
            lastKnownPresetIndex = SourcesRegistry.getInstance().getFirstPage();
        }
        return (int) lastKnownPresetIndex;
    }

    private int calculateAdapterPositionBasedOnPresetIndexAndMode() {
        int indexToSet;

        NodeSysCapsValidModes.Mode currentMode = NowPlayingManager.getInstance().getNowPlayingDataModel().currentMode.get();
        int indexOfCurrentlyPlayingPreset = NowPlayingManager.getInstance().getNowPlayingDataModel().indexOfCurrentlyPlayingPreset.get();

        if (currentMode == null) {
            return  SourcesRegistry.getInstance().getFirstPage();
        }

        switch (currentMode) {
            case AuxIn:
                indexToSet = SourcesRegistry.getInstance().getFirstPage() + SourceTab.getValue(SourceTab.AuxinTab);
                break;
            case Bluetooth:
                indexToSet = SourcesRegistry.getInstance().getFirstPage() + SourceTab.getValue(SourceTab.BluetoothTab);
                break;
            case RCA:
                indexToSet = SourcesRegistry.getInstance().getFirstPage() + SourceTab.getValue(SourceTab.RCATab);
                break;
            default:
                indexToSet = SourcesRegistry.getInstance().getFirstPage() + indexOfCurrentlyPlayingPreset;
                break;
        }

        return indexToSet;
    }

    public final void onFooterClicked() {
        if (!mBinding.slidingLayout.isOverlayed()) {
            mBinding.slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        }
    }

    @Override
    public void onBackPressed() {
        if (mBinding.slidingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            mBinding.slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }
}

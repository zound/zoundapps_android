package com.zoundindustries.multiroom.source;

import android.databinding.BaseObservable;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.view.View;

import com.apps_lib.multiroom.connection.NewConnectionWasMadeEvent;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysMode;
import com.frontier_silicon.NetRemoteLib.Radio.INodeNotificationCallback;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by lsuhov on 17/05/16.
 */
public abstract class SourceViewModelBase extends BaseObservable implements INodeNotificationCallback {

    public ObservableBoolean modeActivated = new ObservableBoolean(false);
    public ObservableBoolean modeActivatingOrCheckingIfActivated = new ObservableBoolean(false);
    public ObservableBoolean isClickable = new ObservableBoolean(false);

    NodeSysCapsValidModes.Mode mMode;

    SourceViewModelBase(NodeSysCapsValidModes.Mode mode) {
        mMode = mode;
    }

    public void init() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        checkIfModeIsActive();

        reattachNodeListeners();

        modeActivatingOrCheckingIfActivated.addOnPropertyChangedCallback(new OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                isClickable.set(!modeActivatingOrCheckingIfActivated.get());
            }
        });
    }

    private void checkIfModeIsActive() {
        Radio radio = getActionableRadio();
        if (radio == null || radio.isMissing()) {
            return;
        }

        modeActivatingOrCheckingIfActivated.set(true);

        radio.getMode(new Radio.IRadioModeCallback() {
            @Override
            public void getCurrentMode(NodeSysCapsValidModes.Mode currentMode) {
                modeActivatingOrCheckingIfActivated.set(false);

                modeActivated.set((currentMode != null && currentMode == mMode));
            }
        });
    }

    public void onActivateClicked(View view) {
        Radio radio = getActionableRadio();
        if (radio == null || radio.isMissing())
            return;

        modeActivatingOrCheckingIfActivated.set(true);

        radio.setMode(mMode, false);
    }

    @Override
    public void onNodeUpdated(NodeInfo node) {
        if (node instanceof NodeSysMode) {
            Radio radio = getActionableRadio();
            if (radio == null || radio.isMissing())
                return;

            radio.getMode(new Radio.IRadioModeCallback() {
                @Override
                public void getCurrentMode(NodeSysCapsValidModes.Mode currentMode) {
                    modeActivatingOrCheckingIfActivated.set(false);

                    if (currentMode != null) {
                        modeActivated.set(currentMode == mMode);
                    }
                }
            });
        }
    }

    @Subscribe
    public void onNewConnection(NewConnectionWasMadeEvent event) {
        checkIfModeIsActive();
        reattachNodeListeners();
    }

    //Depending on selected mode we need the clicked radio or his server
    public abstract Radio getActionableRadio();

    public void reattachNodeListeners() {
        Radio radio = getActionableRadio();
        if (radio != null) {
            radio.removeNotificationListener(this);
            radio.addNotificationListener(this);
        }
    }

    public void dispose() {
        Radio radio = NetRemoteManager.getInstance().getCurrentRadio();
        if (radio != null) {
            radio.removeNotificationListener(this);
        }

        EventBus.getDefault().unregister(this);
    }

    void setViewClickable(boolean clickable) {
        isClickable.set(clickable);
    }
}

package com.zoundindustries.multiroom.source;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps_lib.multiroom.NavigationHelper;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.FragmentSourceCloudBinding;
import com.zoundindustries.multiroom.source.apps.AirplayAppActivity;
import com.zoundindustries.multiroom.source.apps.GoogleCastAppActivity;
import com.zoundindustries.multiroom.source.apps.SpotifyAppActivity;

/**
 * Created by lsuhov on 11/05/16.
 */
public class CloudSourceFragment extends SimpleSourceFragment {

    private FragmentSourceCloudBinding mBinding;
    private CloudSourceViewModel mViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_source_cloud, container, false);

        mViewModel = new CloudSourceViewModel(this);
        mBinding.setViewModel(mViewModel);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupControls();
    }


    @Override
    public void onResume() {
        super.onResume();
        mViewModel.init(getActivity());
    }
    
    private void setupControls() {
        mBinding.buttonActivateIR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isActive()) {
                    return;
                }
                mBinding.getViewModel().onActivateClicked(v);
            }
        });

        mBinding.buttonSpotifyConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isActive()) {
                    return;
                }
                NavigationHelper.goToActivity(getActivity(), SpotifyAppActivity.class,
                        NavigationHelper.AnimationType.SlideToLeft);
            }
        });

        mBinding.buttonGoogleCast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isActive()) {
                    return;
                }
                NavigationHelper.goToActivity(getActivity(), GoogleCastAppActivity.class,
                        NavigationHelper.AnimationType.SlideToLeft);
            }
        });

        mBinding.buttonAirplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isActive()) {
                    return;
                }
                NavigationHelper.goToActivity(getActivity(), AirplayAppActivity.class,
                        NavigationHelper.AnimationType.SlideToLeft);
            }
        });
    }

    @Override
    public void onPause() {
        mViewModel.dispose();
        super.onPause();
    }

    @Override
    protected void setViewClickable(boolean clickable) {
        mViewModel.setViewClickable(clickable);
    }
}

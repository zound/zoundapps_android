package com.zoundindustries.multiroom.source;

import android.app.Activity;
import android.databinding.Observable;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.nowPlaying.NowPlayingManager;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.zoundindustries.multiroom.browse_ir.BrowseIRActivity;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lsuhov on 17/05/16.
 */
public class CloudSourceViewModel extends SourceViewModelBase {
    private Activity mActivity;
    private Timer mTimer;
    private OnPropertyChangedCallback mPropertyChangedCallback;

    private IActiveStatusProvider mActiveStatusProvider;

    CloudSourceViewModel(IActiveStatusProvider activeStatusProvider) {
        super(NodeSysCapsValidModes.Mode.IR);
        mActiveStatusProvider = activeStatusProvider;
    }

    public void init(Activity activity) {
        super.init();
        mActivity = activity;
    }


    @Override
    public void onActivateClicked(View view) {
        Radio radio = NetRemoteManager.getInstance().getCurrentRadio();
        if (radio == null || radio.isMissing() || !mActiveStatusProvider.isActive())
            return;

        NodeSysCapsValidModes.Mode currentMode = NowPlayingManager.getInstance().getNowPlayingDataModel().currentMode.get();

        if (currentMode != NodeSysCapsValidModes.Mode.IR) {
            addListeners();
            radio.setMode(mMode, false);

            modeActivatingOrCheckingIfActivated.set(true);
            startTimer();
        } else {
            goToBrowseIRActivity();
        }
    }

    @Override
    public Radio getActionableRadio() {
        return NetRemoteManager.getInstance().getCurrentRadio();
    }

    private void addListeners() {
        mPropertyChangedCallback = new OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (observable == modeActivated && modeActivated.get()) {

                    goToBrowseIRActivity();
                    disposeWhenChangingModes();
                }
            }
        };

        modeActivated.addOnPropertyChangedCallback(mPropertyChangedCallback);
    }

    private void goToBrowseIRActivity() {
        NavigationHelper.goToActivity(mActivity, BrowseIRActivity.class,
                NavigationHelper.AnimationType.SlideUp);
    }

    private void disposeWhenChangingModes () {
        modeActivatingOrCheckingIfActivated.set(false);
        stopTimer();
    }


    private void startTimer() {
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                modeActivatingOrCheckingIfActivated.set(false);
            }
        }, 20 * 1000);
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }
    }

    public void dispose() {
        modeActivated.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mActivity = null;
    }
}

package com.zoundindustries.multiroom.source;

import android.view.View;

import com.apps_lib.multiroom.source.SourceFragment;

/**
 * Created by nbalazs on 17/01/2017.
 */

public abstract class SimpleSourceFragment extends SourceFragment implements IActiveStatusProvider {

    private boolean mIsActive = true;

    @Override
    public void setFragmentAlphaForOffset(float currentOffset, EFragmentPosition fragmentPosition) {
        View rootView = getView();

        if (rootView != null) {
            float layoutAlpha;

            if (fragmentPosition == EFragmentPosition.PRIMARY) {
                layoutAlpha = 1f - (currentOffset * LAYOUT_ALPHA_100);
            } else {
                layoutAlpha = currentOffset * LAYOUT_ALPHA_100;
            }

            if (layoutAlpha < LAYOUT_ALPHA_75) {
                setViewClickable(false);
                mIsActive = false;
            } else {
                setViewClickable(true);
                mIsActive = true;
            }

            rootView.setAlpha(layoutAlpha);

            changeViewVisibilityIfNeeded(rootView);

            rootView.invalidate();
        }
    }

    @Override
    public void setFragmentAlphaForPosition(EFragmentPosition fragmentPosition) {
        View rootView = getView();

        if (rootView != null) {
            switch (fragmentPosition) {
                case PRIMARY:
                    rootView.setAlpha(LAYOUT_ALPHA_100);
                    setViewClickable(true);
                    mIsActive = true;
                    break;
                case SECONDARY:
                    rootView.setAlpha(LAYOUT_ALPHA_0);
                    setViewClickable(false);
                    mIsActive = false;
                    break;
                default:
                    break;
            }

            changeViewVisibilityIfNeeded(rootView);
        }
    }

    @Override
    public void setFadeOutAlpha(float fadeOutAlpha, boolean adjustAlphaIfNeeded) {
        // Do nothing in this case
    }


    @Override
    public void onOffsetChanged(float offset, int offsetPixel, boolean leftToRight, boolean rightToLeft) {
        // Do nothing here
    }

    @Override
    public final void setScaleToOffset(float currentOffset, EFragmentPosition fragmentPosition) {
        // Default declaration, do nothing here
    }

    @Override
    public ESourceFragmentType getFragmentType() {
        return ESourceFragmentType.SIMPLE_SOURCE_FRAGMENT;
    }

    @Override
    public final boolean isActive() {
        return mIsActive;
    }
}

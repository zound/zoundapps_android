package com.zoundindustries.multiroom.browse_ir;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.browse_ir.BrowseIRAdapter;
import com.apps_lib.multiroom.browse_ir.events.PlayStationEvent;
import com.apps_lib.multiroom.browse_ir.events.ShowLoadingProgressEvent;
import com.apps_lib.multiroom.browse_ir.model.BrowseIRManager;
import com.apps_lib.multiroom.widgets.DividerItemDecoration;
import com.apps_lib.multiroom.widgets.EditTextWithBackEvent;
import com.frontier_silicon.components.common.GuiUtils;
import com.frontier_silicon.components.connection.ConnectionState;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.connection.IConnectionStateListener;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivityBrowseIrBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by lsuhov on 27/06/16.
 */
public class BrowseIRActivity extends UEActivityBase implements IConnectionStateListener {

    private ActivityBrowseIrBinding mBinding;
    private BrowseIRViewModel mViewModel;
    private BrowseIRAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_browse_ir);
        mViewModel = new BrowseIRViewModel(this);
        mBinding.setViewModel(mViewModel);

        Window window = getWindow();
        if (window != null) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED);
        }

        setupControls();
    }

    private void setupControls() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);

        mBinding.browseRecyclerView.setHasFixedSize(true);
        mBinding.browseRecyclerView.setLayoutManager(layoutManager);
        mBinding.browseRecyclerView.addItemDecoration(itemDecoration);

        mAdapter = new BrowseIRAdapter(getApplicationContext());
        mBinding.browseRecyclerView.setAdapter(mAdapter);

        mBinding.searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                GuiUtils.hideKeyboard(mBinding.searchEditText, BrowseIRActivity.this);
                if (actionId == EditorInfo.IME_ACTION_DONE || (actionId == EditorInfo.IME_ACTION_NEXT && keyEvent == null)) {
                    String searchText = mBinding.searchEditText.getText().toString().trim();
                    if (TextUtils.isEmpty(searchText)) {
                        return false;
                    }

                    mViewModel.search(searchText);
                    mBinding.focusableLayout.requestFocus();

                    return true;
                }
                return false;
            }
        });

        mBinding.searchEditText.setOnEditTextImeBackListener(new EditTextWithBackEvent.EditTextImeBackListener() {
            @Override
            public void onImeBack(EditTextWithBackEvent ctrl, String text) {
                BrowseIRActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mBinding.searchEditText.setText("");
                    }
                });
                mViewModel.onSearchCancelled();
            }
        });

        mBinding.searchEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (view.getId() == mBinding.searchEditText.getId()) {
                    if (hasFocus) {
                        mViewModel.onSearchInitiated();
                    }
                }
            }
        });
        mBinding.buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mBinding.buttonDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeBrowseActivity();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        mViewModel.onStart(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        attachActivityToVolumePopup();
        ConnectionStateUtil.getInstance().addListener(this, true);

        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        ConnectionStateUtil.getInstance().removeListener(this);
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onStop() {

        mViewModel.onStop();

        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (!mViewModel.tryNavigateUp()) {
            closeBrowseActivity();
        }
    }

    private void closeBrowseActivity() {
        GuiUtils.hideKeyboard(mBinding.searchEditText, this);

        if (mViewModel.isSearchRunning()) {
            BrowseIRManager.reset();
        }

        super.onBackPressed();

        overridePendingTransition(0, R.anim.slide_out_down);
    }

    @Override
    protected void onDestroy() {

        mBinding.setViewModel(null);
        mViewModel.dispose();
        mViewModel = null;

        super.onDestroy();
    }

    @Override
    public void onStateUpdate(ConnectionState newState) {
        switch (newState) {
            case DISCONNECTED:
            case INVALID_SESSION:
            case NOT_CONNECTED_TO_RADIO:
            case NO_WIFI_OR_ETHERNET:
                NavigationHelper.goToHome(this);
                //this.finish();
                break;
        }
    }

    @Subscribe
    public void onShowLoadingScreen(ShowLoadingProgressEvent event) {
        mBinding.browseRecyclerView.scrollToPosition(0);
    }

    @Subscribe
    public void onPlayStationInitiated(final PlayStationEvent event) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isFinishing()) {
                        closeBrowseActivity();
                    }
                }
            });
        }
    }
}

package com.zoundindustries.multiroom.myHome;

import android.app.Activity;
import android.content.res.Resources;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;

import com.apps_lib.multiroom.Preferences;
import com.apps_lib.multiroom.multiroom.MultiroomRebuildGroups;
import com.apps_lib.multiroom.myHome.HomeSpeakerPlayInformationProvider;
import com.apps_lib.multiroom.myHome.HomeSpeakerPlayModel;
import com.apps_lib.multiroom.myHome.ISwitchCastToMultiroomListener;
import com.apps_lib.multiroom.myHome.IToManyMultiSpeakersDialogListener;
import com.apps_lib.multiroom.myHome.IUnlockedSpeakersDialogListener;
import com.apps_lib.multiroom.myHome.PlayingCastInfoDialogFragment;
import com.apps_lib.multiroom.myHome.SpeakerSelectedEvent;
import com.apps_lib.multiroom.myHome.speakers.SpeakerModel;
import com.apps_lib.multiroom.myHome.speakers.SpeakerNameManager;
import com.apps_lib.multiroom.persistence.PersistenceMgr;
import com.apps_lib.multiroom.settings.multi.streamingQuality.StreamingQualityMgr;
import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.ISpeakerImageIdListener;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.DelayedPostsManager;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.frontier_silicon.components.multiroom.MultiroomGroupModel;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;
import com.zoundindustries.multiroom.speakerImages.UrbanearsSpeakerTypeDecoder;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by lsuhov on 06/05/16.
 */
public class HomeSpeakerViewModel extends BaseObservable implements CompoundButton.OnCheckedChangeListener, DelayedPostsManager.IPostCallback {

    private Activity mActivity;
    protected SpeakerModel mHomeSpeakerModel;

    public ObservableField<BitmapDrawable> speakerImage = new ObservableField<>();
    public ObservableBoolean isPlaying = new ObservableBoolean(false);
    public ObservableField<String> playingInformation = new ObservableField<>();
    public ObservableBoolean isMultiModeChanging = new ObservableBoolean(false);
    public ObservableBoolean loading = new ObservableBoolean(false);
    public ObservableBoolean isMasterPresent = new ObservableBoolean(false);

    private boolean mIsPlayingCast = false;
    public boolean mIsMultiMode = false;

    private boolean mCompoundButtonOriginalValue = false;
    private CompoundButton mCompoundButton = null;

    private final long SPINNER_TIMER_ID;
    private static final int RETRY_COUNT = 3;
    private static final int[] RETRY_TIMEOUTS = {5000, 5000, 1500, 500};
    private int mFallbackNumber;

    private final long REFRESH_MODEL_TIMER_ID;
    private static final int CONTENT_REFRESH_TIMEOUT = 15000;


    private static final int MAX_SPEAKER_COUNT_IN_MULTI_ROOM = 5;

    public HomeSpeakerViewModel(Activity activity, SpeakerModel homeSpeakerModel, boolean masterIsPresent) {
        mActivity = activity;
        mHomeSpeakerModel = homeSpeakerModel;

        DelayedPostsManager delayedPostsManager = DelayedPostsManager.getInstance();
        SPINNER_TIMER_ID = delayedPostsManager.registerNewCallbackID();
        REFRESH_MODEL_TIMER_ID = delayedPostsManager.registerNewCallbackID();

        isMasterPresent.set(masterIsPresent);
        allowOnlyUrbanearsSpeakers();
        init();
    }

    private void init() {
        updateCheckBox();
        updateSpeakerImage();
        updatePlayingInformation();

        mFallbackNumber = RETRY_COUNT;
        scheduleUpdateRefresh();


    }

    private void allowOnlyUrbanearsSpeakers() {
        if (!UrbanearsSpeakerTypeDecoder.isUrbanearsSpeaker(mHomeSpeakerModel.deviceModel.mRadio.getModelName())) {
            isMasterPresent.set(false);
        }
    }

    private void updatePlayingInformation() {

        if (mHomeSpeakerModel.deviceModel == null) {
            FsLogger.log("HomeSpeakerViewModel: mHomeSpeakerModel.deviceModel was found null in updatePlayingInformation",
                    LogLevel.Error);
            return;
        }

        HomeSpeakerPlayInformationProvider.retrieveSpeakerPlayingInfo(mHomeSpeakerModel.deviceModel, mActivity,
                new HomeSpeakerPlayInformationProvider.IHomeSpeakerPlayInformationListener() {
                    @Override
                    public void onPlayInfoRetrieved(HomeSpeakerPlayModel homeSpeakerPlayModel) {
                        //check if disposed
                        if (mHomeSpeakerModel == null) {
                            return;
                        }

                        isPlaying.set(!mHomeSpeakerModel.deviceModel.isClient() && homeSpeakerPlayModel.isPlaying);
                        mIsPlayingCast = homeSpeakerPlayModel.isPlayingCast;
                        playingInformation.set(homeSpeakerPlayModel.playingInformation);
                    }
                });
    }

    private void updateSpeakerImage() {
        Radio radio = getRadio();

        SpeakerManager.getInstance().retrieveImageIdAndSoftwareVersionForRadio(radio, ESpeakerImageSizeType.Home, new ISpeakerImageIdListener() {

            @Override
            public void onImageIdRetrieved(int imageId) {
                if (mActivity == null) {
                    return;
                }
                Resources resources = mActivity.getResources();

                Bitmap speakerImageBitmap = BitmapFactory.decodeResource(resources, imageId);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(resources, speakerImageBitmap);

                speakerImage.set(bitmapDrawable);
            }
        });
    }

    private boolean checkIfIsUpdateAvailable(String serialNumber) {
        return PersistenceMgr.getInstance().checkIfSpeakerIsUpdatable(serialNumber);
    }

    private boolean checkIfIsUnlockedSpeaker(String softwareVersion) {
        if (softwareVersion.contains("unlocked")) {
            return true;
        }

        return false;
    }


    private void updateCheckBox() {
        mIsMultiMode = mHomeSpeakerModel.isInMultiMode;
    }

    @Bindable
    public String getRadioName() {
        return SpeakerNameManager.getInstance().getNameForRadio(getRadio());
    }

    private Radio getRadio() {
        if (mHomeSpeakerModel.deviceModel != null && mHomeSpeakerModel.deviceModel.mRadio != null) {
            return mHomeSpeakerModel.deviceModel.mRadio;
        }
        return null;
    }

    @SuppressWarnings("unused")
    public void onListItemClicked(View view) {
        if (UrbanearsSpeakerTypeDecoder.isUrbanearsSpeaker(mHomeSpeakerModel.deviceModel.mRadio.getModelName())) {
            if (checkIfIsUnlockedSpeaker(getRadio().getFirmwareVersion()) && (getDifferenceInDaysFromNowToLastTimeShowingTheUnlockedSpeakerMsg() >= 7 || isTheFirstTimeConnectingToUnlockedSpeaker())) {
                displayUnlockedDialogFragmentIfNeeded();
            } else if (checkIfIsUpdateAvailable(mHomeSpeakerModel.deviceModel.mRadio.getSN()) && (getDifferenceInDaysFromNowToLastTimeShowingTheUpdateAvailableMsg() >= 7 || isTheFirstTimeShowingTheUpdateAvailableMsg())) { // TODO: Add the last 7 days check
                displayUpdateDialogFragmentIfNeeded();
            } else {
                EventBus.getDefault().post(new SpeakerSelectedEvent(mHomeSpeakerModel.deviceModel));
                loading.set(true);
            }
        } else {
            EventBus.getDefault().post(new SpeakerSelectedEvent(mHomeSpeakerModel.deviceModel));
            loading.set(true);
        }
    }

    private void displayUpdateDialogFragmentIfNeeded() {
        DialogFragment updateFragment = OldSWWarningDialogFragment.newInstance(mHomeSpeakerModel, new IOldUpdateListener() {
            @Override
            public void onDialogFinished() {
                if (mHomeSpeakerModel == null) {
                    return;
                }
                loading.set(true);
                EventBus.getDefault().post(new SpeakerSelectedEvent(mHomeSpeakerModel.deviceModel));
            }
        });

        FragmentManager fragmentManager = ((FragmentActivity) mActivity).getSupportFragmentManager();
        // do not allow to open 2 or more dialogs at the same time
        if (fragmentManager.findFragmentByTag("OldUpdateWarningDialogFragment") != null) {
            return;
        }
        updateFragment.show(fragmentManager, "OldUpdateWarningDialogFragment");
    }

    private void displayUnlockedDialogFragmentIfNeeded() {
        DialogFragment unlockedSpeakersDialogFragment = UnlockedModulesDialogFragment.newInstance(mHomeSpeakerModel, new IUnlockedSpeakersDialogListener() {
            @Override
            public void onDialogFinished() {
                if (mHomeSpeakerModel == null) {
                    return;
                }
                loading.set(true);
                EventBus.getDefault().post(new SpeakerSelectedEvent(mHomeSpeakerModel.deviceModel));
            }
        });

        FragmentManager fragmentManager = ((FragmentActivity) mActivity).getSupportFragmentManager();
        // do not allow to open 2 or more dialogs at the same time
        if (fragmentManager.findFragmentByTag("SwitchingToUnlockedDialogFragment") != null) {
            return;
        }
        unlockedSpeakersDialogFragment.show(fragmentManager, "SwitchingToUnlockedDialogFragment");
    }


    private long getDifferenceInDaysFromNowToLastTimeShowingTheUnlockedSpeakerMsg() {
        long dayDifference = 0;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = null;
        Date dateFromPreferences = null;

        String lastUpdatedDate = PersistenceMgr.getInstance().getCheckingDateForUnlocekdSpeakers(mHomeSpeakerModel.deviceModel.mRadio.getSN());

        try {
            dateFromPreferences = simpleDateFormat.parse(lastUpdatedDate);
            currentDate = simpleDateFormat.parse(simpleDateFormat.format(Calendar.getInstance().getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (currentDate != null && dateFromPreferences != null) {
            long difference = currentDate.getTime() - dateFromPreferences.getTime();
            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            dayDifference = difference / daysInMilli;

        }

        return Math.abs(dayDifference);
    }

    private long getDifferenceInDaysFromNowToLastTimeShowingTheUpdateAvailableMsg() {
        long dayDifference = 0;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = null;
        Date dateFromPreferences = null;

        String lastUpdatedDate = PersistenceMgr.getInstance().getMomentOfShowingTheUpdateSpeakerMsg(mHomeSpeakerModel.deviceModel.mRadio.getSN());

        try {
            dateFromPreferences = simpleDateFormat.parse(lastUpdatedDate);
            currentDate = simpleDateFormat.parse(simpleDateFormat.format(Calendar.getInstance().getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (currentDate != null && dateFromPreferences != null) {
            long difference = currentDate.getTime() - dateFromPreferences.getTime();
            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            dayDifference = difference / daysInMilli;

        }

        return Math.abs(dayDifference);
    }

    private boolean isTheFirstTimeConnectingToUnlockedSpeaker() {
        String lastUpdatedDate = PersistenceMgr.getInstance().getCheckingDateForUnlocekdSpeakers(mHomeSpeakerModel.deviceModel.mRadio.getSN());

        if (TextUtils.isEmpty(lastUpdatedDate)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isTheFirstTimeShowingTheUpdateAvailableMsg() {
        String lastUpdatedDate = PersistenceMgr.getInstance().getMomentOfShowingTheUpdateSpeakerMsg(mHomeSpeakerModel.deviceModel.mRadio.getSN());

        if (TextUtils.isEmpty(lastUpdatedDate)) {
            return true;
        } else {
            return false;
        }
    }

    private void sendMultiroomCommand(boolean checked) {
        Radio radio = mHomeSpeakerModel.deviceModel.mRadio;

        MultiroomGroupManager.getInstance().sendSingleGroupCommand(radio, checked);
    }

    @Override
    public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
        if (mHomeSpeakerModel.isInMultiMode == isChecked) {
            return;
        }

        if (isChecked) {
            MultiroomGroupManager groupManager = MultiroomGroupManager.getInstance();
            Set<MultiroomGroupModel> groups = groupManager.getGroupsFromDeviceMap();
            MultiroomGroupModel theGroup = null;

            for (MultiroomGroupModel group : groups) {
                if (!groupManager.isUngroupedDevicesGroup(group.mGroupId)) {
                    theGroup = group;
                    break;
                }
            }

            if (theGroup != null) {
                int groupMembersCount = groupManager.getAllDevicesForGroupId(theGroup.mGroupId).size();
                if (groupMembersCount >= MAX_SPEAKER_COUNT_IN_MULTI_ROOM) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            buttonView.setChecked(true);
                            showTooManySpeakersInMultiRoomAlert(buttonView);
                        }
                    });
                    return;
                }
            }
        }

        // Save temporary values
        lockCompoundButton(buttonView, !isChecked);

        if (isChecked && mIsPlayingCast) {
            DialogFragment fragment = SwitchingToMultiRoomPlayingCastDialogFragment.newInstance(new ISwitchCastToMultiroomListener() {
                @Override
                public void onResult(boolean switchToMultiroom) {
                    Preferences.getInstance().setMultiroomCastDialogWasShown(true);

                    if (switchToMultiroom) {
                        setGroupingModeChanged(true);
                    } else {
                        if (mActivity == null) {
                            return;
                        }
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                restoreCompoundButton();
                                unlockCompoundButton();
                            }
                        });
                    }
                }
            });
            FragmentManager fragmentManager = ((FragmentActivity) mActivity).getSupportFragmentManager();
            // do not allow to open 2 or more dialogs at the same time
            if (fragmentManager.findFragmentByTag("SwitchingToMultiRoomPlayingCastDialogFragment") != null) {
                return;
            }
            fragment.show(fragmentManager, "SwitchingToMultiRoomPlayingCastDialogFragment");
        } else {
            setGroupingModeChanged(isChecked);
        }

    }

    private void showTooManySpeakersInMultiRoomAlert(final CompoundButton buttonView) {
        DialogFragment fragment = ToManyMultiSpeakersDialogFragment.newInstance(new IToManyMultiSpeakersDialogListener() {
            @Override
            public void onDialogFinished() {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        buttonView.setChecked(false);
                    }
                });
            }
        });
        FragmentManager fragmentManager = ((FragmentActivity) mActivity).getSupportFragmentManager();
        // do not allow to open 2 or more dialogs at the same time
        if (fragmentManager.findFragmentByTag("ToManyMultiSpeakersDialogFragment") != null) {
            return;
        }
        fragment.show(fragmentManager, "ToManyMultiSpeakersDialogFragment");
    }

    private void setGroupingModeChanged(final boolean isChecked) {
        if (mActivity == null) {
            return;
        }

        isMultiModeChanging.set(true);

        initSpinnerTimer();

        scheduleSpinnerTimer();

        mHomeSpeakerModel.isInMultiMode = isChecked;

        updateCheckBox();

        sendMultiroomCommand(isChecked);

        setOptimizationIfNeeded();
    }

    @SuppressWarnings("unused")
    public void onInfoIconClicked(View view) {
        DialogFragment dialogFragment = PlayingCastInfoDialogFragment.newInstance();
        FragmentManager fragmentManager = ((FragmentActivity) mActivity).getSupportFragmentManager();
        // do not allow to open 2 or more dialogs at the same time
        if (fragmentManager.findFragmentByTag("PlayingCastInfoDialogFragment") != null) {
            return;
        }
        dialogFragment.show(fragmentManager, "PlayingCastInfoDialogFragment");
    }

    public void dispose() {
        mActivity = null;
        mHomeSpeakerModel = null;
        cancelSpinnerTimer();
        cancelRefreshTimer();
        unlockCompoundButton();
    }

    private void hideSpinner() {
        isMultiModeChanging.set(false);
        mHomeSpeakerModel.isInMultiMode = mCompoundButtonOriginalValue;
        mIsMultiMode = mCompoundButtonOriginalValue;
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                unlockCompoundButton();
                restoreCompoundButton();
            }
        });

        cancelSpinnerTimer();
    }

    private void setOptimizationIfNeeded() {
        List<Radio> radios = NetRemoteManager.getInstance().getRadios();
        Radio radio = getRadio();
        StreamingQualityMgr streamingQualityMgr = StreamingQualityMgr.getInstance();
        boolean enableOptimization = streamingQualityMgr.getGeneralOptimizationStatus(radios);
        if (streamingQualityMgr.isTransportOptimizationEnabled(radio) != enableOptimization) {
            streamingQualityMgr.changeStreamingQuality(radio, new StreamingQualityMgr.IStreamingQualityChangeListener() {
                @Override
                public void onStreamingQualityChanged(boolean success) {
//                    TODO: No need to handle error status?
                }
            }, enableOptimization);
        }
    }

    private void initSpinnerTimer() {
        DelayedPostsManager.getInstance().cancelCallback(SPINNER_TIMER_ID);
        mFallbackNumber = RETRY_COUNT;
    }

    private void scheduleSpinnerTimer() {
        DelayedPostsManager.getInstance().postDelayed(this, SPINNER_TIMER_ID, RETRY_TIMEOUTS[mFallbackNumber]);
    }

    private void cancelSpinnerTimer() {
        DelayedPostsManager.getInstance().removeCallbackId(SPINNER_TIMER_ID);
    }

    private void lockCompoundButton(CompoundButton compoundButton, boolean value) {
        if (compoundButton != null) {
            mCompoundButton = compoundButton;
            mCompoundButtonOriginalValue = value;
            mCompoundButton.setClickable(false);
            mCompoundButton.setOnCheckedChangeListener(null);
            mCompoundButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
    }

    private void restoreCompoundButton() {
        if (mCompoundButton != null) {
            mCompoundButton.setChecked(mCompoundButtonOriginalValue);
        }
    }

    private void unlockCompoundButton() {
        if (mCompoundButton != null) {
            mCompoundButton.setClickable(true);
            mCompoundButton.setOnCheckedChangeListener(this);
            mCompoundButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });
        }
    }

    private void scheduleUpdateRefresh() {
        DelayedPostsManager.getInstance().postDelayed(this, REFRESH_MODEL_TIMER_ID, CONTENT_REFRESH_TIMEOUT);
    }

    private void cancelRefreshTimer() {
        DelayedPostsManager.getInstance().removeCallbackId(REFRESH_MODEL_TIMER_ID);
    }

    @Override
    public void onPost(long callbackID) {
        if (callbackID == SPINNER_TIMER_ID) {
            mFallbackNumber--;
            MultiroomRebuildGroups.getInstance().rebuildMultiroomGroupsAndDevices();
            if (mFallbackNumber >= 0) {
                DelayedPostsManager.getInstance().postDelayed(this, SPINNER_TIMER_ID, RETRY_TIMEOUTS[mFallbackNumber]);
            } else {
                hideSpinner();
            }
        } else if (callbackID == REFRESH_MODEL_TIMER_ID) {
            updatePlayingInformation();
            scheduleUpdateRefresh();
        }
    }
}

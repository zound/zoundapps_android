package com.zoundindustries.multiroom.myHome;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.apps_lib.multiroom.myHome.SpeakerSelectedEvent;
import com.apps_lib.multiroom.myHome.speakers.SpeakerModel;
import com.apps_lib.multiroom.myHome.speakers.SpeakerType;
import com.apps_lib.multiroom.myHome.speakers.SpeakersAdapter;
import com.apps_lib.multiroom.widgets.other.BarAnimator;
import com.frontier_silicon.loggerlib.FsLogger;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ListItemHomeSpeakerBinding;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import static com.apps_lib.multiroom.myHome.SpeakerSelectedEvent.EventType.SPEAKER_CONNECT_TO_SHOW_SETTINGS;

/**
 * Created by viftime on 22/03/16.
 */
public class HomeSpeakersAdapter extends SpeakersAdapter {

    private boolean mMasterIsMissing = false;
    private boolean mNotifyNeeded = true;

    private class HomeSpeakerViewHolder extends RecyclerView.ViewHolder {
        ListItemHomeSpeakerBinding mBinding;

        private HomeSpeakerViewHolder(ListItemHomeSpeakerBinding listItemBinding) {
            super(listItemBinding.getRoot());

            mBinding = listItemBinding;
        }
    }

    public HomeSpeakersAdapter(Activity activity) {
        super(activity);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SpeakerType speakerType = SpeakerType.values()[viewType];
        switch (speakerType) {
            case Speaker:
                ListItemHomeSpeakerBinding listItemBinding = DataBindingUtil.inflate(
                        LayoutInflater.from(parent.getContext()),
                        R.layout.list_item_home_speaker,
                        parent, false);

                return new HomeSpeakerViewHolder(listItemBinding);

            case Divider:
                View dividerLayout = LayoutInflater.from(parent.getContext()).inflate(
                        com.apps_lib.multiroom.R.layout.list_item_home_speaker_divider, parent, false);

                return new DividerViewHolder(dividerLayout);

            case ModeHeader:
            case NoSpeakersHeader:
                return super.onCreateViewHolder(parent, viewType);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        SpeakerModel homeSpeakerModel = mSpeakerModels.get(position);
        switch (homeSpeakerModel.type) {
            case Speaker:
                HomeSpeakerViewHolder homeSpeakerViewHolder = (HomeSpeakerViewHolder) holder;
                final ListItemHomeSpeakerBinding binding = homeSpeakerViewHolder.mBinding;
                final HomeSpeakerViewModel viewModel = new HomeSpeakerViewModel(mActivity, homeSpeakerModel, (homeSpeakerModel.deviceModel == null || homeSpeakerModel.deviceModel.isLonely()) || mMasterIsPresent);
                binding.switchGrouping.setOnCheckedChangeListener(viewModel);
                binding.switchGrouping.setChecked(viewModel.mIsMultiMode, false);
                binding.setViewModel(viewModel);

                binding.imageViewChevron.setOnTouchListener(new View.OnTouchListener() {
                    private boolean mDefaultRetValue = false;
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (v.getId() == R.id.imageViewChevron && !mDefaultRetValue) {
                            EventBus.getDefault().post(new SpeakerSelectedEvent(viewModel.mHomeSpeakerModel.deviceModel, SPEAKER_CONNECT_TO_SHOW_SETTINGS));
                            mDefaultRetValue = true;
                            return true;
                        }

                        return mDefaultRetValue;
                    }
                });
                break;
            case Divider:
                // no work to do here
                break;
            case ModeHeader:
            case NoSpeakersHeader:
                super.onBindViewHolder(holder, position);
                break;
        }
    }


    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        try {
            HomeSpeakerViewHolder homeSpeakerViewHolder = (HomeSpeakerViewHolder) holder;
            ListItemHomeSpeakerBinding binding = homeSpeakerViewHolder.mBinding;

            new BarAnimator(binding.leftAudioBar, 1100, true).animate();
            new BarAnimator(binding.midleAudioBar, 850, true).animate();
            new BarAnimator(binding.rightAudioBar, 1000, true).animate();

        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        int position = holder.getAdapterPosition();
        FsLogger.log("recycled from position " + String.valueOf(position));

        if (holder instanceof HomeSpeakerViewHolder) {
            HomeSpeakerViewHolder homeSpeakerViewHolder = (HomeSpeakerViewHolder) holder;
            disposeSpeakerViewHolder(homeSpeakerViewHolder);
        }

        super.onViewRecycled(holder);
    }

    private void disposeSpeakerViewHolder(HomeSpeakerViewHolder homeSpeakerViewHolder) {
        homeSpeakerViewHolder.mBinding.switchGrouping.setOnCheckedChangeListener(null);

        HomeSpeakerViewModel viewModel = homeSpeakerViewHolder.mBinding.getViewModel();
        if (viewModel != null) {
            viewModel.dispose();
            homeSpeakerViewHolder.mBinding.setViewModel(null);
        }
    }

    @Override
    public void swapItems(List<SpeakerModel> newList) {
        super.swapItems(newList);

        boolean isGroupPresent = isGroupPresent(newList);
        if (isGroupPresent) {
            mMasterIsPresent = false;
            checkIfMasterIsPresent(newList);
            if (!mMasterIsPresent) {
                mMasterIsMissing = true;
                if (mNotifyNeeded) {
                    for (int i = 0; i < mSpeakerModels.size(); i++) {
                        notifyItemChanged(i);
                    }
                    mNotifyNeeded = false;
                }
            }
        }


        if (isGroupPresent && mMasterIsMissing && mMasterIsPresent) {
            mMasterIsMissing = false;
            for (int i=0; i < mSpeakerModels.size(); i++) {
                notifyItemChanged(i);
            }
            mNotifyNeeded = true;
        }
    }
}


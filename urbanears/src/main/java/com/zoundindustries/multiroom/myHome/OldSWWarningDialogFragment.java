package com.zoundindustries.multiroom.myHome;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.apps_lib.multiroom.AnimatedDialogFragmentBase;
import com.apps_lib.multiroom.myHome.speakers.SpeakerModel;
import com.apps_lib.multiroom.persistence.PersistenceMgr;
import com.frontier_silicon.components.common.CommonPreferences;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.FragmentOldUpdateWarningFragmentBinding;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by nbalazs on 08/01/2018.
 */
public class OldSWWarningDialogFragment extends AnimatedDialogFragmentBase{

    private SpeakerModel mSpeakerModel;
    private IOldUpdateListener mListener;
    private FragmentOldUpdateWarningFragmentBinding mBinding;

    public static OldSWWarningDialogFragment newInstance(SpeakerModel speakerModel, IOldUpdateListener listener) {
        OldSWWarningDialogFragment fragment = new OldSWWarningDialogFragment();
        fragment.mSpeakerModel = speakerModel;
        fragment.mListener = listener;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_old_update_warning_fragment, container, false);

        setTextMessage();
        handleButtons();

        Window window = getDialog().getWindow();
        if (window != null) {
            window.requestFeature(Window.FEATURE_NO_TITLE);
        }

        return mBinding.getRoot();
    }

    private void handleButtons() {
        if (mListener == null) {
            return;
        }

        mBinding.buttonGotIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateOfCheck(mSpeakerModel);
                mListener.onDialogFinished();
                dismiss();
            }
        });
    }

    private void setTextMessage() {
        if (mSpeakerModel == null) {
            return;
        }

        mBinding.textMessage.setText(getString(R.string.update_available_msg2, mSpeakerModel.deviceModel.mRadio.getFriendlyName()));
    }

    private void setDateOfCheck(SpeakerModel speakerModel) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = Calendar.getInstance().getTime();
        PersistenceMgr.getInstance().setMomentOfShowingTheUpdateSpeakerMsg(speakerModel.deviceModel.mRadio.getSN(), simpleDateFormat.format(currentDate));
    }
}

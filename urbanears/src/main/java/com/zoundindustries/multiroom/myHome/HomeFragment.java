package com.zoundindustries.multiroom.myHome;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.apps_lib.multiroom.AnalyticsSender;
import com.apps_lib.multiroom.IUEChildFragment;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.browse_ir.events.ShowLoadingProgressEvent;
import com.apps_lib.multiroom.connection.ConnectionManager;
import com.apps_lib.multiroom.connection.IConnectToSpeakerListener;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.factory.EHomeFragmentTag;
import com.apps_lib.multiroom.myHome.HomeRefreshManager;
import com.apps_lib.multiroom.myHome.SpeakerSelectedEvent;
import com.apps_lib.multiroom.myHome.SpeakersDiscoveryManager;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.connection.ConnectionState;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.connection.IConnectionStateListener;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;
import com.frontier_silicon.loggerlib.FsLogger;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.FragmentMyHomeBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by viftime on 25/03/16.
 */
public class HomeFragment extends Fragment implements IConnectionStateListener, IUEChildFragment,
        IConnectToSpeakerListener {

    private FragmentMyHomeBinding mBinding;

    private HomeSpeakersMediator mHomeSpeakersMediator;

    private long mTimeOfConnectionTriggered;
    private long mTimeOfConnectionSuccessed;

    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_home, container, false);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupControls();
    }

    private void setupControls() {

        ImageButton volumeButton = (ImageButton) getActivity().findViewById(R.id.buttonVolume);
        volumeButton.setVisibility(View.VISIBLE);
        volumeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToVolumeActivity();
            }
        });

        setRefreshGesture();
    }

    public void setRefreshGesture() {

        mBinding.swipetoRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                SpeakersDiscoveryManager.getInstance().rescan();
                mBinding.swipetoRefresh.setRefreshing(false);
                AnalyticsSender.sendRefreshingScreenAnalytics();
                HomeRefreshManager.setIsManuallyRefresh(true);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        mHomeSpeakersMediator = new HomeSpeakersMediator();
        mHomeSpeakersMediator.init(getActivity(), mBinding.listSpeakers);
    }

    @Override
    public void onResume() {
        super.onResume();

        mHomeSpeakersMediator.attachToDiscovery();
        SpeakersDiscoveryManager.getInstance().tryStartDiscovery();

        ConnectionStateUtil.getInstance().addListener(this, true);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        mHomeSpeakersMediator.detachFromDiscovery();

        ConnectionStateUtil.getInstance().removeListener(this);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStop() {
        super.onStop();

        mHomeSpeakersMediator.dispose();
    }

    @Override
    public void onStateUpdate(final ConnectionState newState) {
        FsLogger.log(">>>>>>>> STATE UPDATED: " + newState);
        final Activity activity = getActivity();
        if (activity != null && !activity.isFinishing()) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (activity.isFinishing()) {
                        return;
                    }

                    switch (newState) {

                        case NO_WIFI_OR_ETHERNET:
                            goToNoWiFiFragment();
                            break;
                        case CONNECTED_RADIO_AP_MODE:
                            break;
                        case SEARCHING_RADIOS_NO_SPEAKER_FOUND_YET:
                        case NO_RADIOS_FOUND_AFTER_10_SECONDS:
                            goToScanningSpeakersFragment();
                            break;
                        case SEARCHING_RADIOS_SPEAKER_FOUND:
                            break;
                        case NOT_CONNECTED_TO_RADIO:
                            break;
                        case CONNECTED_TO_RADIO:
                            break;
                        case DISCONNECTED:
                            break;
                        case INVALID_SESSION:
                            break;
                    }
                }
            });
        }
    }

    private void goToNoWiFiFragment() {
        NavigationHelper.goToFragment(getActivity(), EHomeFragmentTag.NoWiFiFragment);
    }

    private void goToScanningSpeakersFragment() {
        NavigationHelper.goToFragment(getActivity(), EHomeFragmentTag.ScanExistingSpeakers);
    }

    private void goToVolumeActivity() {
        NavigationHelper.goToVolumeActivity(getActivity());
    }


    @Override
    public EHomeFragmentTag getStaticTag() {
        return EHomeFragmentTag.HomeFragment;
    }

    @Override
    public void onConnectedToServerOrRadio(boolean connectedOk, Radio selectedRadio) {
        mTimeOfConnectionSuccessed = System.currentTimeMillis();

        if (!connectedOk) {
            sendFabricAnswerEvent(false);
            return;
        }
        sendFabricAnswerEvent(true);


        Bundle bundle = new Bundle();
        bundle.putBoolean(ActivityFactory.SPEAKER_JUST_CONNECTED, true);

        NavigationHelper.goToActivity(getActivity(), ActivityFactory.getActivityFactory().getSourcesActivity(), NavigationHelper.AnimationType.SlideToLeft, false, bundle);
    }

    private void sendFabricAnswerEvent(boolean success) {
        long mTimeOfConnection = mTimeOfConnectionSuccessed - mTimeOfConnectionTriggered;
        AnalyticsSender.sendConnectedFromHomeScreenAnalytics(success, mTimeOfConnection);
    }

    @Subscribe
    public void onSpeakerClicked(SpeakerSelectedEvent event) {
        mTimeOfConnectionTriggered = System.currentTimeMillis();
        if (event.eventType == SpeakerSelectedEvent.EventType.SPEAKER_CONNECT_TO_CONTROL) {
            mTimeOfConnectionTriggered = System.currentTimeMillis();
            tryConnectToSelectedRadio(event.multiroomDeviceModel);
        } else {
            openSpeakerSettings(event.multiroomDeviceModel);
        }
    }

    private void openSpeakerSettings(MultiroomDeviceModel deviceModel) {
        Bundle bundle = new Bundle();
        bundle.putString(NavigationHelper.RADIO_UDN_ARG, deviceModel.mRadio.getUDN());

        NavigationHelper.goToActivity(getActivity(), ActivityFactory.getActivityFactory().getSettingsActivity(), NavigationHelper.AnimationType.SlideToLeft,
                false, bundle);
    }


    @Subscribe
    public void onLoadingFinished(ShowLoadingProgressEvent event) {
        mBinding.listSpeakers.scrollToPosition(0);
        HomeRefreshManager.setIsManuallyRefresh(false);
    }

    private void tryConnectToSelectedRadio(MultiroomDeviceModel deviceModel) {

        ConnectionManager.getInstance().connect(deviceModel, false, this);
    }

    //Not used at the moment. It's probable that Zound will request this
//    private void restoreLastConnectedDevice() {
//        if (!ConnectionManager.getInstance().getShouldAutoConnectToLastSpeaker()) {
//            return;
//        }
//
//        ConnectionManager.getInstance().setShouldAutoConnectToLastSpeaker(false);
//
//        Radio currentRadio = NetRemoteManager.getInstance().getCurrentRadio();
//
//        if (currentRadio != null && currentRadio.IsConnectionOk()) {
//            return;
//        }
//
//        Radio lastConnectedRadio = ConnectionManager.getInstance().getLastSelectedDevice();
//        if (lastConnectedRadio == null) {
//            return;
//        }
//
//        MultiroomDeviceModel lastConnectedDeviceModel = MultiroomGroupManager.getInstance().getDevice(lastConnectedRadio.getUDN());
//        if (lastConnectedDeviceModel == null) {
//            return;
//        }
//
//        ConnectionManager.getInstance().connect(lastConnectedDeviceModel, true, this);
//    }
}

package com.zoundindustries.multiroom.myHome;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.apps_lib.multiroom.IUEChildFragment;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.factory.EHomeFragmentTag;
import com.apps_lib.multiroom.factory.FragmentFactory;
import com.apps_lib.multiroom.myHome.IDrawerActivity;
import com.apps_lib.multiroom.myHome.IFragmentSwitcher;
import com.apps_lib.multiroom.myHome.NavDrawerItem;
import com.apps_lib.multiroom.myHome.NavDrawerListAdapter;
import com.frontier_silicon.NetRemoteLib.AccessPointUtil;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.connection.ConnectionLostFragment;
import com.zoundindustries.multiroom.help.HelpActivity;
import com.zoundindustries.multiroom.settings.multi.MultiSpeakersActivity;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.util.ArrayList;

public class HomeActivity extends UEActivityBase implements IFragmentSwitcher, IDrawerActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ArrayList<NavDrawerItem> mNavDrawerItems = new ArrayList<>();
    private final int FRAGMENT_CONTAINER_ID = R.id.layoutContentFragment;
    private boolean mIsSpeakerSettingsDisplayed = true;
    private View headerView;
    private View footerView;
    private Boolean exit = false;

    private static final String STATE_NAVIGATION_DRAWER_SETTINGS_ENABLED = "settings_enabled";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_home);

        createHeaderAndFooterView();
        setupControls();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putBoolean(STATE_NAVIGATION_DRAWER_SETTINGS_ENABLED, mIsSpeakerSettingsDisplayed);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mIsSpeakerSettingsDisplayed = savedInstanceState.getBoolean(STATE_NAVIGATION_DRAWER_SETTINGS_ENABLED, true);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getBoolean("DISCONNECTED", false)) {
            getSupportFragmentManager().beginTransaction()
                    .add(FRAGMENT_CONTAINER_ID, new ConnectionLostFragment())
                    .commit();
        } else {
            addMainFragment(extras);
        }
    }

    @Override
    public void onResume() {
        if (currentNetworkIsAP()) {
            NavigationHelper.startSetupActivity(this);
        }

        initNavigationDrawer(mIsSpeakerSettingsDisplayed);

        super.onResume();
    }

    private void initNavigationDrawer(boolean displaySpeakerSettings) {
        mIsSpeakerSettingsDisplayed = displaySpeakerSettings && AccessPointUtil.isConnectedToWiFiOrEthernet();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.hamburger_open_desc, R.string.hamburger_close_desc);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mNavDrawerItems.clear();

        mNavDrawerItems.add(new NavDrawerItem(getString(R.string.hamburger_setup), R.drawable.ic_menu_setup_speaker));
        if (mIsSpeakerSettingsDisplayed) {
            mNavDrawerItems.add(new NavDrawerItem(getString(R.string.hamburger_settings), R.drawable.ic_menu_speaker_settings));
        }
        mNavDrawerItems.add(new NavDrawerItem(getString(R.string.hamburger_help), R.drawable.ic_menu_help));
        mNavDrawerItems.add(new NavDrawerItem(getString(R.string.hamburger_shop), R.drawable.ic_menu_shop));
        mNavDrawerItems.add(new NavDrawerItem(getString(R.string.hamburger_about), R.drawable.ic_menu_about));

        ListView mDrawerList = (ListView) findViewById(R.id.navList);
        NavDrawerListAdapter navDrawerListAdapter = new NavDrawerListAdapter(this, mNavDrawerItems);
        addHeaderAndFooterToListView(mDrawerList);

        mDrawerList.setAdapter(navDrawerListAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onNavDrawerListItemClicked(position);
            }
        });
    }

    private void addHeaderAndFooterToListView(ListView listView) {
        listView.removeFooterView(footerView);
        listView.removeHeaderView(headerView);

        listView.addFooterView(footerView, null, false);
        listView.addHeaderView(headerView, null, false);
    }

    @SuppressLint("InflateParams")
    private void createHeaderAndFooterView() {
        LayoutInflater inflater = getLayoutInflater();
        headerView = inflater.inflate(R.layout.list_item_header, null);
        footerView = inflater.inflate(R.layout.list_item_footer, null);
    }


    private void addMainFragment(Bundle savedInstanceState) {
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.layoutContentFragment) == null)
            return;

        // However, if we're being restored from a previous state,
        // then we don't need to do anything and should return or else
        // we could end up with overlapping fragments.
        if (savedInstanceState != null)
            return;

        hideAllActionButtons();

        EHomeFragmentTag fragmentTag = getTagOfNeededFragment();

        Fragment fragment = FragmentFactory.getFragment(fragmentTag);

        adjustHeaderContent(fragmentTag);

        if (fragment != null) {

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            fragment.setArguments(getIntent().getExtras());

            getSupportFragmentManager().beginTransaction()
                    .replace(FRAGMENT_CONTAINER_ID, fragment)
                    .commitAllowingStateLoss();
        }
    }

    private void adjustHeaderContent(EHomeFragmentTag fragmentTag) {
        findViewById(R.id.titleImageView).setVisibility(View.VISIBLE);
    }

    @Override
    public void switchFragment(EHomeFragmentTag fragmentTag) {

        if (fragmentTag.equals(EHomeFragmentTag.NoWiFiFragment)) {
            initNavigationDrawer(false);
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(FRAGMENT_CONTAINER_ID);

        if (currentFragment != null) {
            IUEChildFragment childFragment = (IUEChildFragment) currentFragment;
            EHomeFragmentTag tagOfCurrentFragment = childFragment.getStaticTag();

            if (tagOfCurrentFragment.equals(fragmentTag)) {
                return;
            }
        }

        HomeActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideAllActionButtons();
            }
        });

        Fragment newFragment = FragmentFactory.getFragment(fragmentTag);

        adjustHeaderContent(fragmentTag);

        FragmentTransaction transaction = fragmentManager.beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(FRAGMENT_CONTAINER_ID, newFragment);
        //transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commitAllowingStateLoss();
    }

    private void hideAllActionButtons() {
        View refreshButton = findViewById(R.id.buttonRefresh);
        refreshButton.setVisibility(View.GONE);

        View volumeButton = findViewById(R.id.buttonVolume);
        volumeButton.setVisibility(View.GONE);
    }

    private EHomeFragmentTag getTagOfNeededFragment() {
        switch (ConnectionStateUtil.getInstance().getConnectionState()) {
            case NO_WIFI_OR_ETHERNET:
                return EHomeFragmentTag.NoWiFiFragment;
            case DISCONNECTED:
            case INVALID_SESSION:
                return EHomeFragmentTag.ConnectionLostFragment;
            default:
                if (canGoToHome()) {
                    return EHomeFragmentTag.HomeFragment;
                }
                return EHomeFragmentTag.ScanExistingSpeakers;
        }
    }

    private boolean canGoToHome() {
        return NetRemoteManager.getInstance().getRadios().size() != 0 &&
                MultiroomGroupManager.getInstance().getFlatGroupDeviceList().size() != 0;
    }

    private void setupControls() {
        ImageView closeNavigation = (ImageView) findViewById(R.id.close_drawer);
        if (closeNavigation != null) {
            closeNavigation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        mDrawerLayout.removeDrawerListener(mDrawerToggle);
        mDrawerLayout = null;
        mDrawerToggle = null;
        headerView = null;
        footerView = null;

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            closeAppWhenDoubleTapBack();
        }
    }

    private void closeAppWhenDoubleTapBack() {
        if (exit) {
            finish();
        } else {
            Toast.makeText(this, getString(R.string.press_back_again), Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }

    @Override
    public void refreshNavigationDrawer(boolean displaySpeakerSettings) {
        initNavigationDrawer(displaySpeakerSettings);
    }

    private void onNavDrawerListItemClicked(int position) {
        if (mIsSpeakerSettingsDisplayed) {
            switch (position) {
                case 1:
                    openSetupNewSpeakerActivity();
                    break;
                case 2:
                    openSettingsActivity();
                    break;
                case 3:
                    openHelpActivity();
                    break;
                case 4:
                    openWebShop();
                    break;
                case 5:
                    openAboutActivity();
                    break;
            }
        } else {
            switch (position) {
                case 1:
                    openSetupNewSpeakerActivity();
                    break;
                case 2:
                    openHelpActivity();
                    break;
                case 3:
                    openWebShop();
                    break;
                case 4:
                    openAboutActivity();
                    break;
            }
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    private void openSettingsActivity() {
        NavigationHelper.goToActivity(this, MultiSpeakersActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }

    private void openWebShop() {
        NavigationHelper.openWebViewActivityForURL(this, NavigationHelper.AnimationType.Normal, getResources().getString(R.string.link_connected_shop));
    }

    private void openSetupNewSpeakerActivity() {
        NavigationHelper.startSetupActivity(this);
    }

    private void openAboutActivity() {
        NavigationHelper.goToActivity(this, ActivityFactory.getActivityFactory().getAboutScreenActivity(), NavigationHelper.AnimationType.SlideToLeft);
    }

    private void openHelpActivity() {
        NavigationHelper.goToActivity(this, HelpActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }

    private boolean currentNetworkIsAP() {
        String ipAddressString;

        int ipAddress = AccessPointUtil.getWifiManager().getDhcpInfo().gateway;

        // Convert little-endian to big-endianif needed
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

        try {
            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
        } catch (UnknownHostException ex) {
            ipAddressString = null;
        }

        return ipAddressString != null && ipAddressString.equals(NetRemoteManager.RADIO_HUI_DEFAULT_ADDRESS);
    }
}

package com.zoundindustries.multiroom.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;


import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.widgets.AnimationFreeSwitchBase;

/**
 * Created by nbalazs on 26/09/2016.
 */
public class AnimationFreeSwitch extends AnimationFreeSwitchBase {

    public AnimationFreeSwitch(Context context) {
        super(context);
        mLeftString = context.getResources().getString(R.string.solo_caps);
        mRightString = context.getResources().getString(R.string.multi_caps);
    }

    public AnimationFreeSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        mLeftString = context.getResources().getString(R.string.solo_caps);
        mRightString = context.getResources().getString(R.string.multi_caps);
    }

    public AnimationFreeSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mLeftString = context.getResources().getString(R.string.solo_caps);
        mRightString = context.getResources().getString(R.string.multi_caps);
    }

    @Override
    protected Paint createSelectedTextPaint() {
        return createNotSelectedTextPaint();
    }

    @Override
    protected Paint createNotSelectedTextPaint() {
        Paint textPaint = new Paint();
        //noinspection deprecation
        textPaint.setColor(getContext().getResources().getColor(R.color.switch_text_color));
        textPaint.setTextSize(getFixedSize());
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextAlign(Paint.Align.CENTER);
        // Set textSize, typeface, etc, as you wish
        return textPaint;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        final Rect textBounds = new Rect();
        mSelectedTextPaint.getTextBounds(mRightString, 0, mRightString.length(), textBounds);

        // The baseline for the text: centered, including the height of the text itself
        final int heightBaseline = canvas.getClipBounds().height() / 2 + textBounds.height() / 2;

        // This is one quarter of the full width, to measure the centers of the texts
        final int widthQuarter = canvas.getClipBounds().width() / 4;
        canvas.drawText(mLeftString, 0, mLeftString.length(),
                widthQuarter, heightBaseline,
                mSelectedTextPaint);
        canvas.drawText(mRightString, 0, mRightString.length(),
                widthQuarter * 3, heightBaseline,
                mSelectedTextPaint);
    }

    @Override
    protected float getFixedSize() {
        float textSizeInDP = 12.0f;
        float density = getContext().getResources().getDisplayMetrics().density;

        if ((Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) && (density < 1.75f)) {
            textSizeInDP = 9.0f;
        }
        return textSizeInDP * density + 0.5f;
    }


}

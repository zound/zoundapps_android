package com.zoundindustries.multiroom.connection;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.apps_lib.multiroom.IUEChildFragment;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.factory.EHomeFragmentTag;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.FragmentNoConnectionBinding;

/**
 * Created by lsuhov on 09/05/16.
 */
public class ConnectionLostFragment extends Fragment implements IUEChildFragment {

    private ConnectionLostViewModel mViewModel;

    private FragmentNoConnectionBinding mBinding;

    public ConnectionLostFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mViewModel == null) {
            mViewModel = new ConnectionLostViewModel();
        }

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_no_connection, container, false);
        mBinding.setViewModel(mViewModel);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);

        setupControls();
    }

    public void onResume() {
        super.onResume();

        mViewModel.init(getActivity());
    }

    @Override
    public void onPause() {
        mViewModel.clear();

        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (mBinding != null) {
            if (mBinding.getViewModel() != null) {
                mBinding.getViewModel().dispose();
                mBinding.setViewModel(null);
            }
        }

        super.onDestroyView();
    }

    private void setupControls() {
        ImageButton volumeButton = (ImageButton) getActivity().findViewById(R.id.buttonVolume);
        volumeButton.setVisibility(View.VISIBLE);
        volumeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToVolumeActivity();
            }
        });
    }

    private void goToVolumeActivity() {
        NavigationHelper.goToVolumeActivity(getActivity());
    }
    
    @Override
    public EHomeFragmentTag getStaticTag() {
        return EHomeFragmentTag.ConnectionLostFragment;
    }

}

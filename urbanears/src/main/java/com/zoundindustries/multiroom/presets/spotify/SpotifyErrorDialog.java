package com.zoundindustries.multiroom.presets.spotify;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.apps_lib.multiroom.AnimatedDialogFragmentBase;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.connection.ConnectionManager;
import com.apps_lib.multiroom.presets.spotify.AuthenticationScope;
import com.apps_lib.multiroom.setup.normalSetup.SetupManager;
import com.apps_lib.multiroom.setup.presets.PresetsFinalActivity;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.zoundindustries.multiroom.R;

/**
 * Created by nbalazs on 24/11/2016.
 */
public class SpotifyErrorDialog extends AnimatedDialogFragmentBase {

    private AuthenticationScope mAuthenticationScope;

    public static DialogFragment newInstance(AuthenticationScope scope) {
        SpotifyErrorDialog spotifyErrorDialog = new SpotifyErrorDialog();
        spotifyErrorDialog.mAuthenticationScope = scope;

        return spotifyErrorDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spotify_error, container, false);

        setupView(view);

        Window window = getDialog().getWindow();
        if (window != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        return view;
    }

    @SuppressWarnings("all")
    private void setupView(View view) {
        Radio radio = null;
        if (mAuthenticationScope == AuthenticationScope.AUTHENTICATION_AT_THE_END) {
            radio = ConnectionManager.getInstance().getLastSelectedDevice();
        } else {
            radio = SetupManager.getInstance().getFoundRadioAfterSetup();
        }

        ((TextView)(view.findViewById(R.id.alertContent1))).setText(getContext().getString(R.string.spotify_error_content, radio != null ? radio.getFriendlyName() : ""));

        (view.findViewById(R.id.buttonTryAgain)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryAgain();
            }
        });

        (view.findViewById(R.id.buttonTryLater)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryLater();
            }
        });
    }

    private void tryAgain() {
        dismiss();
    }

    private void tryLater() {
        if (mAuthenticationScope == AuthenticationScope.AUTHENTICATION_AT_THE_BEGINNING) {
            NavigationHelper.goToActivityClearingCurrentStack(getActivity(), PresetsFinalActivity.class, NavigationHelper.AnimationType.SlideToLeft);
        }
        dismiss();
    }

}

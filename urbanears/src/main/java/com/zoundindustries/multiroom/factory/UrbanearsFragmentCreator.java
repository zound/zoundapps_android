package com.zoundindustries.multiroom.factory;

import com.apps_lib.multiroom.factory.FragmentCreator;
import com.zoundindustries.multiroom.connection.ConnectionLostFragment;
import com.zoundindustries.multiroom.connection.NoWiFiFragment;
import com.zoundindustries.multiroom.myHome.HomeFragment;
import com.zoundindustries.multiroom.myHome.ScanExistingSpeakersFragment;
import com.zoundindustries.multiroom.setup.normalSetup.NoNewSpeakerFound1Fragment;
import com.zoundindustries.multiroom.setup.normalSetup.NoNewSpeakerFound2Fragment;
import com.zoundindustries.multiroom.setup.normalSetup.NoNewSpeakerFound3Fragment;
import com.zoundindustries.multiroom.setup.normalSetup.NoNewSpeakerFound4Fragment;
import com.zoundindustries.multiroom.setup.normalSetup.ScanNewSpeakersFragment;

/**
 * Created by nbalazs on 10/05/2017.
 */

public class UrbanearsFragmentCreator extends FragmentCreator{

    @Override
    public Class getScanExistingSpeakersFragment() {
        return ScanExistingSpeakersFragment.class;
    }

    @Override
    public Class getHomeFragment() {
        return HomeFragment.class;
    }

    @Override
    public Class getConnectionLostFragment() {
        return ConnectionLostFragment.class;
    }

    @Override
    public Class getNoWiFiFragment() {
        return NoWiFiFragment.class;
    }

    @Override
    public Class getScanNewSpeakerFragment() {
        return ScanNewSpeakersFragment.class;
    }

    @Override
    public Class getNoSpeakerFoundFragment(int tag) {
        switch (tag) {
            case 0:
                return  NoNewSpeakerFound1Fragment.class;
            case 1:
                return NoNewSpeakerFound2Fragment.class;
            case 2:
                return NoNewSpeakerFound3Fragment.class;
            case 3:
                return NoNewSpeakerFound4Fragment.class;
        }

        return null;
    }
}

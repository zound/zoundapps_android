package com.zoundindustries.multiroom.setup.normalSetup;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.setup.normalSetup.AccessPointModel;
import com.apps_lib.multiroom.setup.normalSetup.ConnectToHeadlessAPHelper;
import com.apps_lib.multiroom.setup.normalSetup.SetupManager;
import com.apps_lib.multiroom.setup.normalSetup.TroubleshootingActivity;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.FragmentConnectionLostDuringSetupBinding;

/**
 * Created by cvladu on 31/10/2017.
 */

public class ConnectionLostDuringSetupFragment extends Fragment {
    private FragmentConnectionLostDuringSetupBinding mBinding;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_connection_lost_during_setup, container, false);
        setup();
        return mBinding.getRoot();
    }

    private void setup() {
        mBinding.reconnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AccessPointModel lastSelectedAccessPoint = SetupManager.getInstance().getLastSelectedAccessPoint();
                if (lastSelectedAccessPoint != null) {
                    ConnectToHeadlessAPHelper.getInstance().tryConnectToAP(getActivity(), lastSelectedAccessPoint);
                    ((ScanNewSpeakersActivity) getActivity()).loadNewSpeakerFragment();
                }
            }
        });

        mBinding.troubleshootingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationHelper.goToActivity(getActivity(), TroubleshootingActivity.class, NavigationHelper.AnimationType.SlideToLeft);
            }
        });

        mBinding.cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationHelper.goToHomeAndReconnectToInitialWiFi(getActivity());
            }
        });

    }

}

package com.zoundindustries.multiroom.setup.presets;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.setup.presets.PresetsFinalActivity;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivityPresetsAddingFailedBinding;

/**
 * Created by cvladu on 07/12/16.
 */

public class PresetsAddingFailedActivity extends UEActivityBase {
    private ActivityPresetsAddingFailedBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_presets_adding_failed);

        setupAppBar();

        setTitle(R.string.setup_adding_presets);

        setupControls();
    }

    private void setupControls() {
        mBinding.buttonTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToActivity(PresetsAddingFailedActivity.this, ActivityFactory.getActivityFactory().getSetPresetsActivity(), NavigationHelper.AnimationType.SlideToLeft);
            }
        });

        mBinding.buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToActivity(PresetsAddingFailedActivity.this, PresetsFinalActivity.class, NavigationHelper.AnimationType.SlideToLeft);
            }
        });
    }
}

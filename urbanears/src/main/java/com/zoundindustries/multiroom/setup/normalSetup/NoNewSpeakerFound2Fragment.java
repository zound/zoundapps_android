package com.zoundindustries.multiroom.setup.normalSetup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.multiroom.databinding.FragmentNoNewSpeakerFound2Binding;
import com.zoundindustries.multiroom.R;

/**
 * Created by lsuhov on 08/04/16.
 */
public class NoNewSpeakerFound2Fragment extends NoNewSpeakerFoundFragmentBase {

    public static final String FRAGMENT_TAG = "TAG_NO_NEW_SPEAKER_FOUND_2_FRAGMENT";

    private FragmentNoNewSpeakerFound2Binding mBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_no_new_speaker_found_2, container, false);

        return mBinding.getRoot();
    }

    @Override
    protected void setupControls() {
        mBinding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNextPage();
            }
        });

        mBinding.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelSetup();
            }
        });
    }
}

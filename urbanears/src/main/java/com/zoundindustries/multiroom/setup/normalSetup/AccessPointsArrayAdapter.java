package com.zoundindustries.multiroom.setup.normalSetup;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apps_lib.multiroom.myHome.speakers.SpeakerNameManager;
import com.apps_lib.multiroom.setup.normalSetup.AccessPointModel;
import com.apps_lib.multiroom.setup.normalSetup.SetupManager;
import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.ESpeakerModelType;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.zoundindustries.multiroom.R;

import java.util.List;

public class  AccessPointsArrayAdapter extends ArrayAdapter<AccessPointModel> {

    private Context mContext = null;
    private int mLayoutId;

    public AccessPointsArrayAdapter(Context context, int resource, List<AccessPointModel> objects) {
        super(context, resource, objects);

        mContext = context;
        mLayoutId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        AccessPointModel device = getItem(position);

        if (v == null || v.getTag() == null) {
            v = li.inflate(mLayoutId, null);
        }

        ScanResult scanResult = device.getScanResult();

        ImageView imageView = (ImageView) v.findViewById(R.id.imageSpeaker);
        imageView.setImageResource(SpeakerManager.getInstance().getImageIdForSSID(scanResult.SSID, ESpeakerImageSizeType.Home));

        TextView tv1 = (TextView) v.findViewById(R.id.text1);
        TextView tv2 = (TextView) v.findViewById(R.id.text2);
        String text;

        SpeakerNameManager speakerNameManager = SpeakerNameManager.getInstance();
        if (SpeakerManager.getInstance().getSpeakerModelFromSSID(scanResult.SSID) != ESpeakerModelType.Unknown) {
            ESpeakerModelType speakerModelType = SpeakerManager.getInstance().getSpeakerModelFromSSID(scanResult.SSID);
            tv1.setText(speakerNameManager.generateNameForDevice(speakerModelType, scanResult.SSID, device.getSpeakerName() != null ? device.getSpeakerName() : getRadioNameAndMac(scanResult.SSID)));
            tv2.setText(getRadioColor(scanResult.SSID));
            tv2.setVisibility(View.VISIBLE);

        } else {
            tv2.setVisibility(View.GONE);
            ESpeakerModelType speakerModelType = SpeakerManager.getInstance().getSpeakerModelFromSSID(scanResult.SSID);
            text = speakerNameManager.generateNameForDevice(speakerModelType, scanResult.SSID, device.getSpeakerName() != null ? device.getSpeakerName() : scanResult.SSID);

            tv1.setText(text);
        }

        SetupManager setupManager = SetupManager.getInstance();

        boolean isConnectingToRadio = setupManager.isConnecting(scanResult);
        ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.progressBarConnecting);
        progressBar.setVisibility(isConnectingToRadio ? View.VISIBLE : View.GONE);

        boolean isConnectedToRadio = setupManager.isConnectedToRadio() &&
                setupManager.isConnectedToGivenAP(scanResult);

        ImageView checkMarkImageView = (ImageView) v.findViewById(R.id.radioSelectedImageView);
        checkMarkImageView.setVisibility(isConnectedToRadio ? View.VISIBLE : View.GONE);

        v.setTag(device);

        return v;
    }

    private String getRadioNameAndMac(String radioFullName) {
        String[] radioName = radioFullName.split("_");
        int length = radioName.length;
        return radioName[0] + " " + radioName[length - 1];
    }

    private String getRadioColor(String radioFullName) {
        String[] radioName = radioFullName.split("_");
        int length = radioName.length;
        if (length == 3) {
            return radioName[1];
        } else if (radioName[1].equalsIgnoreCase("Gold") && radioName[2].equalsIgnoreCase("Fish")) {
            return radioName[1] + radioName[2].toLowerCase();
        } else {
            return radioName[1] + " " + radioName[2];
        }
    }
}

package com.zoundindustries.multiroom.castTos;

import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.frontier_silicon.components.common.CommonPreferences;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivityCastTosIntroductionBinding;

/**
 * Created by lsuhov on 30/05/16.
 */
public class CastTosIntroductionActivity extends UEActivityBase {

    private ActivityCastTosIntroductionBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_cast_tos_introduction);

        setupControls();

        startAnimation();
    }

    private void startAnimation() {
        View titleView = findViewById(R.id.introductionTitle);
        View buttonView = findViewById(R.id.buttonAccept);
        View tosIntroductionView = findViewById(R.id.textTosIntroduction);

        Animation animTo100 = AnimationUtils.loadAnimation(this, R.anim.tos_anim_fade_in_100);
        Animation animTo90 = AnimationUtils.loadAnimation(this, R.anim.tos_anim_fade_in_90);

        titleView.startAnimation(animTo100);
        buttonView.startAnimation(animTo90);
        tosIntroductionView.startAnimation(animTo100);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mBinding.videoView.isPlaying()) {
            mBinding.videoView.resume();
        } else {
            mBinding.videoView.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBinding.videoView.isPlaying()) {
            mBinding.videoView.pause();
        }
    }

    private void setupControls() {
        setupVidwoView();

        setupCastText();

        setupAcceptButton();
    }

    void setupVidwoView() {
        String uriPath = "android.resource://" + getPackageName() + "/" + R.raw.intro;
        Uri video = Uri.parse(uriPath);
        mBinding.videoView.setVideoURI(video);
        mBinding.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
    }

    private void setupAcceptButton() {
        mBinding.buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonPreferences.getInstance().setCastTosAcceptance(true);
                NavigationHelper.goToActivity(CastTosIntroductionActivity.this, ActivityFactory.getActivityFactory().getHomeActivity(), NavigationHelper.AnimationType.SlideToLeft);
                finish();
            }
        });
    }

    private void setupCastText() {
        String linkString1 = getString(R.string.terms_conditions);
        String linkString2 = getString(R.string.google_terms_of_service);
        String linkString3 = getString(R.string.google_privacy_policy);

        String tosIntroductionString = getString(R.string.tos_introduction1, linkString1, linkString2, linkString3);

        int startIndex1 = tosIntroductionString.indexOf(linkString1);
        int endIndex1 = startIndex1 + linkString1.length();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(tosIntroductionString);
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                openCastTermsAndConditions();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        }, startIndex1, endIndex1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), startIndex1, endIndex1, 0);


        int startIndex2 = tosIntroductionString.indexOf(linkString2);
        int endIndex2 = startIndex2 + linkString2.length();
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                openCastTermsAndConditions();
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        }, startIndex2, endIndex2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), startIndex2, endIndex2, 0);

        int startIndex3 = tosIntroductionString.indexOf(linkString3);
        int endIndex3 = startIndex3 + linkString3.length();
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                openCastTermsAndConditions();
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        }, startIndex3, endIndex3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), startIndex3, endIndex3, 0);

        mBinding.textTosIntroduction.setMovementMethod(LinkMovementMethod.getInstance());
        mBinding.textTosIntroduction.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);
    }

    private void openCastTermsAndConditions() {
        NavigationHelper.goToActivity(this, CastTermsAndConditionsActivity.class,
                NavigationHelper.AnimationType.SlideToLeft);
    }

    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        } else {
            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);
            System.exit(0);
        }
    }
}
